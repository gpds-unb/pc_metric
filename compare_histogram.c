/*
 * Copyright (C) 2019 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    FILE *h1, *h2;
    int histogram_size = 0;

    if (argc < 3){
        fprintf(stderr, "Usage: %s [OPTIONS]\n", argv[0]);
        fprintf(stderr, "Usage example: %s hist1.bin hist2.bin\n", argv[0]);
        fprintf(stderr, "Outputs the histogram distance.\n");
        return EXIT_SUCCESS;
    }

    h1 = fopen(argv[1], "r");
    h2 = fopen(argv[2], "r");

    if (h1 == NULL || h2 == NULL)
    {
        fprintf(stderr, "Error opening a file.\n");
        return EXIT_FAILURE;
    }

    // get file size
    int h1_size = 0, h2_size = 0;
    fseek(h1, 0L, SEEK_END);
    fseek(h2, 0L, SEEK_END);
    h1_size = ftell(h1);
    h2_size = ftell(h2);
    fseek(h1, 0L, SEEK_SET);
    fseek(h2, 0L, SEEK_SET);

    if (h1_size != h2_size)
    {
        fprintf(stderr, "histograms size don't match\n");
        return EXIT_FAILURE;
    }

    // TODO: implement support for non-normalized histograms in uint64_t
    histogram_size = h1_size / sizeof(double);

    double hist1[histogram_size];
    double hist2[histogram_size];

    double sum = 0;

    fread(hist1, sizeof(double), histogram_size, h1);
    fread(hist2, sizeof(double), histogram_size, h2);

    for (int i = 0; i < histogram_size; i++)
    {
        sum += pow(hist1[i] - hist2[i], 2);
    }

    double result = sqrt(sum);
    // double result = sqrt(sum / histogram_size);
    printf("%.10f", result);


    fclose(h1);
    fclose(h2);

    return EXIT_SUCCESS;
}
