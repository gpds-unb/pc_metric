/*
 * Copyright (C) 2019-2020 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */

#include <unistd.h>
#include <cstdint>
#include <sstream>
#include <iostream>
#include <memory>
#include <thread>

#include <Open3D.h>

using namespace open3d;
using namespace std;

int main(int argc, char *argv[])
{
    bool convert_rgb_to_yuv = false;
    bool convert_rgb_to_gray = false;
    bool divide_color_by_255 = false;
    bool estimate_normals = false;

    auto pc = make_shared<geometry::PointCloud>();

    if (argc < 3)
    {
    usage_info:
        fprintf(stderr, "Usage: %s  [-y] [-g] [-c] [-n] input.{ply,xyz,xyzrgb,pcd} output.{ply,xyz,xyzrgb,pcd}\n", argv[0]);
        fprintf(stderr, " -y    Convert RGB to YUV.\n");
        fprintf(stderr, " -g    Convert RGB to Grayscale.\n");
        fprintf(stderr, " -c    Divide color attributes by 255\n");
        fprintf(stderr, " -n    Estimate normals\n");
        return EXIT_FAILURE;
    }

    if (strstr(argv[argc-2], ".ply" ) || strstr(argv[argc-2], ".xyz") || strstr(argv[argc-2], ".xyzrgb" ) || strstr(argv[argc-2], ".pcd" ))
    {
        if (io::ReadPointCloud(argv[argc-2], *pc)) {
            fprintf(stderr, "Successfully read %s\n", argv[argc-2]);
        } else {
            fprintf(stderr, "Failed to file %s.\n",argv[argc-2]);
            return EXIT_FAILURE;
        }
    }
    if (strstr(argv[argc-2], ".txt" )) // 3dtk format
    {
#if ((OPEN3D_VERSION_MAJOR == 0 ) &&  (OPEN3D_VERSION_MINOR < 10))
        if (io::ReadPointCloudFromXYZRGB(argv[argc-2], *pc, false))
#else
        io::ReadPointCloudOption pc_params = io::ReadPointCloudOption ("auto", true, true, false, NULL);
        if (io::ReadPointCloudFromXYZRGB(argv[argc-2], *pc, pc_params))
#endif
        {
            fprintf(stderr, "Successfully read %s\n", argv[argc-2]);
        } else {
            fprintf(stderr, "Failed to file %s.\n",argv[argc-2]);
            return EXIT_FAILURE;
        }
    }


    int opt;
    while ((opt = getopt(argc, argv, "ygcn")) != -1){
        switch (opt){
        case 'y':
            fprintf(stderr, "RGB to YCbCr enabled\n");
            convert_rgb_to_yuv = true;
            break;
        case 'g':
            fprintf(stderr, "RGB to Grayscale enabled\n");
            convert_rgb_to_gray = true;
            break;
        case 'c':
            fprintf(stderr, "Divide RGB by 255 enabled\n");
            divide_color_by_255 = true;
            break;
        case 'n':
            fprintf(stderr, "Estimate normals enabled\n");
            estimate_normals = true;
            break;
        default:
            fprintf(stderr, "Wrong command line.\n");
            goto usage_info;
        }
    }

    if (convert_rgb_to_yuv)
    {
        double y, cb, cr;
        for (size_t i = 0; i < pc->points_.size(); i++)
        {
          // fprintf(stderr, "colour %f %f %f\n", pc->colors_[i](0), pc->colors_[i](1), pc->colors_[i](2));
            if (divide_color_by_255)
            {
                pc->colors_[i](0) = pc->colors_[i](0) / 255.0;
                pc->colors_[i](1) = pc->colors_[i](1) / 255.0;
                pc->colors_[i](2) = pc->colors_[i](2) / 255.0;

            }
            y = (0.2126 * pc->colors_[i](0)) + (0.7152 * pc->colors_[i](1)) + (0.0722 * pc->colors_[i](2));
            cb  = (pc->colors_[i](2)-y) / 1.8556;
            cr = (pc->colors_[i](0)-y) / 1.5748;
            pc->colors_[i](0) = y;
            pc->colors_[i](1) = cb + 0.5;  // we sum 0.5 in order not to have negative values.
            pc->colors_[i](2) = cr + 0.5;  // we sum 0.5 in order not to have negative values.

            //            if (y > 1.0 || y < 0 || cb + 0.5 > 1.0 || cb + 0.5 < 0 || cr + 0.5 > 1.0 || cr + 0.5 < 0)
            //              fprintf(stderr, "----> colour mais %f %f %f\n", pc->colors_[i](0), pc->colors_[i](1), pc->colors_[i](2)); 
        }
    }

    if (convert_rgb_to_gray)
    {
        double y;
        for (size_t i = 0; i < pc->points_.size(); i++)
        {
            if (divide_color_by_255)
            {
                pc->colors_[i](0) = pc->colors_[i](0) / 255.0;
                pc->colors_[i](1) = pc->colors_[i](1) / 255.0;
                pc->colors_[i](2) = pc->colors_[i](2) / 255.0;

            }
            y = (0.2126 * pc->colors_[i](0)) + (0.7152 * pc->colors_[i](1)) + (0.0722 * pc->colors_[i](2));
            pc->colors_[i](0) = y;
            pc->colors_[i](1) = y;
            pc->colors_[i](2) = y;
        }
    }

    if (!convert_rgb_to_gray && !convert_rgb_to_yuv && divide_color_by_255)
    {
        for (size_t i = 0; i < pc->points_.size(); i++)
        {
            pc->colors_[i](0) = pc->colors_[i](0) / 255.0;
            pc->colors_[i](1) = pc->colors_[i](1) / 255.0;
            pc->colors_[i](2) = pc->colors_[i](2) / 255.0;
        }
    }

    if (estimate_normals)
    {
        geometry::KDTreeFlann kdtree;
        kdtree.SetGeometry(*pc);

        double close_nn = 9999999;
        double distant_nn = 0;
        double average_dist = 0;
        for (size_t i = 0; i < pc->points_.size(); i++)
        {
            std::vector<int> indices_vec(2);
            std::vector<double> dists_vec(2);

            kdtree.SearchKNN(pc->points_[i], 2, indices_vec, dists_vec);

            Eigen::Vector3d point_close = pc->points_[i];
            Eigen::Vector3d point_distant = pc->points_[indices_vec[1]];

            double dist = sqrt ( (pow((point_distant[0] - point_close[0]), 2)) +
                                 (pow((point_distant[1] - point_close[1]), 2)) +
                                 (pow((point_distant[2] - point_close[2]), 2)) );

            average_dist += dist;

            if (dist < close_nn)
            {
                close_nn = dist;
            }
            if (dist > distant_nn)
            {
                distant_nn = dist;
            }
        }
        average_dist /= pc->points_.size();


        // explicar isso aqui ...
        pc->EstimateNormals(geometry::KDTreeSearchParamHybrid(average_dist * 10, 12)); // radius, max_nn
        // pc->OrientNormalsConsistentTangentPlane(48);
        pc->NormalizeNormals();
    }

#if  !((OPEN3D_VERSION_MAJOR == 0 ) &&  (OPEN3D_VERSION_MINOR < 10))
        io::WritePointCloudOption pc_params = io::WritePointCloudOption (true, false, false,  NULL);
#endif

    fprintf(stderr, "Writing to %s\n", argv[argc-1]);
    if ( strstr(argv[argc-1], ".ply" ))
#if ((OPEN3D_VERSION_MAJOR == 0 ) &&  (OPEN3D_VERSION_MINOR < 10))
        io::WritePointCloudToPLY(argv[argc-1], *pc, true, false); //
#else
        io::WritePointCloudToPLY(argv[argc-1], *pc, pc_params); //
#endif
    if( strstr(argv[argc-1], ".xyz"))
#if ((OPEN3D_VERSION_MAJOR == 0 ) &&  (OPEN3D_VERSION_MINOR < 10))
        io::WritePointCloudToXYZ(argv[argc-1], *pc, true, false);
#else
        io::WritePointCloudToXYZ(argv[argc-1], *pc, pc_params);
#endif
    if( strstr(argv[argc-1], ".xyzrgb"))
#if ((OPEN3D_VERSION_MAJOR == 0 ) &&  (OPEN3D_VERSION_MINOR < 10))
        io::WritePointCloudToXYZRGB(argv[argc-1], *pc, true, false);
#else
        io::WritePointCloudToXYZRGB(argv[argc-1], *pc, pc_params);
#endif
    if( strstr(argv[argc-1], ".pcd"))
#if ((OPEN3D_VERSION_MAJOR == 0 ) &&  (OPEN3D_VERSION_MINOR < 10))
        io::WritePointCloudToPCD(argv[argc-1], *pc, true, false);
#else
        io::WritePointCloudToPCD(argv[argc-1], *pc, pc_params);
#endif


    return EXIT_SUCCESS;

}
