/*
 * Copyright (C) 2019-2021 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */

#include <unistd.h>
#include <cstdint>
#include <sstream>
#include <iostream>
#include <memory>
#include <thread>

#include <Open3D.h>

#include "metric.h"
#include "pc_utils.h"

#include "ColorSpace/ColorSpace.h"
#include "ColorSpace/Comparison.h"

using namespace open3d;
using namespace std;

int main(int argc, char *argv[])
{

    // auto pc = geometry::TriangleMesh::CreateCylinder(2.25, 5.5, 50, 10);

    Eigen::Vector3d color_1;
    color_1(0) = 0.5;
    color_1(1) = 0.5;
    color_1(2) = 0.5;

    Eigen::Vector3d color_2;
    color_2(0) = 0.3;
    color_2(1) = 0.3;
    color_2(2) = 0.3;

    double heights[10];
    double radiuses[10];
    heights[0] = 5.5;
    heights[1] = 5.0;
    heights[2] = 4.5;
    heights[3] = 4.0;
    heights[4] = 3.5;
    heights[5] = 3.0;
    heights[6] = 2.5;
    heights[7] = 2.0;
    heights[8] = 1.5;
    heights[9] = 1.0;

    double radius = 2.75;
    double height  = 5.5;
    int resolution = 100;
    int split = 20;


    auto mesh_ptr = std::make_shared<geometry::TriangleMesh>();


    mesh_ptr->vertices_.resize(resolution * (split + 1) + 2);
    mesh_ptr->vertex_colors_.resize(resolution * (split + 1) + 2);

    mesh_ptr->vertices_[0] = Eigen::Vector3d(0.0, 0.0, height * 0.5);
    mesh_ptr->vertices_[1] = Eigen::Vector3d(0.0, 0.0, -height * 0.5);

    mesh_ptr->vertex_colors_[0] = color_2;
    mesh_ptr->vertex_colors_[1] = color_2;


    double step = M_PI * 2.0 / (double)resolution;
    double h_step = height / (double)split;
    for (int i = 0; i <= split; i++) {
        for (int j = 0; j < resolution; j++) {
            double theta = step * j;
            mesh_ptr->vertices_[2 + resolution * i + j] =
                    Eigen::Vector3d(cos(theta) * radius, sin(theta) * radius,
                                    height * 0.5 - h_step * i);
            mesh_ptr->vertex_colors_[2 + resolution * i + j] = color_1;
        }
    }
    for (int j = 0; j < resolution; j++) {
        int j1 = (j + 1) % resolution;
        int base = 2;
        mesh_ptr->triangles_.push_back(Eigen::Vector3i(0, base + j, base + j1));
        base = 2 + resolution * split;
        mesh_ptr->triangles_.push_back(Eigen::Vector3i(1, base + j1, base + j));
    }
    for (int i = 0; i < split; i++) {
        int base1 = 2 + resolution * i;
        int base2 = base1 + resolution;
        for (int j = 0; j < resolution; j++) {
            int j1 = (j + 1) % resolution;
            mesh_ptr->triangles_.push_back(
                    Eigen::Vector3i(base2 + j, base1 + j1, base1 + j));
            mesh_ptr->triangles_.push_back(
                    Eigen::Vector3i(base2 + j, base2 + j1, base1 + j1));
        }
    }


#if 0
    Eigen::Vector3d color;
    color(0) = 0.5;
    color(1) = 0.5;
    color(2) = 0.5;
    pc->PaintUniformColor(color);
#endif

    auto pc = mesh_ptr;

    double trans_1 = 0;
    double trans_2 = 0;
    double trans_3 = 0;

    double theta_x = M_PI / 2;
    double theta_y = 0;
    double theta_z = 0;

    double scale = 1.0;

    Eigen::Affine3d transform = Eigen::Affine3d::Identity();
    transform.translation() << trans_1, trans_2, trans_3;
    transform.rotate (Eigen::AngleAxisd (theta_z, Eigen::Vector3d::UnitZ()));
    transform.rotate (Eigen::AngleAxisd (theta_y, Eigen::Vector3d::UnitY()));
    transform.rotate (Eigen::AngleAxisd (theta_x, Eigen::Vector3d::UnitX()));
    transform.scale (scale);

    pc->Transform(transform.matrix());


    io::WriteTriangleMesh("out.ply",
                       *pc,
                       true,
                       false,
                       true,
                       true);



//    visualization::DrawGeometries({pc},"Cylinder",  640, 480, 200, 200);

    // CreatesCylinder(double radius = 1.0,
    //             double height = 2.0,
    //             int resolution = 20,
    //             int split = 4);
    // auto vis = std::make_shared<visualization::Visualizer>();

//    vis->CreateVisualizerWindow("GPDS PC Viewer", 800, 800, 50, 50, true);

//    vis->AddGeometry({pc});

#if 0

    while (vis)
    {
        vis->UpdateGeometry();
        vis->PollEvents();
        vis->UpdateRender();
    }
    vis->CaptureScreenImage("out.png", true);
#endif

    return EXIT_SUCCESS;
}
