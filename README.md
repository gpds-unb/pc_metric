pc_metric
===

[TOC]

# Scope of the project

This project aims to create a good metric for volumetric image quality assessment.


# Licence

[![License: GPL v2](https://img.shields.io/badge/License-GPL%20v2-blue.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)


# Prerequisites

- Compiler
  - C++ 17 compatible compiler. Tested with GCC version 9 or greater.
- Open3D library
    - [http://www.open3d.org/](http://www.open3d.org/)
    - Compatible with version 0.9.0 and greater.
- FASM
  - [https://flatassembler.net/](https://flatassembler.net/)


## Build instructions (Debian 11 / Ubuntu 22.04)

  ```bash
  ~$ sudo apt-get install libopen3d-dev libeigen3-dev libpng-dev  zlib1g-dev fasm
  ~$ git clone --recursive https://gitlab.com/gpds-unb/pc_metric.git
  ~$ cd pc_metric
  ~/pc_metric$ make
  ```  


##  Citation

If you use this code for your research, please cite our paper.

```
@article{diniz2021point,
  title={Point Cloud Quality Assessment Based on Geometry-aware Texture Descriptors},
  author={Diniz, Rafael and Freitas, Pedro G and Farias, Mylene C.Q.},
  journal={Computers \& Graphics},
  year={2021},
  publisher={Elsevier}
}
```
