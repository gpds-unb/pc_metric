// https://github.com/lovasko/libcsv
/*
Copyright (c) 2015 Daniel Lovasko
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 
*/


#ifndef CSV_H
#define CSV_H

#include <stdlib.h>
#include <unistd.h>

#define CSV_OK                 0
#define CSV_END                1
#define CSV_E_NULL             2
#define CSV_E_NEWLINE          3
#define CSV_E_FIELD_COUNT      4
#define CSV_E_IO               5
#define CSV_E_TOO_FEW_FIELDS   6
#define CSV_E_TOO_MANY_FIELDS  7
#define CSV_E_NOT_ENOUGH_LINES 8
#define CSV_E_INVALID_CODE     9
#define CSV_E_MAX              9

#define __CSV_BUF_SIZE 65536

struct csv {
	char buffer[__CSV_BUF_SIZE];
	char** fields;
	size_t field_count;
	ssize_t i;
	ssize_t bytes_read;
	int fd;
	char sep;
	char padding[3];
};

int csv_open(struct csv* file, char* path, char separator, size_t field_count);
int csv_read_record(struct csv* file, char*** out_fields);
int csv_error_string(int code, char** out_error_string);
int csv_close(struct csv* file);

#endif

