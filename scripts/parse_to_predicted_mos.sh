#!/bin/bash

DATA_PATH=/mnt/ssd/RafaelDiniz/queiroz
MOS_INPUT=scores-mean.txt
LBP_INPUT=scores.csv
OUTPUT_DATA=data.csv
OUTPUT_DIR=/mnt/ssd/RafaelDiniz/queiroz-processed


for j in a b c d e f; do

    echo "distancia,MOS" > ${OUTPUT_DIR}/${j}/${OUTPUT_DATA}
    paste -d, ${OUTPUT_DIR}/${j}/${LBP_INPUT} ${DATA_PATH}/${MOS_INPUT} >> ${OUTPUT_DIR}/${j}/${OUTPUT_DATA}

done;


echo "distancia,MOS" > ${OUTPUT_DIR}/angular_data.csv
paste -d, ${OUTPUT_DIR}/angular_scores-mean.txt ${DATA_PATH}/${MOS_INPUT} >> ${OUTPUT_DIR}/angular_data.csv


for j in mpeg-ptplaneh.txt mpeg-ptplanemse.txt mpeg-ptpointh.txt mpeg-ptpointmse.txt mpeg-yh.txt mpeg-ymse.txt; do
    echo "distancia,MOS" > ${OUTPUT_DIR}/$(basename ${j} .txt)_data.csv
    paste -d, ${OUTPUT_DIR}/${j} ${DATA_PATH}/${MOS_INPUT} >> ${OUTPUT_DIR}/$(basename ${j} .txt)_data.csv
done;
