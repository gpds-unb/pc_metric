#!/bin/sh

# our library
. ./lbp_functions.sh

# PREFIX=/usr
PREFIX=/home/rafael2k

PC_METRIC_BIN=${PREFIX}/bin/pc_metric4fix
CORRELATE_BIN=${PREFIX}/bin/correlate
COMPARE_HISTOGRAM_BIN=${PREFIX}/bin/compare_histogram
VOXELIZE_BIN=${PREFIX}/bin/optimize_voxel_size
PEDRO_BIN=/home/rafael2k/pc_metric/scripts/predicted_mos.py

DATA_PATH=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/reference_APSIPA
CORRELATION_OUTPUT=correlation.csv
SCORE_OUTPUT=scores.csv

OUTPUT_DIR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results

# for python pre-processing step
PY_OUTPUT_DATA=data.csv
PY_OUTPUT_DATA_PREFIX=data
PY_MOS_INPUT=results_mean.txt
PY_CORRELATION_OUTPUT=correlation-py.csv
PY_CORRELATION_OUTPUT_PREFIX=correlation

VOXEL_INFO=voxelization.csv


RUN_NV=1
RUN_ST1=0
RUN_ST2=0
RUN_ST3=0
RUN_ST4=1

if [ $RUN_NV -eq 1 ]
then
    OUTPUT_DIR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results_m4_novoxel

    create_directories
    calculate_histogram
    calculate_histogram_difference
    #correlate_python

fi


if [ $RUN_ST1 -eq 1 ]
then
    
    VOXEL_STRATEGY=1
    for VOXEL_BIAS in 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80; do
        OUTPUT_DIR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results_m4_strategy1_${VOXEL_BIAS}

        create_directories
        calculate_histogram_voxelized
        calculate_histogram_difference
        #correlate_python
    done;
fi

if [ $RUN_ST2 -eq 1 ]
then

    VOXEL_STRATEGY=2
    for VOXEL_BIAS in 0.7 1 1.3 1.6 1.9 2.2 2.5 2.8 3.1 3.4 3.7 4.0 4.3 4.6 4.9 5.2; do
        OUTPUT_DIR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results_m4_strategy2_${VOXEL_BIAS}

        create_directories
        calculate_histogram_voxelized
        calculate_histogram_difference
        # correlate_queiroz

    done;
fi

if [ $RUN_ST3 -eq 1 ]
then

    VOXEL_STRATEGY=3
    for VOXEL_BIAS in 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80; do
        OUTPUT_DIR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results_m4_strategy3_${VOXEL_BIAS}

        create_directories
        calculate_histogram_auto_voxelized
        calculate_histogram_difference
        # correlate_queiroz

    done;
fi

if [ $RUN_ST4 -eq 1 ]
then

    VOXEL_STRATEGY=4
    #    for VOXEL_BIAS in 0.7 1 1.3 1.6 1.9 2.2 2.5 2.8 3.1 3.4 3.7 4.0 4.3 4.6 4.9 5.2; do
    for VOXEL_BIAS in 3.4; do
        OUTPUT_DIR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results_m4_strategy4_${VOXEL_BIAS}

        create_directories
        calculate_histogram_auto_voxelized
        calculate_histogram_difference
        # correlate_queiroz
    done;
fi
