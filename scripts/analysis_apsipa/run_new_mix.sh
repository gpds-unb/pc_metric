#!/bin/sh

# our library
. ./lbp_functions.sh

# PREFIX=/usr
PREFIX=/home/rafael2k

FITTING_BIN=/home/rafael2k/pc_metric/scripts/fitting.py
PEDRO_BIN=/home/rafael2k/pc_metric/scripts/predicted_mos.py

MPEG_PCC_BIN=/home/rafael2k/metrics/mpeg-pcc-dmetric/test/pc_error
PC_CONVERT_BIN=${PREFIX}/bin/pc_convert
MATLAB_BIN=/mnt/hd/RafaelDiniz/MATLAB/R2018a/bin/matlab

CORRELATION_OUTPUT=correlation.csv
SCORE_OUTPUT=scores.csv

OUTPUT_DIR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results

# for python pre-processing step
PY_OUTPUT_DATA=data.csv
PY_MOS_INPUT=results_mean.txt
PY_CORRELATION_OUTPUT=correlation-py.csv


DATA_PATH=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/reference_APSIPA
NORMALS_DIR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/normals
DATA_PATH_XYZ=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/xyz

MPEG_SCORE_OUTPUT=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-all.txt

PTPOINT_MSE=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-ptpointmse.txt
PTPOINT_MSE_PSNR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-ptpointmsepsnr.txt

PTPOINT_H=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-ptpointh.txt
PTPOINT_H_PSNR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-ptpointhpsnr.txt

PTPLANE_MSE=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-ptplanemse.txt
PTPLANE_MSE_PSNR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-ptplanemsepsnr.txt

PTPLANE_H=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-ptplaneh.txt
PTPLANE_H_PSNR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-ptplanehpsnr.txt

COLOR_TEMP=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-temp.txt

COLOR_MSE_Y=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-ymse.txt
COLOR_PSNR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-ypsnr.txt
COLOR_H_Y=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-yh.txt
COLOR_H_PSNR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/mpeg-hpsnr.txt

ANGULAR_SCRIPT_TEMP=angular_script.m
ANGULAR_SCORE_OUTPUT_MSE=angular_scores-mse.csv
ANGULAR_SCORE_OUTPUT_RMS=angular_scores-rms.csv


METRIC_LBP_FIT=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/lbp-fit.txt
METRIC_1_FIT=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/m1-fit.txt
METRIC_2_FIT=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/m2-fit.txt
METRIC_3_FIT=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/m3-fit.txt
METRICS_MERGED=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/merged-fit.txt
NEW_MIX_SCORE=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/net_mix_score.txt
NEW_MIX_CORRELATION=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results/net_mix_correlation.txt

mkdir -p ${OUTPUT_DIR}

#create_normals
#mpeg_pcc_metrics
#parse_mpeg_metrics


# angular_similarity

#correlate_mpeg_python

new_metric_avg
correlate_new_metric
