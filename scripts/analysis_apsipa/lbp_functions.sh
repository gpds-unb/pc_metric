#!/bin/bash

create_directories ()
{

mkdir -p ${OUTPUT_DIR}

mkdir -p ${OUTPUT_DIR}/a
mkdir -p ${OUTPUT_DIR}/b
mkdir -p ${OUTPUT_DIR}/c
mkdir -p ${OUTPUT_DIR}/d
mkdir -p ${OUTPUT_DIR}/e
mkdir -p ${OUTPUT_DIR}/f

}

# PC_METRIC_BIN
# DATA_PATH
# OUTPUT_DIR
# VOXEL_SIZE
calculate_histogram ()
{

    cd ${DATA_PATH}/

    for j in a b c d e f; do
        for i in $(ls -1 *.ply); do
            $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/$(basename ${i} .ply)-hist.bin -${j} > ${OUTPUT_DIR}/${j}/$(basename $i .ply)-hist.gnuplot
        done;
    done;

}


# PC_METRIC_BIN
# DATA_PATH
# OUTPUT_DIR
# VOXEL_SIZE
calculate_histogram_auto_voxelized ()
{

  cd ${DATA_PATH}/

  rm -f ${OUTPUT_DIR}/${VOXEL_INFO}
  
  for j in a b c d e f; do

      for i in $(ls -1 *amphoriskos*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${VOXEL_BIAS} -s ${VOXEL_STRATEGY} -o ${OUTPUT_DIR}/${VOXEL_INFO} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

      for i in $(ls -1 *biplane*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${VOXEL_BIAS} -s ${VOXEL_STRATEGY} -o ${OUTPUT_DIR}/${VOXEL_INFO} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

      for i in $(ls -1 *head*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${VOXEL_BIAS} -s ${VOXEL_STRATEGY} -o ${OUTPUT_DIR}/${VOXEL_INFO} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;
      
      for i in $(ls -1 *longdress*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${VOXEL_BIAS} -s ${VOXEL_STRATEGY} -o ${OUTPUT_DIR}/${VOXEL_INFO} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;
      
      for i in $(ls -1 *loot*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${VOXEL_BIAS} -s ${VOXEL_STRATEGY} -o ${OUTPUT_DIR}/${VOXEL_INFO} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

      for i in $(ls -1 *redandblack*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${VOXEL_BIAS} -s ${VOXEL_STRATEGY} -o ${OUTPUT_DIR}/${VOXEL_INFO} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;
      
      for i in $(ls -1 *romanoillamp*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${VOXEL_BIAS} -s ${VOXEL_STRATEGY} -o ${OUTPUT_DIR}/${VOXEL_INFO} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

      for i in $(ls -1 *soldier*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${VOXEL_BIAS} -s ${VOXEL_STRATEGY} -o ${OUTPUT_DIR}/${VOXEL_INFO} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

      for i in $(ls -1 *the20smaria*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${VOXEL_BIAS} -s ${VOXEL_STRATEGY} -o ${OUTPUT_DIR}/${VOXEL_INFO} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

  done;

}


# PC_METRIC_BIN
# DATA_PATH
# OUTPUT_DIR
# VOXEL_SIZE
calculate_histogram_voxelized ()
{

  cd ${DATA_PATH}/

  AMPHORISKOS=`$VOXELIZE_BIN ${VOXEL_STRATEGY} ${VOXEL_BIAS} *amphoriskos*`
  BIPLANE=`$VOXELIZE_BIN ${VOXEL_STRATEGY} ${VOXEL_BIAS} *biplane*`
  HEAD=`$VOXELIZE_BIN ${VOXEL_STRATEGY} ${VOXEL_BIAS} *head*`
  LONGDRESS=`$VOXELIZE_BIN ${VOXEL_STRATEGY} ${VOXEL_BIAS} *longdress*`
  LOOT=`$VOXELIZE_BIN ${VOXEL_STRATEGY} ${VOXEL_BIAS} *loot*`
  REDANDBLACK=`$VOXELIZE_BIN ${VOXEL_STRATEGY} ${VOXEL_BIAS} *redandblack*`
  ROMANOILLAMP=`$VOXELIZE_BIN ${VOXEL_STRATEGY} ${VOXEL_BIAS} *romanoillamp*`
  SOLDIER=`$VOXELIZE_BIN ${VOXEL_STRATEGY} ${VOXEL_BIAS} *soldier*`
  THE20SMARIA=`$VOXELIZE_BIN ${VOXEL_STRATEGY} ${VOXEL_BIAS} *the20smaria*`

  rm -f ${OUTPUT_DIR}/${VOXEL_INFO}
  
  echo "Amphoriskos,${AMPHORISKOS}" >> ${OUTPUT_DIR}/${VOXEL_INFO}
  echo "Biplane,${BIPLANE}" >> ${OUTPUT_DIR}/${VOXEL_INFO}
  echo "Head,${HEAD}" >> ${OUTPUT_DIR}/${VOXEL_INFO}
  echo "Longdress,${LONGDRESS}" >> ${OUTPUT_DIR}/${VOXEL_INFO}
  echo "Loot,${LOOT}" >> ${OUTPUT_DIR}/${VOXEL_INFO}
  echo "RedAndBlack,${REDANDBLACK}" >> ${OUTPUT_DIR}/${VOXEL_INFO}
  echo "RomanOilLamp,${ROMANOILLAMP}" >> ${OUTPUT_DIR}/${VOXEL_INFO}
  echo "Soldier,${SOLDIER}" >> ${OUTPUT_DIR}/${VOXEL_INFO}
  echo "The20sMaria,${THE20SMARIA}" >> ${OUTPUT_DIR}/${VOXEL_INFO}

  for j in a b c d e f; do
      
      for i in $(ls -1 *amphoriskos*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${AMPHORISKOS} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

      for i in $(ls -1 *biplane*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${BIPLANE} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

      for i in $(ls -1 *head*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${HEAD} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

      for i in $(ls -1 *longdress*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${LONGDRESS} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;
      
      for i in $(ls -1 *loot*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${LOOT} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

      for i in $(ls -1 *redandblack*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${REDANDBLACK} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;
      
      for i in $(ls -1 *romanoillamp*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${ROMANOILLAMP} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

      for i in $(ls -1 *soldier*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${SOLDIER} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

      for i in $(ls -1 *the20smaria*.ply); do
          $PC_METRIC_BIN -i ${i} -h ${OUTPUT_DIR}/${j}/${i%.*}-hist.bin -${j} -v ${THE20SMARIA} > ${OUTPUT_DIR}/${j}/${i%.*}-hist.gnuplot
      done;

  done;

}

# input
# COMPARE_HISTOGRAM_BIN
# OUTPUT_DIR
# SCORE_OUTPUT
calculate_histogram_difference ()
{

    for j in a b c d e f; do
        cd ${OUTPUT_DIR}/${j}

        rm -f ${SCORE_OUTPUT}

        for i in $(ls -1 amphoriskos*.bin biplane*.bin head*.bin longdress*.bin loot*.bin romanoillamp*.bin soldier*.bin the20smaria*.bin); do

            # echo ${COMPARE_HISTOGRAM_BIN} ${OUTPUT_DIR}/${j}/${i} ${OUTPUT_DIR}/${j}/${i}
            ${COMPARE_HISTOGRAM_BIN} ${OUTPUT_DIR}/${j}/${i} ${OUTPUT_DIR}/${j}/${i} >> ${SCORE_OUTPUT}
            echo >> ${SCORE_OUTPUT}

            for k in *$(echo ${i}| cut -d _ -f 1)*octree-predlift*.bin *$(echo ${i}| cut -d _ -f 1)*octree-raht*.bin *$(echo ${i}| cut -d _ -f 1)*trisoup-predlift*.bin *$(echo ${i}| cut -d _ -f 1)*trisoup-raht*.bin tmc2_$(echo ${i}| cut -d _ -f 1)*.bin; do

                # echo ${COMPARE_HISTOGRAM_BIN} ${OUTPUT_DIR}/${j}/${i} ${OUTPUT_DIR}/${j}/${k}
                ${COMPARE_HISTOGRAM_BIN} ${OUTPUT_DIR}/${j}/${i} ${OUTPUT_DIR}/${j}/${k} >> ${SCORE_OUTPUT}
                echo >> ${SCORE_OUTPUT}
            done;

        done;

    done;
}

# CORRELATE_BIN
# DATA_PATH
# OUTPUT_DIR
# CORRELATION_OUTPUT
correlate()
{
    for j in a b c d e f; do
        
        ${CORRELATE_BIN} -s 42 -n 50 ${DATA_PATH}/../scores/subjective/scores_ubi_uc_unin.csv ${OUTPUT_DIR}/${j}/${SCORE_OUTPUT} > ${OUTPUT_DIR}/${j}/${CORRELATION_OUTPUT}

    done;
}

# OUTPUT_DIR
# OUTPUT_DATA
# DATA_PATH
correlate_python()
{

    for j in a b c d e f; do
        
        echo "distancia,MOS" > ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA}
        paste -d, ${OUTPUT_DIR}/${j}/${SCORE_OUTPUT} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA}

    done;

    for j in a b c d e f; do
        ${PEDRO_BIN} ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA} > ${OUTPUT_DIR}/${j}/${PY_CORRELATION_OUTPUT}
        ${FITTING_BIN} ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA} | sed 's/\[//g' | sed 's/\]//g' | sed 's/ //g' > ${OUTPUT_DIR}/${j}/${METRIC_FITTED}
    done;


}

# OUTPUT_DIR
# OUTPUT_DATA
# DATA_PATH
correlate_python_separate()
{
    for j in a b c d e f; do
        
        echo "distancia,MOS" > ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA_PREFIX}_ubi.csv
        paste -d, ${OUTPUT_DIR}/${j}/${SCORE_OUTPUT} ${DATA_PATH}/../scores/subjective/UBI/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA_PREFIX}_ubi.csv
        
        echo "distancia,MOS" > ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA_PREFIX}_uc.csv
        paste -d, ${OUTPUT_DIR}/${j}/${SCORE_OUTPUT} ${DATA_PATH}/../scores/subjective/UC/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA_PREFIX}_uc.csv

        echo "distancia,MOS" > ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA_PREFIX}_unin.csv
        paste -d, ${OUTPUT_DIR}/${j}/${SCORE_OUTPUT} ${DATA_PATH}/../scores/subjective/UNIN/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA_PREFIX}_unin.csv
        
    done;

    for j in a b c d e f; do
        ${PEDRO_BIN} ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA_PREFIX}_ubi.csv > ${OUTPUT_DIR}/${j}/${PY_CORRELATION_OUTPUT_PREFIX}_ubi.csv
        ${PEDRO_BIN} ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA_PREFIX}_uc.csv > ${OUTPUT_DIR}/${j}/${PY_CORRELATION_OUTPUT_PREFIX}_uc.csv
        ${PEDRO_BIN} ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA_PREFIX}_unin.csv > ${OUTPUT_DIR}/${j}/${PY_CORRELATION_OUTPUT_PREFIX}_unin.csv
    done;


}


# CREATE_NORMALS_BIN
# NORMALS_DIR
create_normals()
{
    mkdir -p ${NORMALS_DIR}

    cd ${DATA_PATH}/
    
    for i in $(ls -1 *.ply); do
        ${PC_CONVERT_BIN} -y -n ${i} ${NORMALS_DIR}/${i}
    done;

}


# MPEG_PCC_BIN
# MPEG_SCORE_OUTPUT
# NORMALS_DIR
# DATA_PATH
mpeg_pcc_metrics()
{

    cd ${NORMALS_DIR}

    rm -f ${MPEG_SCORE_OUTPUT}

    for i in $(ls -1 amphoriskos*.ply biplane*.ply head*.ply longdress*.ply loot*.ply romanoillamp*.ply soldier*.ply the20smaria*.ply); do

        # echo ${MPEG_PCC_BIN} -a ${NORMALS_DIR}/${i} -b ${NORMALS_DIR}/${i} -c 1 -d 1 --nbThreads=16
        ${MPEG_PCC_BIN} -a ${NORMALS_DIR}/${i} -b ${NORMALS_DIR}/${i} -c 1 -d 1 --nbThreads=16 >> ${MPEG_SCORE_OUTPUT}
        
        echo >> ${MPEG_SCORE_OUTPUT}

        for k in *$(echo ${i}| cut -d _ -f 1)*octree-predlift*.ply *$(echo ${i}| cut -d _ -f 1)*octree-raht*.ply *$(echo ${i}| cut -d _ -f 1)*trisoup-predlift*.ply *$(echo ${i}| cut -d _ -f 1)*trisoup-raht*.ply tmc2_$(echo ${i}| cut -d _ -f 1)*.ply; do


            ${MPEG_PCC_BIN} -a ${NORMALS_DIR}/${i} -b ${NORMALS_DIR}/${k} -c 1 -d 1 --nbThreads=16 >> ${MPEG_SCORE_OUTPUT}
            # echo ${MPEG_PCC_BIN} -a ${NORMALS_DIR}/${i} -b ${NORMALS_DIR}/${k} -c 1 -d 1 --nbThreads=16
            echo >> ${MPEG_SCORE_OUTPUT}
        done;

    done;

}

parse_mpeg_metrics()
{

    cat ${MPEG_SCORE_OUTPUT} | grep mseF | grep -v PSNR | grep p2point | cut -f 11 -d " " > ${PTPOINT_MSE}

    cat ${MPEG_SCORE_OUTPUT} | grep mseF | grep PSNR | grep p2point | cut -f 6 -d " " > ${PTPOINT_MSE_PSNR}
    
    cat ${MPEG_SCORE_OUTPUT} | grep mseF | grep -v PSNR | grep p2plane | cut -f 11 -d " " > ${PTPLANE_MSE}

    cat ${MPEG_SCORE_OUTPUT} | grep mseF | grep PSNR | grep p2plane | cut -f 6 -d " " > ${PTPLANE_MSE_PSNR}

    cat ${MPEG_SCORE_OUTPUT} | grep h. | grep p2point | grep -v PSNR | grep -v "1(" | grep -v "2("  | cut -d ":" -f 2 | cut -f 2 -d " " > ${PTPOINT_H}

    cat ${MPEG_SCORE_OUTPUT} | grep h. | grep p2point | grep PSNR | grep -v "1(" | grep -v "2("  | cut -d ":" -f 2 | cut -f 2 -d " " > ${PTPOINT_H_PSNR}
  
    cat ${MPEG_SCORE_OUTPUT} | grep h. | grep p2plane | grep -v PSNR | grep -v "1(" | grep -v "2("  | cut -d ":" -f 2 | cut -f 2 -d " " > ${PTPLANE_H}

    cat ${MPEG_SCORE_OUTPUT} | grep h. | grep p2plane | grep PSNR | grep -v "1(" | grep -v "2("  | cut -d ":" -f 2 | cut -f 2 -d " " > ${PTPLANE_H_PSNR}

    
    cat ${MPEG_SCORE_OUTPUT} | grep "c" | grep F| grep -v h | grep -v PSNR | grep ":" | cut -d ":" -f 2 | cut -d " " -f 2 > ${COLOR_TEMP}

    rm -f ${COLOR_MSE_Y}

    while read a; do 
        read b;
        read c;
        awk -v a=$a -v b=$b -v c=$c 'BEGIN{print (((6*a) + b + c)/8)}\' >> ${COLOR_MSE_Y}
    done < ${COLOR_TEMP}

    
    cat ${MPEG_SCORE_OUTPUT} | grep "c" | grep F| grep -v h | grep PSNR | grep ":" | cut -d ":" -f 2 | cut -d " " -f 2 > ${COLOR_TEMP}

    rm -f ${COLOR_PSNR}

    while read a; do 
        read b;
        read c;
        awk -v a=$a -v b=$b -v c=$c 'BEGIN{print (((6*a) + b + c)/8)}\' >> ${COLOR_PSNR}
    done < ${COLOR_TEMP}

    
    cat ${MPEG_SCORE_OUTPUT} | grep "c" | grep F| grep h | grep -v PSNR | grep ":" | cut -d ":" -f 2 | cut -d " " -f 2 > ${COLOR_TEMP}

    rm -f ${COLOR_H_Y}

    while read a; do 
        read b;
        read c;
        awk -v a=$a -v b=$b -v c=$c 'BEGIN{print (((6*a) + b + c)/8)}\' >> ${COLOR_H_Y}
    done < ${COLOR_TEMP}

    
    cat ${MPEG_SCORE_OUTPUT} | grep "c" | grep F| grep h | grep PSNR | grep ":" | cut -d ":" -f 2 | cut -d " " -f 2 > ${COLOR_TEMP}

    rm -f ${COLOR_H_PSNR}

    while read a; do 
        read b;
        read c;
        awk -v a=$a -v b=$b -v c=$c 'BEGIN{print (((6*a) + b + c)/8)}\' >> ${COLOR_H_PSNR}
    done < ${COLOR_TEMP}

}

correlate_mpeg_python()
{
    
    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPOINT_MSE} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

#    echo "Dataset, Approach, Variant, PCC, SROCC, RMSE" > ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    echo -n "EPFL \& UnB Alexiou, Point-to-point, po2point\$_{MSE}\$, " > ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPOINT_MSE_PSNR} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-po2point\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPOINT_H} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, po2point\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    
    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPOINT_H_PSNR} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-po2point\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    
    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${COLOR_MSE_Y} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, Color-YCbCr\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${COLOR_PSNR} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-Color-YCbCr\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    
    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${COLOR_H_Y} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, Color-YCbCr\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${COLOR_H_PSNR} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-Color-YCbCr\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPLANE_MSE} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",Point-to-plane, po2plane\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPLANE_MSE_PSNR} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-po2plane\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPLANE_H} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, po2plane\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPLANE_H_PSNR} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-po2plane\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_MSE} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",Plane-to-plane, pl2plane\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_RMS} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, pl2plane\$_{RMS}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${PEDRO_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    
}

angular_similarity()
{

    rm -f ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    rm -f ${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_RMS}
    rm -f ${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_MSE}

    cd ${DATA_PATH_XYZ}

    echo "fileIDMSE = fopen('${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_MSE}','a');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "fileIDRMS = fopen('${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_RMS}','a');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}

    for i in $(ls -1 amphoriskos*.xyz biplane*.xyz head*.xyz longdress*.xyz loot*.xyz romanoillamp*.xyz soldier*.xyz the20smaria*.xyz); do


    echo "xyzPoints1 = load('${DATA_PATH_XYZ}/${i}');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "xyzPoints2 = load('${DATA_PATH_XYZ}/${i}');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "normals1 = pcnormals(ptCloud1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1,'Normal',normals1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "normals2 = pcnormals(ptCloud2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2,'Normal',normals2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
  
    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'MSE');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n "fprintf(fileIDMSE,'%.10f" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}

    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'RMS');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n "fprintf(fileIDRMS,'%.10f" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}

    
        for k in *$(echo ${i}| cut -d _ -f 1)*octree-predlift*.xyz *$(echo ${i}| cut -d _ -f 1)*octree-raht*.xyz *$(echo ${i}| cut -d _ -f 1)*trisoup-predlift*.xyz *$(echo ${i}| cut -d _ -f 1)*trisoup-raht*.xyz tmc2_$(echo ${i}| cut -d _ -f 1)*.xyz; do
    echo "xyzPoints1 = load('${DATA_PATH_XYZ}/${i}');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "xyzPoints2 = load('${DATA_PATH_XYZ}/${k}');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "normals1 = pcnormals(ptCloud1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1,'Normal',normals1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "normals2 = pcnormals(ptCloud2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2,'Normal',normals2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    
    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'MSE');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n "fprintf(fileIDMSE,'%.10f" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}

    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'RMS');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n "fprintf(fileIDRMS,'%.10f" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}


  done;

done;

echo "fclose(fileIDMSE);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
echo "fclose(fileIDRMS);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}

echo "run: cd ${OUTPUT_DIR}"
echo "run: ${MATLAB_BIN} -nodisplay -r \"run ${ANGULAR_SCRIPT_TEMP},quit\""


}

# our MOSp
# MOSp of 2 other good metrics
new_metric_avg()
{
## our LBP
    j=f
    
    echo "distancia,MOS" > ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA}
    paste -d, ${OUTPUT_DIR}/${j}/${SCORE_OUTPUT} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA}

    ${FITTING_BIN} ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA} | sed 's/\[//g' | sed 's/\]//g' | sed 's/ //g' > ${METRIC_LBP_FIT}


##  PTPLANE_MSE
    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPLANE_MSE} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    ${FITTING_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} | sed 's/\[//g' | sed 's/\]//g' | sed 's/ //g' > ${METRIC_1_FIT}

##  PTPOINT_MSE
    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPOINT_MSE} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    ${FITTING_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} | sed 's/\[//g' | sed 's/\]//g' | sed 's/ //g' > ${METRIC_2_FIT}

##  COLOR_MSE_Y
    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${COLOR_MSE_Y} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    ${FITTING_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} | sed 's/\[//g' | sed 's/\]//g' | sed 's/ //g' > ${METRIC_3_FIT}

    rm -f ${NEW_MIX_SCORE}

    # averaging...
    paste -d"\n" ${METRIC_LBP_FIT} ${METRIC_1_FIT} ${METRIC_2_FIT} > ${METRICS_MERGED}
    while read a; do 
        read b;
        read c;
        awk -v a=$a -v b=$b -v c=$c 'BEGIN{print ((a + b + c)/3)}\' >> ${NEW_MIX_SCORE}
        #        awk -v a=$a -v b=$b 'BEGIN{print ((a + b)/2)}\' >> ${NEW_MIX_SCORE}
    done < ${METRICS_MERGED}

}

correlate_new_metric()
{

    echo "distancia,MOS" > ${NEW_MIX_CORRELATION}
    paste -d, ${NEW_MIX_SCORE} ${DATA_PATH}/../scores/${PY_MOS_INPUT} >> ${NEW_MIX_CORRELATION}

    ${PEDRO_BIN} ${NEW_MIX_CORRELATION}

}
