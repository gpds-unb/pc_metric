#!/bin/sh

# our library
. ./lbp_functions.sh

# PREFIX=/usr
PREFIX=/home/rafael2k

PC_METRIC_BIN=${PREFIX}/bin/pc_metric4
CORRELATE_BIN=${PREFIX}/bin/correlate
COMPARE_HISTOGRAM_BIN=${PREFIX}/bin/compare_histogram
VOXELIZE_BIN=${PREFIX}/bin/optimize_voxel_size
PEDRO_BIN=/home/rafael2k/pc_metric/scripts/predicted_mos.py

DATA_PATH=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/reference_APSIPA
CORRELATION_OUTPUT=correlation.csv
SCORE_OUTPUT=scores.csv

OUTPUT_DIR=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results

# for python pre-processing step
PY_OUTPUT_DATA=data.csv
PY_OUTPUT_DATA_PREFIX=data
PY_MOS_INPUT=results_mean.txt
PY_CORRELATION_OUTPUT=correlation-py.csv
PY_CORRELATION_OUTPUT_PREFIX=correlation


mkdir -p ${OUTPUT_DIR}

mkdir -p ${OUTPUT_DIR}/a
mkdir -p ${OUTPUT_DIR}/b
mkdir -p ${OUTPUT_DIR}/c
mkdir -p ${OUTPUT_DIR}/d
mkdir -p ${OUTPUT_DIR}/e
mkdir -p ${OUTPUT_DIR}/f

#calculate_histogram

# this is the default
#calculate_histogram_voxelized

# last!
#calculate_histogram_auto_voxelized

#calculate_histogram_difference
correlate_python

