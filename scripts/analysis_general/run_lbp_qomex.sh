#!/bin/sh

# our library
. ./pc_functions.sh

# PREFIX=/usr
PREFIX=/home/rafael2k

CSV=/mnt/ssd/RafaelDiniz/QoMEX_2019/scores/scores.csv

PC_METRIC_BIN=${PREFIX}/bin/pc_metric_journal
COMPARE_HISTOGRAM_BIN=${PREFIX}/bin/compare_histogram_journal_split
VOXELIZE_BIN=${PREFIX}/bin/optimize_voxel_size

MOSP_BIN=/home/rafael2k/pc_metric/scripts/predicted_mos.py
DISTANCE_BIN=/home/rafael2k/pc_metric/scripts/statistics_temp/script_compute_distances.py
SINGLE_DB_LOGISTIC=/home/rafael2k/pc_metric/scripts/statistics_temp/script_simulation_singledb_logistic.py
SINGLE_DB_REGRESSORS=/home/rafael2k/pc_metric/scripts/statistics_temp/script_simulation_singledb_Regressors.py
LINEPLOT_CORRELATION=/home/rafael2k/pc_metric/scripts/statistics_temp/script_lineplot_correlation_per_k.py

DATA_PATH=/mnt/ssd/RafaelDiniz/QoMEX_2019/contents

VOXEL_INFO=voxelization.csv

OUTPUT_DIR=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-journal-knn
OUTPUT_DIR_SDB_LOGISTIC=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-journal/sdb-logistic
OUTPUT_DIR_SDB_REGRESSORS=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-journal/sdb-regressors

SCORE_OUTPUT=raw_scores

# for python pre-processing step
PY_TEMP_DATA=data-temp.csv
PY_TEMP_DATA2=data-temp2.csv
PY_CORRELATION_OUTPUT=correlation.csv

#switches to enable or disable each metric
M0=1
M1=1
M2=1
M3=1
M4=1
M5=1
M6=1
M7=1
M8=1
M9=1
M10=1

METRICS_LIST="0 1 2 3 4 5 6 7 8 9 10"

NEIGHBOURHOOD_LIST=6,7,8,9,10,11,12

VOXEL_SIZES="0.7 1.0 1.3 1.6 2.0 3.0 4.5 6.0 7.5"

## calculation starts here

#create_directories

#generate_fv_no_voxelize_no_split

## put target voxel sizes
#get_voxel_size_knn
##get_voxel_size

#generate_fv_no_split

pre_process_data

calculate_distance

compute_single_db_simulations_distances_only_logistic

# compute_single_db_simulations_distances_only_regressors

#plot_correlation_per_k_logistic

#plot_correlation_per_k_regressors
