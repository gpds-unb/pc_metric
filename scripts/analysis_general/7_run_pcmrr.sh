#!/bin/sh

# our library
. ./pc_functions.sh

# PREFIX=/usr
PREFIX=/home/rafael2k

CORRELATE_BIN=${PREFIX}/bin/correlate
COMPARE_HISTOGRAM_BIN=${PREFIX}/bin/compare_histogram
VOXELIZE_BIN=${PREFIX}/bin/optimize_voxel_size
MOSP_BIN=/home/rafael2k/pc_metric/scripts/predicted_mos.py
#MOSP_BIN=/home/rafael2k/pc_metric/scripts/statistics_fixed/correlation_no_fitting.py
SINGLE_DB_LOGISTIC=/home/rafael2k/pc_metric/scripts/statistics_fixed/script_simulation_singledb_logistic.py
LINEPLOT_CORRELATION=/home/rafael2k/pc_metric/scripts/statistics_fixed/script_plot_pcmrr.py


# MPEG_PCC_BIN=/home/rafael2k/Point_to_distribution_metric/build/pc_error
PC_CONVERT_BIN=${PREFIX}/bin/pc_convert
NORMALS_BIN=${PREFIX}/bin/create_normals
MATLAB_BIN=/home/rafael2k/MATLAB/R2018a/bin/matlab

# scores in csv format
D1_MOS=/mnt/ssd/RafaelDiniz/queiroz/scores.csv
D2_MOS=/mnt/ssd/RafaelDiniz/QoMEX_2019/scores/scores.csv
D3_MOS=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/scores/scores.csv
D4_MOS=/mnt/ssd/RafaelDiniz/icip2020pc/AllLabsResultsSummary-noref.csv
D5_MOS=/mnt/ssd/RafaelDiniz/sjtu-PCQA/mos-finalized.csv

D1_DATAPATH=/mnt/ssd/RafaelDiniz/queiroz/testing
D2_DATAPATH=/mnt/ssd/RafaelDiniz/QoMEX_2019/contents
D3_DATAPATH=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/reference_APSIPA
D4_DATAPATH=/mnt/ssd/RafaelDiniz/icip2020pc/contents
D5_DATAPATH=/mnt/ssd/RafaelDiniz/sjtu-PCQA/contents

D1_RESULTS=/mnt/ssd/RafaelDiniz/queiroz/pcmrr-metrics
D2_RESULTS=/mnt/ssd/RafaelDiniz/QoMEX_2019/pcmrr-metrics
D3_RESULTS=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/pcmrr-metrics
D4_RESULTS=/mnt/ssd/RafaelDiniz/icip2020pc/pcmrr-metrics
D5_RESULTS=/mnt/ssd/RafaelDiniz/sjtu-PCQA/pcmrr-metrics

CORRELATION_OUTPUT=correlation.csv
SCORE_OUTPUT=scores.csv


PCMRR_RAW_DISTANCE=simulation.csv
PCMRR_SCRIPT=script.m
PCMRR_RUNPATH=/home/rafael2k/metrics/PCM_RR

PY_MOS_INPUT=scores-mean.txt
PY_CORRELATION_OUTPUT=correlation-py.csv
PY_LOGISTIC_CORRELATION_OUTPUT=simulation.csv

DATASETS="1 3 4 5"

# data-sets...
#for i in ${DATASETS}; do
for i in 3; do

#  mkdir -p $( eval "echo \$D${i}_RESULTS" )

  DATA_PATH=$( eval "echo \$D${i}_DATAPATH" )
  OUTPUT_DIR=$( eval "echo \$D${i}_RESULTS" )

  echo ${OUTPUT_DIR}

  NORMALS_DIR=${DATA_PATH}/../with_normals
  NORMALS_YCBCR_DIR=${DATA_PATH}/../with_normals_ycbcr


#  echo ${NORMALS_DIR}
#  ls ${NORMALS_DIR}

#  mkdir -p ${NORMALS_DIR}

  CSV=$( eval "echo \$D${i}_MOS" )
#  echo ${CSV}

  create_directories

  pcmrr_metric


#  plot_correlation_logistic_pcmrr_metric

done
