#!/bin/bash

export NUMEXPR_MAX_THREADS=32

pcmrr_metric ()
{

    cd ${PCMRR_RUNPATH}

    rm -f ${OUTPUT_DIR}/${PCMRR_RAW_DISTANCE}

    echo "simulation,nome_do_pc,nome_referencia,MOS,d_pcmrr" > ${OUTPUT_DIR}/${PCMRR_RAW_DISTANCE}

    rm -f ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}

    echo "k = 9;" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}
    echo "fileout = fopen('${OUTPUT_DIR}/${PCMRR_RAW_DISTANCE}','a');" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}


    COUNTER=0

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            continue
        fi


        reference_pc_name=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
        test_pc_name=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')
        mos=$(echo $line | awk -F',' '{printf "%s", $3}' | tr -d '"')

        reference_pc=${DATA_PATH}/${reference_pc_name}
        test_pc=${DATA_PATH}/${test_pc_name}

        echo ${reference_pc}
        echo ${test_pc}

        echo "ang_sim_ref = [];" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}
        echo "ang_sim_dis = [];" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}

        echo "refPC = pcread('${reference_pc}');" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}
        echo "disPC = pcread('${test_pc}');" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}


        echo "refF = getFeatures(refPC, ang_sim_ref, k);" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}
        echo "disF = getFeatures(disPC, ang_sim_dis, k);" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}

        echo "feats = double(abs(refF - disF));" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}

        echo "load('./weights.mat', 'alpha');" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}

        echo "d = feats'*alpha;" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}

        echo "fprintf(fileout, '${COUNTER},${test_pc_name},${reference_pc_name},${mos},%.10f\\\n', d);" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}
        echo "" >> ${PCMRR_RUNPATH}/${PCMRR_SCRIPT}

        COUNTER=$((COUNTER+1))
    done < $CSV

    ${MATLAB_BIN} -nodisplay -r "run ${PCMRR_SCRIPT},quit"

}

plot_correlation_logistic_pcmrr_metric ()
{
  echo ${OUTPUT_DIR}

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric SROCC \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-srocc.png \
          --output_correlation ${OUTPUT_DIR}/correlation-srocc.csv

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric PCC \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-pcc.png \
          --output_correlation ${OUTPUT_DIR}/correlation-pcc.csv

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric RMSE \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-rmse.png \
          --output_correlation ${OUTPUT_DIR}/correlation-rmse.csv

}

pointssim_metric_averaging ()
{

  TEMP_FILE=${POINTSSIM_RAW_DISTANCE}-temp

  echo "simulation,nome_do_pc,nome_referencia,MOS,d_ssim_color,d_ssim_geo,d_ssim_mix" > ${OUTPUT_DIR}/${TEMP_FILE}

  while read -r line
  do
    field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
    if [ "$field1" = "simulation" ]; then # this is to skip the first line
      continue
    fi

    field2=$(echo $line | awk -F',' '{printf "%s", $2}' | tr -d '"')
    field3=$(echo $line | awk -F',' '{printf "%s", $3}' | tr -d '"')
    field4=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

    field5=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
    field6=$(echo $line | awk -F',' '{printf "%s", $6}' | tr -d '"')

    echo -n "${field1},${field2},${field3},${field4},${field5},${field6}," >> ${OUTPUT_DIR}/${TEMP_FILE}

    awk -v a=${field5} -v b=${field6} 'BEGIN{print ((a + b)/2)}\' >> ${OUTPUT_DIR}/${TEMP_FILE}

  done < ${OUTPUT_DIR}/${POINTSSIM_RAW_DISTANCE}

  mv ${OUTPUT_DIR}/${TEMP_FILE} ${OUTPUT_DIR}/${POINTSSIM_RAW_DISTANCE}

}


pointssim_metric ()
{

  cd ${POINTSSIM_RUNPATH}

  rm -f ${OUTPUT_DIR}/${POINTSSIM_RAW_DISTANCE}

  echo "simulation,nome_do_pc,nome_referencia,MOS,d_ssim_color,d_ssim_geo" > ${OUTPUT_DIR}/${POINTSSIM_RAW_DISTANCE}

  rm -f ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}

  echo "fileout = fopen('${OUTPUT_DIR}/${POINTSSIM_RAW_DISTANCE}','a');" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}

  COUNTER=0

  while read -r line
  do
    field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
    if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
      continue
    fi

    reference_pc_name=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
    test_pc_name=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')
    mos=$(echo $line | awk -F',' '{printf "%s", $3}' | tr -d '"')

    reference_pc=${DATA_PATH}/${reference_pc_name}
    test_pc=${DATA_PATH}/${test_pc_name}

#    echo -n "${COUNTER}," >> ${OUTPUT_DIR}/${POINTSSIM_RAW_DISTANCE}
#    echo -n "${test_pc_name}," >> ${OUTPUT_DIR}/${POINTSSIM_RAW_DISTANCE}
#    echo -n "${reference_pc_name}," >> ${OUTPUT_DIR}/${POINTSSIM_RAW_DISTANCE}
#    echo -n "${mos}," >> ${OUTPUT_DIR}/${POINTSSIM_RAW_DISTANCE}


    echo "a = pcread('${reference_pc}');" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}
    echo "b = pcread('${test_pc}');" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}

    echo "pcA.geom = a.Location;" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}
    echo "pcA.color = a.Color;" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}

    echo "pcB.geom = b.Location;" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}
    echo "pcB.color = b.Color;" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}


    echo "PARAMS.ATTRIBUTES.GEOM = true;" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}
    echo "PARAMS.ATTRIBUTES.NORM = false;" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}
    echo "PARAMS.ATTRIBUTES.CURV = false;" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}
    echo "PARAMS.ATTRIBUTES.COLOR = true;" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}

    echo "PARAMS.ESTIMATOR_TYPE = {'VAR'};" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}
    echo "PARAMS.POOLING_TYPE = {'Mean'};" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}
    echo "PARAMS.NEIGHBORHOOD_SIZE = 12;" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}
    echo "PARAMS.CONST = eps(1);" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}
    echo "PARAMS.REF = 0;" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}

    echo "[pointssim] = pc_ssim(pcA, pcB, PARAMS);" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}


    echo "fprintf(fileout, '${COUNTER},${test_pc_name},${reference_pc_name},${mos},%.10f,%.10f\\\n', pointssim.colorSym, pointssim.geomSym);" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}
    echo "" >> ${POINTSSIM_RUNPATH}/${POINTSSIM_SCRIPT}

    COUNTER=$((COUNTER+1))

  done < $CSV

  ${MATLAB_BIN} -nodisplay -r "run ${POINTSSIM_SCRIPT},quit"

}

pcqm_metric ()
{

    # PCQM needs files in hardcoded local build directory... mamma mia
    cd ${PCQM_RUNPATH}

    rm -f ${OUTPUT_DIR}/${PCQM_RAW_DISTANCE}

    echo "simulation,nome_do_pc,nome_referencia,MOS,d_pcqm" > ${OUTPUT_DIR}/${PCQM_RAW_DISTANCE}

    COUNTER=0

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            continue
        fi


        reference_pc_name=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
        test_pc_name=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')
        mos=$(echo $line | awk -F',' '{printf "%s", $3}' | tr -d '"')

        reference_pc=${DATA_PATH}/${reference_pc_name}
        test_pc=${DATA_PATH}/${test_pc_name}

	echo ${reference_pc}
	echo ${test_pc}

	echo -n "${COUNTER}," >> ${OUTPUT_DIR}/${PCQM_RAW_DISTANCE}
	echo -n "${test_pc_name}," >> ${OUTPUT_DIR}/${PCQM_RAW_DISTANCE}
	echo -n "${reference_pc_name}," >> ${OUTPUT_DIR}/${PCQM_RAW_DISTANCE}
	echo -n "${mos}," >> ${OUTPUT_DIR}/${PCQM_RAW_DISTANCE}
	
	${PCQM_BIN} ${reference_pc} ${test_pc} -fq | grep "PCQM value" | cut -f 5 -d" " >> ${OUTPUT_DIR}/${PCQM_RAW_DISTANCE}

	COUNTER=$((COUNTER+1))
    done < $CSV
    
}

plot_correlation_logistic_pointssim_metric()
{
  echo ${OUTPUT_DIR}

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric SROCC \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-srocc.png \
          --output_correlation ${OUTPUT_DIR}/correlation-srocc.csv

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric PCC \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-pcc.png \
          --output_correlation ${OUTPUT_DIR}/correlation-pcc.csv

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric RMSE \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-rmse.png \
          --output_correlation ${OUTPUT_DIR}/correlation-rmse.csv

}

plot_correlation_logistic_pcqm_metric()
{
  echo ${OUTPUT_DIR}

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric SROCC \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-srocc.png \
          --output_correlation ${OUTPUT_DIR}/correlation-srocc.csv

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric PCC \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-pcc.png \
          --output_correlation ${OUTPUT_DIR}/correlation-pcc.csv

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric RMSE \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-rmse.png \
          --output_correlation ${OUTPUT_DIR}/correlation-rmse.csv

}



# RESULTS_OTHER, OUTPUT_DIR
parse_mpeg_correlation ()
{

  paste -d"\n" ${RESULTS_OTHER}/correlation-pcc.csv ${RESULTS_OTHER}/correlation-srocc.csv ${RESULTS_OTHER}/correlation-rmse.csv  > ${OUTPUT_DIR}/mpeg/correlation-temp.csv

  echo "DB & Metric & PCC & SROCC & RMSE \\\\" > ${OUTPUT_DIR}/mpeg/correlation-final.csv

  while read -r pcc
  do
    read -r srocc
    read -r rmse

    field1=$(echo $pcc | awk -F',' '{printf "%s", $1}' | tr -d '"')
    if [ "$field1" = "Distance" ]; then # this is to skip the first line
      continue
    fi

    field2=$(echo $pcc | awk -F',' '{printf "%.3f", $2}' | tr -d '"' )
    field3=$(echo $srocc | awk -F',' '{printf "%.3f", $2}' | tr -d '"') # isnt it wrong???
    field4=$(echo $rmse | awk -F',' '{printf "%.3f", $2}' | tr -d '"')

    echo "& ${field1} & ${field2} & ${field3} & ${field4} \\\\" >> ${OUTPUT_DIR}/mpeg/correlation-final.csv

  done < ${OUTPUT_DIR}/mpeg/correlation-temp.csv

  rm ${OUTPUT_DIR}/mpeg/correlation-temp.csv

}

parse_hybrid_correlation ()
{

  vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"

  for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
  do

    paste -d"\n" ${RESULTS_HYBRID}/correlation-n${neighbors}-pcc.csv ${RESULTS_HYBRID}/correlation-n${neighbors}-srocc.csv ${RESULTS_HYBRID}/correlation-n${neighbors}-rmse.csv > ${RESULTS_HYBRID}/correlation-n${neighbors}-temp.csv

    echo "DB & Metric & PCC & SROCC & RMSE \\\\" > ${OUTPUT_DIR}/hybrid/correlation-n${neighbors}-final.csv

    while read -r pcc
    do
      read -r srocc
      read -r rmse

      field1=$(echo $pcc | awk -F',' '{printf "%s", $1}' | tr -d '"')
      if [ "$field1" = "voxel param" ]; then # this is to skip the first line
        continue
      fi

      field2=$(echo $pcc | awk -F',' '{printf "%.3f", $3}' | tr -d '"' )
      field3=$(echo $srocc | awk -F',' '{printf "%.3f", $3}' | tr -d '"')
      field4=$(echo $rmse | awk -F',' '{printf "%.3f", $3}' | tr -d '"')

      echo "& ${field1} & ${field2} & ${field3} & ${field4} \\\\" >> ${OUTPUT_DIR}/hybrid/correlation-n${neighbors}-final.csv

    done < ${RESULTS_HYBRID}/correlation-n${neighbors}-temp.csv

    rm ${RESULTS_HYBRID}/correlation-n${neighbors}-temp.csv

  done


}

create_directories ()
{
    mkdir -p ${OUTPUT_DIR}
}

get_voxel_size_knn ()
{
    cd ${DATA_PATH}

    output_dir=${OUTPUT_DIR}
    reference_pc=""

    echo "Cleaning previous voxel size values"
    rm -rf ${output_dir}/*.vsize

    mkdir -p ${output_dir}

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            continue
        fi

        old_ref=${reference_pc}

        reference_pc=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
        test_pc=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

        if [ "${reference_pc}" = "${old_ref}" ]; then
            skip_ref=true;
        else
            skip_ref=false;
        fi

        for KNOB in ${VOXEL_SIZES}; do
            if [ $skip_ref = false ]; then
                ${VOXELIZE_BIN} 3 ${KNOB} ${reference_pc} >> ${output_dir}/$(basename ${reference_pc} .ply).vsize
            fi
            ${VOXELIZE_BIN} 3 ${KNOB} ${test_pc} >> ${output_dir}/$(basename ${test_pc} .ply).vsize

        done;

    done < $CSV


}

get_voxel_size ()
{
    cd ${DATA_PATH}

    output_dir=${OUTPUT_DIR}
    reference_pc=""

    echo "Cleaning results directory"
    rm -rf ${output_dir}/*.vsize

    mkdir -p ${output_dir}

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            continue
        fi

        old_ref=${reference_pc}

        reference_pc=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
        test_pc=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

        if [ "${reference_pc}" = "${old_ref}" ]; then
            skip_ref=true;
        else
            skip_ref=false;
        fi

        for KNOB in ${VOXEL_SIZES}; do
            if [ $skip_ref = false ]; then
                ${VOXELIZE_BIN} 2 ${KNOB} ${reference_pc} >> ${output_dir}/$(basename ${reference_pc} .ply).vsize
            fi
            ${VOXELIZE_BIN} 2 ${KNOB} ${test_pc} >> ${output_dir}/$(basename ${test_pc} .ply).vsize

        done;

    done < $CSV


}

generate_fv_no_voxelize ()
{

    cd ${DATA_PATH}

    output_dir=${OUTPUT_DIR}
    reference_pc=""

    echo "Cleaning results directory"
    rm -rf ${output_dir}/results-novox.csv

    mkdir -p ${output_dir}

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            continue
        fi

        old_ref=${reference_pc}

        reference_pc=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
        test_pc=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

        if [ "${reference_pc}" = "${old_ref}" ]; then
            skip_ref=true;
        else
            skip_ref=false;
        fi

        if [ $skip_ref = false ]; then
            ${PC_METRIC_BIN} -s -i ${reference_pc} -h ${output_dir}/results-novox -m $M0,$M1,$M2,$M3,$M4,$M5,$M6,$M7,$M8,$M9,$M10,$M11,$M12,$M13,$M14,$M15 -n ${NEIGHBOURHOOD_LIST}
        fi

        ${PC_METRIC_BIN} -s -i ${test_pc} -h ${output_dir}/results-novox -m $M0,$M1,$M2,$M3,$M4,$M5,$M6,$M7,$M8,$M9,$M10,$M11,$M12,$M13,$M14,$M15 -n ${NEIGHBOURHOOD_LIST}


    done < $CSV

}


generate_fv_no_voxelize_no_split()
{

    cd ${DATA_PATH}

    output_dir=${OUTPUT_DIR}
    reference_pc=""

    echo "Cleaning results directory"
    rm -rf ${output_dir}/results-novox.csv

    mkdir -p ${output_dir}

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            continue
        fi

        old_ref=${reference_pc}

        reference_pc=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
        test_pc=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

        if [ "${reference_pc}" = "${old_ref}" ]; then
            skip_ref=true;
            echo skip
        else
            skip_ref=false;
            echo no skip
        fi

        if [ $skip_ref = false ]; then
            ${PC_METRIC_BIN} -i ${reference_pc} -h ${output_dir}/results-novox.csv -m $M0,$M1,$M2,$M3,$M4,$M5,$M6,$M7,$M8,$M9,$M10,$M11,$M12,$M13,$M14,$M15 -n ${NEIGHBOURHOOD_LIST}
        fi

        ${PC_METRIC_BIN} -i ${test_pc} -h ${output_dir}/results-novox.csv -m $M0,$M1,$M2,$M3,$M4,$M5,$M6,$M7,$M8,$M9,$M10,$M11,$M12,$M13,$M14,$M15 -n ${NEIGHBOURHOOD_LIST}

    done < $CSV

}

generate_fv ()
{

    cd ${DATA_PATH}

    output_dir=${OUTPUT_DIR}
    reference_pc=""

    echo "Cleaning results directory"
    rm -rf ${output_dir}/results-k*.csv

    mkdir -p ${output_dir}

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            continue
        fi

        old_ref=${reference_pc}

        reference_pc=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
        test_pc=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

        if [ "${reference_pc}" = "${old_ref}" ]; then
            skip_ref=true;
        else
            skip_ref=false;
        fi

        if [ $skip_ref = false ]; then
            COUNTER=1
            while read -r vox_knob
            do
                knob_idx=`echo ${VOXEL_SIZES} | cut -d " " -f ${COUNTER}`
                ${PC_METRIC_BIN} -s -i ${reference_pc} -h ${output_dir}/results-k${knob_idx} -m $M0,$M1,$M2,$M3,$M4,$M5,$M6,$M7,$M8,$M9,$M10,$M11,$M12,$M13,$M14,$M15 -n ${NEIGHBOURHOOD_LIST} -v ${vox_knob}
                COUNTER=$((COUNTER+1))
            done < ${output_dir}/$(basename ${reference_pc} .ply).vsize
        fi

        COUNTER=1
        while read -r vox_knob
        do
            knob_idx=`echo ${VOXEL_SIZES} | cut -d " " -f ${COUNTER}`
            ${PC_METRIC_BIN} -s -i ${test_pc} -h ${output_dir}/results-k${knob_idx} -m $M0,$M1,$M2,$M3,$M4,$M5,$M6,$M7,$M8,$M9,$M10,$M11,$M12,$M13,$M14,$M15 -n ${NEIGHBOURHOOD_LIST} -v ${vox_knob}
            COUNTER=$((COUNTER+1))
        done < ${output_dir}/$(basename ${test_pc} .ply).vsize

    done < $CSV

}

generate_fv_no_split ()
{

    cd ${DATA_PATH}

    output_dir=${OUTPUT_DIR}
    reference_pc=""

    echo "Cleaning results directory"
    rm -rf ${output_dir}/results-k*.csv

    mkdir -p ${output_dir}

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            continue
        fi

        old_ref=${reference_pc}

        reference_pc=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
        test_pc=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

        if [ "${reference_pc}" = "${old_ref}" ]; then
            skip_ref=true;
        else
            skip_ref=false;
        fi

        if [ $skip_ref = false ]; then
            COUNTER=1
            while read -r vox_knob
            do
                knob_idx=`echo ${VOXEL_SIZES} | cut -d " " -f ${COUNTER}`
                ${PC_METRIC_BIN} -i ${reference_pc} -h ${output_dir}/results-k${knob_idx}.csv -m $M0,$M1,$M2,$M3,$M4,$M5,$M6,$M7,$M8,$M9,$M10,$M11,$M12,$M13,$M14,$M15 -n ${NEIGHBOURHOOD_LIST} -v ${vox_knob}
                COUNTER=$((COUNTER+1))
            done < ${output_dir}/$(basename ${reference_pc} .ply).vsize
        fi

        COUNTER=1
        while read -r vox_knob
        do
            knob_idx=`echo ${VOXEL_SIZES} | cut -d " " -f ${COUNTER}`
            ${PC_METRIC_BIN} -i ${test_pc} -h ${output_dir}/results-k${knob_idx}.csv -m $M0,$M1,$M2,$M3,$M4,$M5,$M6,$M7,$M8,$M9,$M10,$M11,$M12,$M13,$M14,$M15 -n ${NEIGHBOURHOOD_LIST} -v ${vox_knob}
            COUNTER=$((COUNTER+1))
        done < ${output_dir}/$(basename ${test_pc} .ply).vsize

    done < $CSV

}


# old
calculate_histogram_voxelized ()
{
    cd ${DATA_PATH}

    while read -r line
    do
      field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
      if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
        continue
      fi

      reference_pc=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
      test_pc=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

      j=f

      if [ ! -f "${OUTPUT_DIR}/${j}/$(dirname "${reference_pc}")/$(basename ${reference_pc} .ply).bin" ]; then
        output_dir="${OUTPUT_DIR}/${j}/$(dirname "${reference_pc}")"
        mkdir -p "${output_dir}"
        ${PC_METRIC_BIN} -i ${reference_pc} -h ${output_dir}/$(basename ${reference_pc} .ply).bin -${j} -v 20 > ${output_dir}/$(basename ${reference_pc} .ply).gnuplot
        #                ${PC_METRIC_BIN} -i ${reference_pc} -h ${output_dir}/$(basename ${reference_pc} .ply).bin -${j} > ${output_dir}/$(basename ${reference_pc} .ply).gnuplot
      fi

      output_dir="${OUTPUT_DIR}/${j}/$(dirname "${test_pc}")"
      mkdir -p "${output_dir}"
      ${PC_METRIC_BIN} -i ${test_pc} -h ${output_dir}/$(basename ${test_pc} .ply).bin -${j} -v 20 > ${output_dir}/$(basename ${test_pc} .ply).gnuplot
      #            ${PC_METRIC_BIN} -i ${test_pc} -h ${output_dir}/$(basename ${test_pc} .ply).bin -${j} > ${output_dir}/$(basename ${test_pc} .ply).gnuplot  

    done < $CSV

}


calculate_histogram_difference ()
{

    cd ${OUTPUT_DIR}/
    rm -f ${SCORE_OUTPUT}*.csv

    vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"
#    echo $vox_idx_list

    for KNOB in ${vox_idx_list}; do
      ${COMPARE_HISTOGRAM_BIN} -i ${CSV} -f results-${KNOB} -n ${NEIGHBOURHOOD_LIST} -m $M0,$M1,$M2,$M3,$M4,$M5,$M6,$M7,$M8,$M9,$M10 -o ${SCORE_OUTPUT}-${KNOB}
    done;

}


# PEDRO_BIN
# DATA_PATH
# OUTPUT_DIR
# CORRELATION_OUTPUT
correlate_mpeg_python()
{
    rm -f ${DATA_PATH}/${PY_MOS_INPUT}

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            echo ${line} >> ${SCORE_OUTPUT}
            continue
        fi

        mos=$(echo $line | awk -F',' '{printf "%s", $3}' | tr -d '"')
        echo ${mos} >> ${DATA_PATH}/${PY_MOS_INPUT}

    done < ${CSV}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPOINT_MSE} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo "Dataset, Approach, Variant, PCC, SROCC, RMSE" > ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    echo -n ", Point-to-point, po2point\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPOINT_MSE_PSNR} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-po2point\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPOINT_H} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, po2point\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPOINT_H_PSNR} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-po2point\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${COLOR_MSE_Y} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, Color-YCbCr\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${COLOR_PSNR} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-Color-YCbCr\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${COLOR_H_Y} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, Color-YCbCr\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${COLOR_H_PSNR} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-Color-YCbCr\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPLANE_MSE} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",Point-to-plane, po2plane\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPLANE_MSE_PSNR} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-po2plane\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPLANE_H} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, po2plane\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPLANE_H_PSNR} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo -n ",, PSNR-po2plane\$_{Hausdorff}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}


#    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
#    paste -d, ${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_MSE} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

#    echo -n ",Plane-to-plane, pl2plane\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
#    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

#    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
#    paste -d, ${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_RMS} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

#    echo -n ",, pl2plane\$_{RMS}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
#    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

}


correlate_mpeg_logistic()
{
    echo -n "nome_do_pc,nome_referencia,subjective_MOS," > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    echo "d_ptpointmse,d_ptpointmsepsnr,d_ptpointh,d_ptpointhpsnr,d_colormsey,d_colorpsnr,d_colorhy,d_colorhpsnr,d_ptplanemse,d_ptplanemsepsnr,d_ptplaneh,d_ptplanehpsnr"  >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

    i=1

    while read -r line
    do


        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            continue
        fi

        field2=$(echo $line | awk -F',' '{printf "%s", $2}' | tr -d '"')

        mos=$(echo $line | awk -F',' '{printf "%s", $3}' | tr -d '"')


        echo -n "${field1},${field2},${mos}," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${PTPOINT_MSE} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${PTPOINT_MSE_PSNR} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${PTPOINT_H} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${PTPOINT_H_PSNR} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${COLOR_MSE_Y} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${COLOR_PSNR} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${COLOR_H_Y} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${COLOR_H_PSNR} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${PTPLANE_MSE} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${PTPLANE_MSE_PSNR} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${PTPLANE_H} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

        tail -n+${i} ${PTPLANE_H_PSNR} | head -n1 | tr -d '\n' >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
        echo "" >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

# fill here with the remaining metrics...

        i=$((i+1))

    done < ${CSV}


    python3 ${SINGLE_DB_LOGISTIC} \
         --raw_distances_file ${OUTPUT_DIR}/${PY_OUTPUT_DATA} \
         --output_file ${OUTPUT_DIR}/${PY_LOGISTIC_CORRELATION_OUTPUT}


#    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
#    paste -d, ${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_MSE} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

#    echo -n ",Plane-to-plane, pl2plane\$_{MSE}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
#    ${LOGISTIC_CORRELATION_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

#    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
#    paste -d, ${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_RMS} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}

#    echo -n ",, pl2plane\$_{RMS}\$, " >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
#    ${LOGISTIC_CORRELATION_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

}


correlate_no_fitting()
{


  for metric in ${METRICS_LIST};
  do
    for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
    do
      INPUT_DIR=${OUTPUT_DIR}/metric-${metric}/neighbors-${neighbors}/
      output_dir=${OUTPUT_DIR_SDB_LOGISTIC}/metric-${metric}/neighbors-${neighbors}/

      mkdir -p ${output_dir}

      vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"
      for k in ${vox_idx_list}
      do
        input_distances=${INPUT_DIR}/distances-${k}.csv
        output=${output_dir}/correlation-${k}.csv
        python3 ${MOSP_BIN} \
            --raw_distances_file ${input_distances} \
            --output_file ${output}
      done
    done
  done


}


correlate_no_fitting_hybrid()
{

  vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"

  correlation_output=${OUTPUT_DIR_HYBRID_DIR}/correlations.csv

  echo "n,k,PCC,SROCC,RMSE" > ${correlation_output}

  for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
  do

    for k in ${vox_idx_list}
    do

      simulation_input=${OUTPUT_DIR_HYBRID_DIR}/hybrid-n${neighbors}-${k}.csv

      echo -n "${neighbors},${k}," >> ${correlation_output}

 #     echo "${CORRELATE_HYBRID}"

      python3 ${CORRELATE_HYBRID} ${simulation_input} >> ${correlation_output}
#      echo "" >> ${correlation_output}
    done
  done


}

# MOSP_BIN
# OUTPUT_DIR
# OUTPUT_DATA
# DATA_PATH
correlate_python()
{
  csv_temp=${OUTPUT_DIR}/csv.temp

  sed '1d' ${CSV} > ${csv_temp}

  echo "Metric, PCC, SROCC, RMSE" > ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

  for i in $(ls -1 ${OUTPUT_DIR}/${SCORE_OUTPUT}*); do

    base_name=${i##*/}
    echo ${base_name}
    features=${base_name%.csv}
    # this is way ad-hoc!
    name=$(echo ${features} | cut -c 12-)
    echo ${name}

    paste -d"\n" ${i} ${csv_temp} > ${OUTPUT_DIR}/${PY_TEMP_DATA2}

    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_TEMP_DATA}

    tic=tic
    while read -r line
    do
      field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
      if [ "$field1" = "SIGNAL" ]; then # this is to skip the first lines
        continue
      fi
      if [ "$tic" = "tic" ]; then
        score=$(echo $line | awk -F',' '{printf "%s", $3}' | tr -d '"')
        echo ${score}
        echo -n ${score} >> ${OUTPUT_DIR}/${PY_TEMP_DATA}
        echo -n "," >> ${OUTPUT_DIR}/${PY_TEMP_DATA}
        tic=toc
      else
        score=$(echo $line | awk -F',' '{printf "%s", $3}' | tr -d '"')
        echo ${score}
        echo ${score} >> ${OUTPUT_DIR}/${PY_TEMP_DATA}
        tic=tic
      fi

    done < ${OUTPUT_DIR}/${PY_TEMP_DATA2};

    echo -n "${name}," >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}
    ${MOSP_BIN} ${OUTPUT_DIR}/${PY_TEMP_DATA} >> ${OUTPUT_DIR}/${PY_CORRELATION_OUTPUT}

  done;
}

# NORMALS_BIN
# NORMALS_DIR
create_normals()
{
    mkdir -p ${NORMALS_DIR}

    cd ${DATA_PATH}

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            echo ${line} >> ${SCORE_OUTPUT}
            continue
        fi

        reference_pc=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
        test_pc=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

        if [ ! -f "${NORMALS_DIR}/${reference_pc}" ]; then
            mkdir -p ${NORMALS_DIR}/$(dirname "${reference_pc}")
            ${NORMALS_BIN} ${reference_pc} ${NORMALS_DIR}/${reference_pc}
        fi

        mkdir -p ${NORMALS_DIR}/$(dirname "${test_pc}")
        ${NORMALS_BIN} ${test_pc} ${NORMALS_DIR}/${test_pc}

    done < ${CSV}
}

# CREATE_NORMALS_BIN
# NORMALS_DIR
# NORMALS_YCBCR_DIR
convert_to_ycbcr()
{
    mkdir -p ${NORMALS_YCBCR_DIR}

    cd ${NORMALS_DIR}

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            continue
        fi

        reference_pc=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
        test_pc=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

        if [ ! -f "${NORMALS_YCBCR_DIR}/${reference_pc}" ]; then
            ${PC_CONVERT_BIN} -y ${reference_pc} ${NORMALS_YCBCR_DIR}/${reference_pc}
        fi

        ${PC_CONVERT_BIN} -y ${test_pc} ${NORMALS_YCBCR_DIR}/${test_pc}

    done < ${CSV}
}


# MPEG_PCC_BIN
# MPEG_SCORE_OUTPUT
# NORMALS_DIR
# DATA_PATH
mpeg_pcc_metrics()
{

    cd ${NORMALS_DIR}

    rm -f ${MPEG_SCORE_OUTPUT}

    mkdir -p $(dirname ${MPEG_SCORE_OUTPUT})

    while read -r line
    do
        field1=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "SIGNAL" ]; then # this is to skip the first line
            continue
        fi

        reference_pc=$(echo $line | awk -F',' '{printf "%s", $5}' | tr -d '"')
        test_pc=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

        echo ${reference_pc}
        echo ${test_pc}

        ${MPEG_PCC_BIN} -a ${reference_pc} -b ${test_pc} -c 1 -d 1 --nbThreads=16 >> ${MPEG_SCORE_OUTPUT}
        echo >> ${MPEG_SCORE_OUTPUT}

    done < ${CSV}

}

parse_mpeg_metrics()
{

    cat ${MPEG_SCORE_OUTPUT} | grep mseF | grep -v PSNR | grep p2point | cut -f 11 -d " " > ${PTPOINT_MSE}

    cat ${MPEG_SCORE_OUTPUT} | grep mseF | grep PSNR | grep p2point | cut -f 6 -d " " > ${PTPOINT_MSE_PSNR}

    cat ${MPEG_SCORE_OUTPUT} | grep mseF | grep -v PSNR | grep p2plane | cut -f 11 -d " " > ${PTPLANE_MSE}

    cat ${MPEG_SCORE_OUTPUT} | grep mseF | grep PSNR | grep p2plane | cut -f 6 -d " " > ${PTPLANE_MSE_PSNR}

    cat ${MPEG_SCORE_OUTPUT} | grep h. | grep p2point | grep -v PSNR | grep -v "1(" | grep -v "2("  | cut -d ":" -f 2 | cut -f 2 -d " " > ${PTPOINT_H}

    cat ${MPEG_SCORE_OUTPUT} | grep h. | grep p2point | grep PSNR | grep -v "1(" | grep -v "2("  | cut -d ":" -f 2 | cut -f 2 -d " " > ${PTPOINT_H_PSNR}

    cat ${MPEG_SCORE_OUTPUT} | grep h. | grep p2plane | grep -v PSNR | grep -v "1(" | grep -v "2("  | cut -d ":" -f 2 | cut -f 2 -d " " > ${PTPLANE_H}

    cat ${MPEG_SCORE_OUTPUT} | grep h. | grep p2plane | grep PSNR | grep -v "1(" | grep -v "2("  | cut -d ":" -f 2 | cut -f 2 -d " " > ${PTPLANE_H_PSNR}

    cat ${MPEG_SCORE_OUTPUT} | grep "c" | grep F| grep -v h | grep -v PSNR | grep ":" | cut -d ":" -f 2 | cut -d " " -f 2 > ${COLOR_TEMP}

    rm -f ${COLOR_MSE_Y}

    while read a; do
        read b;
        read c;
        awk -v a=$a -v b=$b -v c=$c 'BEGIN{print (((6*a) + b + c)/8)}\' >> ${COLOR_MSE_Y}
    done < ${COLOR_TEMP}


    cat ${MPEG_SCORE_OUTPUT} | grep "c" | grep F| grep -v h | grep PSNR | grep ":" | cut -d ":" -f 2 | cut -d " " -f 2 > ${COLOR_TEMP}

    rm -f ${COLOR_PSNR}

    while read a; do
        read b;
        read c;
        awk -v a=$a -v b=$b -v c=$c 'BEGIN{print (((6*a) + b + c)/8)}\' >> ${COLOR_PSNR}
    done < ${COLOR_TEMP}


    cat ${MPEG_SCORE_OUTPUT} | grep "c" | grep F| grep h | grep -v PSNR | grep ":" | cut -d ":" -f 2 | cut -d " " -f 2 > ${COLOR_TEMP}

    rm -f ${COLOR_H_Y}

    while read a; do
        read b;
        read c;
        awk -v a=$a -v b=$b -v c=$c 'BEGIN{print (((6*a) + b + c)/8)}\' >> ${COLOR_H_Y}
    done < ${COLOR_TEMP}


    cat ${MPEG_SCORE_OUTPUT} | grep "c" | grep F| grep h | grep PSNR | grep ":" | cut -d ":" -f 2 | cut -d " " -f 2 > ${COLOR_TEMP}

    rm -f ${COLOR_H_PSNR}

    while read a; do
        read b;
        read c;
        awk -v a=$a -v b=$b -v c=$c 'BEGIN{print (((6*a) + b + c)/8)}\' >> ${COLOR_H_PSNR}
    done < ${COLOR_TEMP}


}

angular_similarity()
{

    rm -f ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    rm -f ${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_RMS}
    rm -f ${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_MSE}

    cd ${DATA_PATH_XYZ}

    echo "fileIDMSE = fopen('${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_MSE}','a');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "fileIDRMS = fopen('${OUTPUT_DIR}/${ANGULAR_SCORE_OUTPUT_RMS}','a');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}

for i in $(ls -1 *hidden*.xyz); do

  # TODO: iterate in the options of the metric? ('MSE', ...)
  for k in $(ls -1 $(basename ${i} _hidden.xyz)*octree*.xyz); do
    echo "xyzPoints1 = load('${DATA_PATH_XYZ}/${i}');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "xyzPoints2 = load('${DATA_PATH_XYZ}/${k}');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "normals1 = pcnormals(ptCloud1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1,'Normal',normals1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "normals2 = pcnormals(ptCloud2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2,'Normal',normals2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}

    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'MSE');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n "fprintf(fileIDMSE,'%.10f" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}

    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'RMS');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n "fprintf(fileIDRMS,'%.10f" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}


  done;

    echo "xyzPoints1 = load('${DATA_PATH_XYZ}/${i}');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "xyzPoints2 = load('${DATA_PATH_XYZ}/${i}');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "normals1 = pcnormals(ptCloud1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1,'Normal',normals1);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "normals2 = pcnormals(ptCloud2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2,'Normal',normals2);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
  
    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'MSE');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n "fprintf(fileIDMSE,'%.10f" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}

    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'RMS');" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n "fprintf(fileIDRMS,'%.10f" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}

done;

echo "fclose(fileIDMSE);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}
echo "fclose(fileIDRMS);" >> ${OUTPUT_DIR}/${ANGULAR_SCRIPT_TEMP}

echo "run: cd ${OUTPUT_DIR}"
echo "run: ${MATLAB_BIN} -nodisplay -r \"run ${ANGULAR_SCRIPT_TEMP},quit\""


}

# our MOSp
# MOSp of 2 other good metrics
new_metric_avg()
{
## our LBP
#    for j in a b c d e f; do
    j=f
        echo "distancia,MOS" > ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA}
        paste -d, ${OUTPUT_DIR}/${j}/${SCORE_OUTPUT} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA}

        ${FITTING_BIN} ${OUTPUT_DIR}/${j}/${PY_OUTPUT_DATA} | sed 's/\[//g' | sed 's/\]//g' | sed 's/ //g' > ${METRIC_LBP_FIT}
#    done


##  PTPLANE_MSE
    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPLANE_MSE} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    ${FITTING_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} | sed 's/\[//g' | sed 's/\]//g' | sed 's/ //g' > ${METRIC_1_FIT}

##  PTPOINT_MSE
    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${PTPOINT_MSE} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    ${FITTING_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} | sed 's/\[//g' | sed 's/\]//g' | sed 's/ //g' > ${METRIC_2_FIT}

##  COLOR_MSE_Y
    echo "distancia,MOS" > ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    paste -d, ${COLOR_MSE_Y} ${DATA_PATH}/${PY_MOS_INPUT} >> ${OUTPUT_DIR}/${PY_OUTPUT_DATA}
    ${FITTING_BIN} ${OUTPUT_DIR}/${PY_OUTPUT_DATA} | sed 's/\[//g' | sed 's/\]//g' | sed 's/ //g' > ${METRIC_3_FIT}

    rm -f ${NEW_MIX_SCORE}

    # averaging...
    paste -d"\n" ${METRIC_LBP_FIT} ${METRIC_1_FIT} ${METRIC_2_FIT} > ${METRICS_MERGED}
    while read a; do
        read b;
        read c;
        awk -v a=$a -v b=$b -v c=$c 'BEGIN{print ((a + b + c)/3)}\' >> ${NEW_MIX_SCORE}
        #        awk -v a=$a -v b=$b 'BEGIN{print ((a + b)/2)}\' >> ${NEW_MIX_SCORE}
    done < ${METRICS_MERGED}

}

hybrid_geotex_raw()
{

  vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"


  rm -f ${OUTPUT_DIR_HYBRID_DIR}/*
  mkdir -p ${OUTPUT_DIR_HYBRID_DIR}

  for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
  do

    for k in ${vox_idx_list}
    do
## bitdance work
#      simulation_input_text=${OUTPUT_DIR}/metric-10/neighbors-${neighbors}/distances-${k}.csv
#      simulation_input_text=${OUTPUT_DIR}/metric-4/neighbors-${neighbors}/distances-${k}.csv
      simulation_input_text=${OUTPUT_DIR}/metric-1/neighbors-${neighbors}/distances-${k}.csv
      simulation_input_geo=${OUTPUT_DIR}/metric-11/neighbors-6/distances-novox.csv

      local_output=${OUTPUT_DIR_HYBRID_DIR}/hybrid_raw-n${neighbors}-${k}.csv

#      echo "distancia,MOS" > ${local_output}
      echo "nome_do_pc,nome_referencia,subjective_MOS,d_hybrid" > ${local_output}

      paste -d"\n" ${simulation_input_text} ${simulation_input_geo} > ${OUTPUT_DIR_HYBRID_DIR}/combined-intermediate-n${neighbors}-${k}.csv

      while read -r textd; do
        read -r geod;

        field1=$(echo ${textd} | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "nome_do_pc" ]; then # this is to skip the first line
          continue
        fi

        name_pc=$(echo ${textd} | awk -F',' '{printf "%s", $1}' | tr -d '"')
        ref_pc=$(echo ${textd} | awk -F',' '{printf "%s", $2}' | tr -d '"')
        mos=$(echo ${textd} | awk -F',' '{printf "%s", $3}' | tr -d '"')

        jensenshannont=$(echo ${textd} | awk -F',' '{printf "%s", $10}' | tr -d '"')
        euclideant=$(echo ${textd} | awk -F',' '{printf "%s", $9}' | tr -d '"')
        jensenshannong=$(echo ${geod} | awk -F',' '{printf "%s", $10}' | tr -d '"')

#        hybrid=$(awk -v a=${jensenshannont} -v b=${jensenshannong} 'BEGIN{print ((a + b)/2)}\')
        hybrid=$(awk -v a=${euclideant} -v b=${jensenshannong} 'BEGIN{print ((a + b)/2)}\')
        echo "${name_pc},${ref_pc},${mos},${hybrid}" >> ${local_output}
#        echo "${hybrid},${mos}" >> ${local_output}

      done < ${OUTPUT_DIR_HYBRID_DIR}/combined-intermediate-n${neighbors}-${k}.csv

    done

  done

}

hybrid_geotex()
{

  vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"


  rm -f ${OUTPUT_DIR_HYBRID_DIR}/*
  mkdir -p ${OUTPUT_DIR_HYBRID_DIR}

  for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
  do

    for k in ${vox_idx_list}
    do
      simulation_input_text=${OUTPUT_DIR_SDB_LOGISTIC}/metric-10/neighbors-${neighbors}/simulations-${k}.csv

      simulation_input_geo=${OUTPUT_DIR_SDB_LOGISTIC}/metric-12/neighbors-6/simulations-novox.csv
#      simulation_input_geo=${OUTPUT_DIR_SDB_LOGISTIC}/metric-11/neighbors-8/simulations-novox.csv

      local_output=${OUTPUT_DIR_HYBRID_DIR}/hybrid-n${neighbors}-${k}.csv

      echo "distancia,MOS" > ${local_output}
#      echo "nome_do_pc,nome_referencia,subjective_MOS,d_hybrid" > ${local_output}

      paste -d"\n" ${simulation_input_text} ${simulation_input_geo} > ${OUTPUT_DIR_HYBRID_DIR}/combined-intermediate-n${neighbors}-${k}.csv

      while read -r textd; do
        read -r geod;

        field1=$(echo ${textd} | awk -F',' '{printf "%s", $1}' | tr -d '"')
        if [ "$field1" = "simulation" ]; then # this is to skip the first line
          continue
        fi

        name_pc=$(echo ${textd} | awk -F',' '{printf "%s", $2}' | tr -d '"')
        ref_pc=$(echo ${textd} | awk -F',' '{printf "%s", $3}' | tr -d '"')
        mos=$(echo ${textd} | awk -F',' '{printf "%s", $4}' | tr -d '"')

        jensenshannont=$(echo ${textd} | awk -F',' '{printf "%s", $11}' | tr -d '"')
        jensenshannong=$(echo ${geod} | awk -F',' '{printf "%s", $11}' | tr -d '"')

#        hybrid=$(awk -v a=${jensenshannont} -v b=${jensenshannong} 'BEGIN{print ((2*a + b)/2)}\')
        hybrid=$(awk -v a=${jensenshannont} -v b=${jensenshannong} 'BEGIN{print ((a + b)/2)}\')
#        echo "${name_pc},${ref_pc},${mos},${hybrid}" >> ${local_output}
        echo "${hybrid},${mos}" >> ${local_output}

      done < ${OUTPUT_DIR_HYBRID_DIR}/combined-intermediate-n${neighbors}-${k}.csv

    done

  done

}

correlate_new_metric()
{

    echo "distancia,MOS" > ${NEW_MIX_CORRELATION}
    paste -d, ${NEW_MIX_SCORE} ${DATA_PATH}/${PY_MOS_INPUT} >> ${NEW_MIX_CORRELATION}

    ${MOSP_BIN} ${NEW_MIX_CORRELATION}

}

pre_process_data()
{

  # ad-hoc alert!

  for metric in ${METRICS_LIST};
  do
    for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
    do
      DIR=${OUTPUT_DIR}/metric-${metric}/neighbors-${neighbors}/
      mkdir -p ${DIR}

      vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"
      for k in ${vox_idx_list}
      do
        input=${OUTPUT_DIR}/results-${k}.csv
        output=${DIR}/features-${k}.csv
        cat ${input} | grep ",metric_${metric}," | grep ",n_${neighbors}," > ${output}

        # Add header
        columns=$(head -1 ${output} | sed 's/[^,]//g' | wc -c )
        features=$((columns - 3))
        header="file,metric,neighbors"
        for col in $(seq 1 ${features})
        do
          header="${header},fv${col}"
        done
        echo ${header} | cat - ${output} > temp && mv temp ${output}
        echo ${output}
      done
    done
  done
}

calculate_distance()
{

  for metric in ${METRICS_LIST};
  do
    for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
    do
      INPUT_DIR=${OUTPUT_DIR}/metric-${metric}/neighbors-${neighbors}/
      output_dir=${OUTPUT_DIR}/metric-${metric}/neighbors-${neighbors}/

      mkdir -p ${output_dir}

      vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"
      for k in ${vox_idx_list}
      do
        input_score=${CSV}
        input_feature=${INPUT_DIR}/features-${k}.csv
        output=${output_dir}/distances-${k}.csv
        python3 ${DISTANCE_BIN} --score_file ${input_score} \
               --feature_file ${input_feature} --output_file ${output}
        echo ${output}
      done
    done
  done

}

compute_single_db_simulations_distances_only_logistic()
{

  for metric in ${METRICS_LIST};
  do
    for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
    do
      INPUT_DIR=${OUTPUT_DIR}/metric-${metric}/neighbors-${neighbors}/
      output_dir=${OUTPUT_DIR_SDB_LOGISTIC}/metric-${metric}/neighbors-${neighbors}/

      mkdir -p ${output_dir}

      vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"
      for k in ${vox_idx_list}
      do
        input_distances=${INPUT_DIR}/distances-${k}.csv
        output=${output_dir}/simulations-${k}.csv
        python3 ${SINGLE_DB_LOGISTIC} \
            --raw_distances_file ${input_distances} \
            --output_file ${output}
      done
    done
  done
}

compute_single_db_simulations_distances_only_regressors()
{

  for metric in ${METRICS_LIST};
  do
    for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
    do
      INPUT_DIR=${OUTPUT_DIR}/metric-${metric}/neighbors-${neighbors}/
      output_dir=${OUTPUT_DIR_SDB_REGRESSORS}/metric-${metric}/neighbors-${neighbors}/
      mkdir -p ${output_dir}

      vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"
      for k in ${vox_idx_list}
      do
        input_distances=${INPUT_DIR}/distances-${k}.csv
        output=${output_dir}/simulations-${k}.csv
        python3 ${SINGLE_DB_REGRESSORS} \
            --raw_distances_file ${input_distances} \
            --output_file ${output}
      done
    done
  done
}


logistic_correlation_hybrid()
{
  vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"


  for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
  do
    for k in ${vox_idx_list}
    do
      input_distances=${OUTPUT_DIR_HYBRID_DIR}/hybrid_raw-n${neighbors}-${k}.csv
      simulation_output=${OUTPUT_DIR_HYBRID_DIR}/simulation-n${neighbors}-${k}.csv

      python3 ${SINGLE_DB_LOGISTIC} \
              --raw_distances_file ${input_distances} \
              --output_file ${simulation_output}
    done
  done

}

plot_logistic_hybrid()
{

  vox_idx_list="novox $(printf '%s\n' "$VOXEL_SIZES" | sed 's/[^[:space:]]\{1,\}/k&/g')"


  output_dir=${OUTPUT_DIR_HYBRID_DIR}/


  for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
  do

    simulation_input=${OUTPUT_DIR_HYBRID_DIR}/simulation-n${neighbors}

    python3 ${LINEPLOT_CORRELATION_HYBRID} \
            --metric_prefix ${simulation_input} --metric PCC \
            --show_legend 1 \
            --output_file ${output_dir}/logistic-n${neighbors}-pcc.png \
            --output_correlation ${output_dir}/correlation-n${neighbors}-pcc.csv


    python3 ${LINEPLOT_CORRELATION_HYBRID} \
            --metric_prefix ${simulation_input} --metric SROCC \
            --show_legend 1 \
            --output_file ${output_dir}/logistic-n${neighbors}-srocc.png \
            --output_correlation ${output_dir}/correlation-n${neighbors}-srocc.csv


    python3 ${LINEPLOT_CORRELATION_HYBRID} \
            --metric_prefix ${simulation_input} --metric RMSE \
            --show_legend 1 \
            --output_file ${output_dir}/logistic-n${neighbors}-rmse.png \
            --output_correlation ${output_dir}/correlation-n${neighbors}-rmse.csv

  done

}


plot_correlation_per_k_logistic()
{

  # LOGISTIC ONLY
  for metric in ${METRICS_LIST};
  do
    for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
    do
      echo LOGISTIC ${metric} ${neighbors}
      INPUT_DIR=${OUTPUT_DIR_SDB_LOGISTIC}/metric-${metric}/neighbors-${neighbors}/
      output_dir=${OUTPUT_DIR_SDB_LOGISTIC}/metric-${metric}/neighbors-${neighbors}/
      mkdir -p ${output_dir}

      python3 ${LINEPLOT_CORRELATION} \
             --metric_directory ${INPUT_DIR} --metric SROCC \
             --show_legend 1 \
             --output_file ${output_dir}/logistic-srocc.png \
             --output_correlation ${output_dir}/correlation-srocc.csv

      python3 ${LINEPLOT_CORRELATION} \
             --metric_directory ${INPUT_DIR} --metric PCC \
             --show_legend 1 \
             --output_file ${output_dir}/logistic-pcc.png \
             --output_correlation ${output_dir}/correlation-pcc.csv

      python3 ${LINEPLOT_CORRELATION} \
             --metric_directory ${INPUT_DIR} --metric RMSE \
             --show_legend 1 \
             --output_file ${output_dir}/logistic-rmse.png \
             --output_correlation ${output_dir}/correlation-rmse.csv

    done
  done
}


plot_correlation_per_k_regressors()
{

# REGRESSORS
  for metric in ${METRICS_LIST};
  do
    for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
    do
      for regressor in ExtraTreesRegressor GradientBoostingRegressor RandomForestRegressor
      do
        echo ${regressor} ${metric} ${neighbors}
        INPUT_DIR=${OUTPUT_DIR_SDB_REGRESSORS}/metric-${metric}/neighbors-${neighbors}/${regressor}
        output_dir=${OUTPUT_DIR_SDB_REGRESSORS}/metric-${metric}/neighbors-${neighbors}/
        mkdir -p ${output_dir}

      python3 ${LINEPLOT_CORRELATION} \
             --metric_directory ${INPUT_DIR} --metric SROCC \
             --show_legend 1 \
             --output_file ${output_dir}/${regressor}-srocc.png \
             --output_correlation ${output_dir}/correlation-${regressor}-srocc.csv

      python3 ${LINEPLOT_CORRELATION} \
             --metric_directory ${INPUT_DIR} --metric PCC \
             --show_legend 1 \
             --output_file ${output_dir}/${regressor}-pcc.png \
             --output_correlation ${output_dir}/correlation-${regressor}-pcc.csv

      python3 ${LINEPLOT_CORRELATION} \
             --metric_directory ${INPUT_DIR} --metric RMSE \
             --show_legend 1 \
             --output_file ${output_dir}/${regressor}-rmse.png \
             --output_correlation ${output_dir}/correlation-${regressor}-rmse.csv

      done
    done
  done

}


plot_correlation_logistic_mpegmetrics()
{
  echo ${OUTPUT_DIR}

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric SROCC \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-srocc.png \
          --output_correlation ${OUTPUT_DIR}/correlation-srocc.csv

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric PCC \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-pcc.png \
          --output_correlation ${OUTPUT_DIR}/correlation-pcc.csv

  python3 ${LINEPLOT_CORRELATION} \
          --metric_directory ${OUTPUT_DIR} --metric RMSE \
          --show_legend 1 \
          --output_file ${OUTPUT_DIR}/logistic-rmse.png \
          --output_correlation ${OUTPUT_DIR}/correlation-rmse.csv

}




#plot_correlation_per_k_regressors()
#{
#
## REGRESSORS
#  for metric in ${METRICS_LIST};
#  do
#    for neighbors in ${NEIGHBOURHOOD_LIST_W_SPACES};
#    do
#      for regressor in ExtraTreesRegressor GradientBoostingRegressor RandomForestRegressor
#      do
#        echo ${regressor} ${metric} ${neighbors}
#        INPUT_DIR=${OUTPUT_DIR_SDB_REGRESSORS}/metric-${metric}/neighbors-${neighbors}/${regressor}
#        output_dir=${OUTPUT_DIR_SDB_REGRESSORS}/metric-${metric}/neighbors-${neighbors}/
#        mkdir -p ${output_dir}
#        python3 ${LINEPLOT_CORRELATION} \
#          --metric_directory ${INPUT_DIR} --metric SROCC \
#          --show_legend 1 \
#          --output_file ${output_dir}/${regressor}.png
#      done
#    done
#  done
#
#}

group_plot()
{
  ## TODO - remove the database parameters?
  # LOGISTIC ONLY
  for metric in ${METRICS_LIST};
  do
    python3 script_group_plots.py \
           --plot_directories ${OUTPUT_DIR_SDB_LOGISTIC} \
           --metric metric-${metric} --database ${database} \
           --output_directory ${OUTPUT_DIR}/
  done

}
