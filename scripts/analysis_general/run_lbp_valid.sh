#!/bin/sh

# our library
. ./pc_functions.sh

# PREFIX=/usr
PREFIX=/home/rafael2k

CSV=/mnt/ssd/RafaelDiniz/VALID/VALID_PC_iterative.csv

PC_METRIC_BIN=${PREFIX}/bin/pc_metric
CORRELATE_BIN=${PREFIX}/bin/correlate
COMPARE_HISTOGRAM_BIN=${PREFIX}/bin/compare_histogram
VOXELIZE_BIN=${PREFIX}/bin/optimize_voxel_size
MOSP_BIN=/home/rafael2k/pc_metric/scripts/predicted_mos.py

DATA_PATH=/mnt/ssd/RafaelDiniz/VALID/converted_db
CORRELATION_OUTPUT=correlation.csv
SCORE_OUTPUT=scores.csv

OUTPUT_DIR=/mnt/ssd/RafaelDiniz/VALID/results

# for python pre-processing step
PY_OUTPUT_DATA=data.csv
PY_MOS_INPUT=scores-mean.txt
PY_CORRELATION_OUTPUT=correlation-py.csv


mkdir -p ${OUTPUT_DIR}

mkdir -p ${OUTPUT_DIR}/a
mkdir -p ${OUTPUT_DIR}/b
mkdir -p ${OUTPUT_DIR}/c
mkdir -p ${OUTPUT_DIR}/d
mkdir -p ${OUTPUT_DIR}/e
mkdir -p ${OUTPUT_DIR}/f

# just for tests
##get_optimal_voxel_size

##calculate_histogram

#calculate_histogram_voxelized

#calculate_histogram_difference

correlate_python
