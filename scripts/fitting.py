#!/usr/bin/python3

import sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from scipy.stats import spearmanr, pearsonr
from sklearn.metrics import mean_squared_error

# df = pd.read_csv('data.csv')
df = pd.read_csv(sys.argv[1])

# df["distancia"] = df["distancia"].astype(float)

X = df["distancia"].values.reshape(-1, 1)
y = df["MOS"].values.reshape(-1, 1)

# drop infinities
X[X == np.inf] = 9999999
y[y == np.inf] = 9999999

poly_reg = PolynomialFeatures(degree=3)
X_poly = poly_reg.fit_transform(X)
pol_reg = LinearRegression()
pol_reg.fit(X_poly, y)

fitted = pol_reg.predict(poly_reg.fit_transform(X))

#plt.scatter(X, y, color='red')
#plt.plot(X, fitted,color='blue')
#plt.title('Fitted')
#plt.xlabel('L2')
#plt.ylabel('Score')
#plt.show()

df["Predicted"] = fitted

#print("Predited")
print(fitted)
#print("MOS")
#print(y)

# print(pol_reg.coef_)

#corr, p_value = pearsonr(df["Predicted"], df["MOS"])

#print("PCC,"+str(corr))
#print(str(corr), end = '')

#corr, p_value = spearmanr(df["Predicted"], df["MOS"])

#print("SROCC,"+str(corr))
#print(", "+str(corr), end = '')

#print(", "+str(mean_squared_error(df["Predicted"], df["MOS"], None, squared=False)))



