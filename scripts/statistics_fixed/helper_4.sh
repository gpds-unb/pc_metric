#!/bin/bash
export LC_NUMERIC="en_US.UTF-8";

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
   -m)
    metric="$2"
    shift # past argument
    shift # past value
    ;;
    --default)
    DEFAULT=YES
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

for neighbors in $(seq 6 12);
do
    for database in apsipa19 qomex19 spie19;
    do
        INPUT_DIR=2_RAW_DISTANCES_PER_METRIC/metric-${metric}/neighbors-${neighbors}/${database}/
        OUTPUT_DIR=4_SIMULATIONS_SINGLE_DB_DISTANCES_ONLY_Regressors/metric-${metric}/neighbors-${neighbors}/${database}/
        mkdir -p ${OUTPUT_DIR}
        
        for k in k0.7 k1.0 k1.5 k2.0 k3.0 k4.0 k5.0 k6.0 novox
        do
            input_distances=${INPUT_DIR}/distances-${k}.csv
            output=${OUTPUT_DIR}/simulations-${k}.csv
            python script_simulation_singledb_Regressors.py \
            --raw_distances_file ${input_distances} \
            --output_file ${output}
        done
    done
done