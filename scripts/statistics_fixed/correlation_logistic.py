import os
import sys
import argparse

import pandas as pd
import numpy as np
from absl import app
from absl.flags import argparse_flags
from scipy.stats import wasserstein_distance as EMD
from scipy.stats import energy_distance as energy
from scipy.optimize import curve_fit
from scipy.stats import pearsonr, spearmanr, kendalltau
from tqdm import tqdm



def itu_logistic_function(x, b1, b2, b3, b4, wi, ei):
    v = (x-b3) / np.abs(b4)
    v = np.array(v, dtype=np.float128)
    e = np.exp(-v)
    e = np.array(e, dtype=np.float64)
    f = (b1 - b2) / (1.0 + e)
    return ei + wi * (f + b2)


def return_single_simulation(x_train, y_train, x_test, y_test):
    b1 = np.max(y_train)
    b2 = np.min(y_train)
    b3 = np.median(x_train)
    b4 = 1.0
    delta = np.std(y_train)
    wi = 1.0 / delta
    Yiw = wi * y_train
    ei = np.abs(x_test - y_test)
    eiw = wi * ei
    p = np.array([b1, b2, b3, b4, wi, eiw], dtype=np.float64)
    maximum = np.iinfo(np.int32).max
    popt, pcov = curve_fit(itu_logistic_function,
                           x_train, Yiw, p0=p, maxfev=maximum)
    [b1, b2, b3, b4, wi, eiw] = popt
    y_pred = itu_logistic_function(x_test, b1, b2, b3, b4, wi, eiw)
    residual = y_test - y_pred
    return [x_test, y_test, y_pred, residual, b1, b2, b3, b4, wi, eiw]



df = pd.read_csv(sys.argv[1)

df["distancia"] = df["distancia"].replace(np.inf, 99)
x_test = df["distancia"]
y_train = df["MOS"]

## FUCKED UP!!!
