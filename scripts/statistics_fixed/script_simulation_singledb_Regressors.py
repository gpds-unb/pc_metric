import os
import sys
import argparse

import pandas as pd
import numpy as np
from absl import app
from absl.flags import argparse_flags
from scipy.optimize import curve_fit
from scipy.stats import pearsonr, spearmanr, kendalltau
from tqdm import tqdm
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.linear_model import ARDRegression, BayesianRidge
from sklearn.linear_model import Lars, ElasticNet, ElasticNetCV, Lasso
from sklearn.linear_model import RANSACRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neural_network import MLPRegressor


models = [
    RandomForestRegressor(
        n_estimators=1000, criterion='mse', max_depth=None, min_samples_split=2,
        min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto',
        max_leaf_nodes=None, min_impurity_decrease=0.0,
        min_impurity_split=None, bootstrap=True, oob_score=False, n_jobs=None,
        random_state=None, verbose=0, warm_start=False, max_samples=None
    ),
    ExtraTreesRegressor(
        n_estimators=1000, criterion='mse', max_depth=None, min_samples_split=2,
        min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto',
        max_leaf_nodes=None, min_impurity_decrease=0.0,
        min_impurity_split=None, bootstrap=True, oob_score=False, n_jobs=None,
        random_state=None, verbose=0, warm_start=False
    ),
    GradientBoostingRegressor(
        loss='huber', learning_rate=0.1, n_estimators=1000, subsample=1.0,
        criterion='friedman_mse', min_samples_split=2, min_samples_leaf=1,
        min_weight_fraction_leaf=0.0, max_depth=3, min_impurity_decrease=0.0,
        min_impurity_split=None, init=None, random_state=None,
        max_features=None, alpha=0.9, verbose=0, max_leaf_nodes=None,
        warm_start=False, validation_fraction=0.1,
        n_iter_no_change=None, tol=0.0001
    ),
    # BayesianRidge(
    #     n_iter=300, tol=0.001, alpha_1=1e-06, alpha_2=1e-06, lambda_1=1e-06,
    #     lambda_2=1e-06, compute_score=False,
    #     fit_intercept=True, normalize=False, copy_X=True, verbose=False
    # ),
    # ARDRegression(
    #     n_iter=300, tol=0.001, alpha_1=1e-06, alpha_2=1e-06, lambda_1=1e-06,
    #     lambda_2=1e-06, compute_score=False, threshold_lambda=10000.0,
    #     fit_intercept=True, normalize=False, copy_X=True, verbose=False
    # ),
    # Lars(),
    # ElasticNet(),
    # ElasticNetCV(),
    # Lasso(),
    # RANSACRegressor(),
    # KNeighborsRegressor(
    #     n_neighbors=5, weights='uniform', algorithm='auto', leaf_size=30,
    #     p=3, metric='minkowski', metric_params=None, n_jobs=None
    # ),
    # MLPRegressor(
    #     hidden_layer_sizes=(2, ), activation='logistic', solver='lbfgs',
    #     alpha=0.0001, batch_size='auto', learning_rate='adaptive',
    #     learning_rate_init=0.001, power_t=0.5, max_iter=20000, shuffle=True,
    #     random_state=None, tol=0.0001, verbose=False, warm_start=False,
    #     momentum=0.9, nesterovs_momentum=True, early_stopping=False,
    #     validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08,
    #     n_iter_no_change=10
    # )
]


def mean_squared_error(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def return_single_simulation(x_train, y_train, x_test, y_test, model):
    x_train = x_train.reshape(-1, 1)
    x_test = x_test.reshape(-1, 1)
    y_train = y_train.ravel()
    model.fit(x_train, y_train)
    y_pred = model.predict(x_test)
    residual = y_test - y_pred
    return [x_test, y_test, y_pred, residual]


def run(args):
    df = pd.read_csv(args.raw_distances_file)
    simulations = len(df.index)
    output = []
    for model in models:
        modelname = type(model).__name__
        for i in tqdm(range(simulations)):
            row = df.iloc[[i]]
            reference_name = row.nome_referencia.tolist()[0]
            others = df[df.nome_referencia != reference_name]
            y_test = row.subjective_MOS.values
            line = {
                'simulation': i,
                'nome_do_pc': row["nome_do_pc"].tolist()[0],
                'nome_referencia': row["nome_referencia"].tolist()[0],
                'MOS': y_test[0],
            }
            distances = [col for col in df if col.startswith('d_')]
            for d in distances:
                x_test = row[d].values
                x_train = others[d].values
                y_train = others.subjective_MOS.values
                [x_test, y_test, y_pred, residual] = return_single_simulation(
                    x_train, y_train, x_test, y_test, model)
                line[d] = y_pred[0]
            output.append(line)
        df_out = pd.DataFrame.from_dict(output)
        filename = os.path.basename(args.output_file)
        pathname = os.path.dirname(args.output_file) + "/" + modelname
        mkdir(pathname)
        df_out.to_csv(pathname + "/" + filename, index=False)

        rmse = mean_squared_error(df_out.d_euclidean, df_out.MOS)
        SROCC = spearmanr(df_out.d_euclidean, df_out.MOS)[0]
        PCC = pearsonr(df_out.d_euclidean, df_out.MOS)[0]
        print(args.raw_distances_file, "PCC=",
              PCC, "SROCC=", SROCC, "RMSE=", rmse)


def parse_args(argv):
    """Parses command line arguments."""
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--raw_distances_file", "-r",
        type=str, dest="raw_distances_file",
        help="File containing the raw distances and the subjective scores.")
    parser.add_argument(
        "--output_file", "-o",
        type=str, dest="output_file",
        help="File containing the processed distances.")
    args = parser.parse_args(argv[1:])
    return args


def main(args):
    run(args)


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)
