#!/bin/bash

for metric in $(seq 0 10); 
do
  for neighbors in $(seq 6 12); 
  do
    for database in apsipa19 qomex19 spie19;
    do
      INPUT_DIR=1_RAW_FEATURES_PER_METRIC/metric-${metric}/neighbors-${neighbors}/${database}/
      OUTPUT_DIR=2_RAW_DISTANCES_PER_METRIC/metric-${metric}/neighbors-${neighbors}/${database}/
      mkdir -p ${OUTPUT_DIR}
      
      for k in k0.7 k1.0 k1.5 k2.0 k3.0 k4.0 k5.0 k6.0 novox
      do
        input_score=0_INPUT_RAW_FEAURES/${database}/scores.csv
        input_feature=${INPUT_DIR}/features-${k}.csv
        output=${OUTPUT_DIR}/distances-${k}.csv
        python script_compute_distances.py --score_file ${input_score} \
            --feature_file ${input_feature} --output_file ${output}
        echo ${output}
      done
    done
  done
done