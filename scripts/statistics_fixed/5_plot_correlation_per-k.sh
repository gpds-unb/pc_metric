#!/bin/bash
export LC_NUMERIC="en_US.UTF-8";

# LOGISTIC ONLY
for metric in $(seq 0 10); 
do
  for neighbors in $(seq 6 12); 
  do
    for database in apsipa19 qomex19 spie19;
    do
      echo LOGISTIC ${metric} ${neighbors} ${database}
      INPUT_DIR=3_SIMULATIONS_SINGLE_DB_DISTANCES_ONLY_logistic//metric-${metric}/neighbors-${neighbors}/${database}/
      OUTPUT_DIR=5_PLOTS_PER_K/metric-${metric}/neighbors-${neighbors}/${database}/
      mkdir -p ${OUTPUT_DIR}
      python script_lineplot_correlation_per_k.py \
        --metric_directory ${INPUT_DIR} --metric SROCC \
        --show_legend 1 \
        --output_file ${OUTPUT_DIR}/logistic.png
    done
  done
done


# REGRESSORS
for metric in $(seq 0 10); 
do
  for neighbors in $(seq 6 12); 
  do
    for database in apsipa19 qomex19 spie19;
    do
      for regressor in ExtraTreesRegressor GradientBoostingRegressor RandomForestRegressor
      do
        echo ${regressor} ${metric} ${neighbors} ${database}
        INPUT_DIR=4_SIMULATIONS_SINGLE_DB_DISTANCES_ONLY_Regressors/metric-${metric}/neighbors-${neighbors}/${database}/${regressor}
        OUTPUT_DIR=5_PLOTS_PER_K/metric-${metric}/neighbors-${neighbors}/${database}/
        mkdir -p ${OUTPUT_DIR}
        python script_lineplot_correlation_per_k.py \
          --metric_directory ${INPUT_DIR} --metric SROCC \
          --output_file ${OUTPUT_DIR}/${regressor}.png
      done
    done
  done
done