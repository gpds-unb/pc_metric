#!/bin/bash
export LC_NUMERIC="en_US.UTF-8";

# LOGISTIC ONLY
for metric in $(seq 0 10);
do
    for database in apsipa19 qomex19 spie19;
    do
        python script_group_plots.py \
            --plot_directories 5_PLOTS_PER_K \
            --metric metric-${metric} --database ${database} \
            --output_directory 6_GROUPED_PLOTS/
    done
done
