import os
import sys
import argparse

import pandas as pd
import numpy as np
from absl import app
from absl.flags import argparse_flags
from scipy.stats import wasserstein_distance as EMD
from scipy.stats import energy_distance as energy
from scipy.optimize import curve_fit
from scipy.stats import pearsonr, spearmanr, kendalltau
from tqdm import tqdm


def mean_squared_error(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())


def itu_logistic_function(x, b1, b2, b3, b4, wi, ei):
    v = (x-b3) / np.abs(b4)
    v = np.array(v, dtype=np.float128)
    e = np.exp(-v)
    e[e == np.inf] = np.finfo(np.float64).max
#    print("e = ", e)
    e = np.array(e, dtype=np.float64)
    f = (b1 - b2) / (1.0 + e)
    return ei + wi * (f + b2)


def return_single_simulation(x_train, y_train, x_test, y_test):
    b1 = np.max(y_train)
    b2 = np.min(y_train)
    b3 = np.median(x_train)
    b4 = 1.0
    delta = np.std(y_train)
    wi = 1.0 / delta
    Yiw = wi * y_train
    ei = np.abs(x_test - y_test)
    eiw = wi * ei
    p = np.array([b1, b2, b3, b4, wi, eiw], dtype=np.float64)
    maximum = np.iinfo(np.int32).max
    popt, pcov = curve_fit(itu_logistic_function,
                           x_train, Yiw, p0=p, maxfev=maximum)
    [b1, b2, b3, b4, wi, eiw] = popt
    y_pred = itu_logistic_function(x_test, b1, b2, b3, b4, wi, eiw)
    residual = y_test - y_pred
    return [x_test, y_test, y_pred, residual, b1, b2, b3, b4, wi, eiw]


def run(args):
    df = pd.read_csv(args.raw_distances_file)

    df = df.replace(np.inf, 99)

#    df['subjective_MOS'] = df['subjective_MOS'].values.reshape(-1, 1)

    simulations = len(df.index)
    output = []
    for i in tqdm(range(simulations)):
        row = df.iloc[[i]]
        reference_name = row.nome_referencia.tolist()[0]
        others = df[df.nome_referencia != reference_name]
        y_test = row.subjective_MOS.values
        line = {
            'simulation': i,
            'nome_do_pc': row["nome_do_pc"].tolist()[0],
            'nome_referencia': row["nome_referencia"].tolist()[0],
            'MOS': y_test[0],
        }
        distances = [col for col in df if col.startswith('d_')]
        for d in distances:
            x_test = row[d].values
            x_train = others[d].values
            y_train = others.subjective_MOS.values
            [x_test, y_test, y_pred, residual, b1, b2, b3, b4, wi,
                eiw] = return_single_simulation(x_train, y_train, x_test, y_test)
            line[d] = y_pred[0]
        output.append(line)
    df_out = pd.DataFrame.from_dict(output)
    df_out.to_csv(args.output_file, index=False)

#    rmse = mean_squared_error(df_out.d_euclidean, df_out.MOS)
#    SROCC = spearmanr(df_out.d_euclidean, df_out.MOS)[0]
#    PCC = pearsonr(df_out.d_euclidean, df_out.MOS)[0]
#    print(args.raw_distances_file, "PCC=", PCC, "SROCC=", SROCC, "RMSE=", rmse)


def parse_args(argv):
    """Parses command line arguments."""
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--raw_distances_file", "-r",
        type=str, dest="raw_distances_file",
        help="File containing the raw distances and the subjective scores.")
    parser.add_argument(
        "--output_file", "-o",
        type=str, dest="output_file",
        help="File containing the processed distances.")
    args = parser.parse_args(argv[1:])
    return args


def main(args):
    run(args)


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)
