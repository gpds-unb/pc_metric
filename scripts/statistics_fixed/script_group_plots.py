import argparse
import pandas as pd
import numpy as np
from absl import app
from absl.flags import argparse_flags
import seaborn as sns
from skimage.io import imread
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid


def run(args):
    template_path = "{a}/{m}/{nn}/{d}/{r}.png"
    fig, axarr = plt.subplots(7, 4, figsize=(32, 56), gridspec_kw={'width_ratios': [1, 1, 1, 1.35]})
    fig.tight_layout(pad=0)
    for y, n in enumerate(range(6, 13)):
        neighbor = "neighbors-{}".format(n)
        for x, r in enumerate(["ExtraTreesRegressor", "GradientBoostingRegressor",
                               "RandomForestRegressor", "logistic"]):
            input_path = template_path.format(
                nn=neighbor, r=r, a=args.plot_directory,
                m=args.metric, d=args.database)
            title = "{r} (N={n})".format(r=r, n=n)
            try:
                im = imread(input_path)
            except FileNotFoundError:
                im = np.ones((100, 100))
                im[0:99, 50] = 0
                im[50, 0:99] = 0
            axarr[y, x].imshow(im)
            axarr[y, x].axis('off')
            axarr[y, x].set_title(title, fontsize=24)
    output_path = "{p}/{m}_{d}.pdf".format(
        p=args.output_directory, m=args.metric, d=args.database)
    fig.savefig(output_path)


def parse_args(argv):
    """Parses command line arguments."""
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--plot_directories", "-d",
        type=str, dest="plot_directory",
        help="Directory containing the plot files.")
    parser.add_argument(
        "--metric", "-m",
        type=str, dest="metric",
        help="PCC Metric.")
    parser.add_argument(
        "--database", "-l",
        type=str, dest="database",
        help="Database name")
    parser.add_argument(
        "--output_directory", "-o",
        type=str, dest="output_directory",
        help="Directory containing the output plots.")
    args = parser.parse_args(argv[1:])
    return args


def main(args):
    run(args)


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)
