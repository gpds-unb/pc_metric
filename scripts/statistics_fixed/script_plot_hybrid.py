import argparse
import pandas as pd
import numpy as np
from absl import app
from absl.flags import argparse_flags
from scipy.stats import pearsonr, spearmanr, kendalltau
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.transforms as transforms
import matplotlib.ticker as ticker
import pickle



def PCC(predictions, targets):
    return np.abs(pearsonr(predictions, targets)[0])


def SROCC(predictions, targets):
    return np.abs(spearmanr(predictions, targets)[0])


def RMSE(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())




def run(args):
    voxel_params = ["novox", "k0.7", "k1.0", "k1.3", "k1.6", "k2.0", "k3.0", "k4.5", "k6.0" , "k7.5" ]
    distances = [ "d_hybrid" ]
    out_dict = dict.fromkeys(distances, [])
    maximo = -10**10
    minimo = 10**10
    for k in voxel_params:
        for d in distances:
            file = args.metric_prefix + "-{}.csv".format(k)
            df = pd.read_csv(file)
            if args.metric == "PCC":
                cor = PCC(df["MOS"], df[d])
            elif args.metric == "SROCC":
                cor = SROCC(df["MOS"], df[d])
            else:
                cor = RMSE(df["MOS"], df[d])
            maximo = max(cor, maximo)
            minimo = min(cor,minimo)
            out_dict[d] = out_dict[d] + [cor]

    out_dict["voxel param"] = voxel_params
    df_out = pd.DataFrame.from_dict(out_dict)
    new_names = ["Hybrid Descriptor" ]
    for i in range(len(distances)):
        df_out.rename(columns={distances[i] :  new_names[i]}, inplace=True)

    df = df_out.melt("voxel param", var_name='Distance',  value_name=args.metric)

    df.to_csv(args.output_correlation, index=False)

    fig = sns.catplot(x="voxel param", y=args.metric, hue='Distance', data=df, kind='point', legend=args.legend)
#    fig = sns.catplot(x=distances, y=args.metric, hue='Distance', data=df, kind='bar', legend=args.legend)
#    fig = sns.catplot(x=distances, y=args.metric, hue='Distance', data=df, kind='bar', legend=args.legend)
    # fig = sns.barplot(x=distances, y=args.metric, hue='Distance', data=df, legend=args.legend)
    fig.despine(top=False, right=False, left=False, bottom=False)
    ranges = np.arange(0, 1.2, 0.2).tolist() + [maximo]
    fig.set(yticks=ranges)
    if args.metric == "RMSE":
        plt.axhline(y=minimo, color="gray", linestyle=":")
    else:
        plt.axhline(y=maximo, color="gray", linestyle=":")
#    plt.xlabel('MPEG METRICS')

    #pickle.dump(fig, open(args.output_file, 'wb'))
    fig.savefig(args.output_file)


def parse_args(argv):
    """Parses command line arguments."""
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--metric_prefix", "-d",
        type=str, dest="metric_prefix",
        help="Directory containing the metric path+filename prefix")
    parser.add_argument(
        "--metric", "-m",
        type=str, dest="metric",
        help="Metric (PCC, SROCC, or RMSE).")
    parser.add_argument(
        "--show_legend", "-l",
        type=bool, dest="legend",
        help="Add the legend to plot (True or False)")
    parser.add_argument(
        "--output_file", "-o",
        type=str, dest="output_file",
        help="File containing the output plot.")
    parser.add_argument(
        "--output_correlation", "-c",
        type=str, dest="output_correlation",
        help="File containing the correlation output in text.")
    args = parser.parse_args(argv[1:])
    return args


def main(args):
    run(args)


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)
