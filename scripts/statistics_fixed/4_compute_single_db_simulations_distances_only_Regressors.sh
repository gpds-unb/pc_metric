#!/bin/bash

for metric in $(seq 0 10); 
do
  tmux new-session -d -s "simulation-${metric}" "bash helper_4.sh -m ${metric}"
done