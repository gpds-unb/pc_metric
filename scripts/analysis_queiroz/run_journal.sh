#!/bin/sh

# our library
. ./lbp_functions.sh

PREFIX=/home/rafael2k

PC_METRIC_BIN=${PREFIX}/bin/pc_metric_journal
CORRELATE_BIN=${PREFIX}/bin/correlate
COMPARE_HISTOGRAM_BIN=${PREFIX}/bin/compare_histogram
VOXELIZE_BIN=${PREFIX}/bin/optimize_voxel_size
PEDRO_BIN=/home/rafael2k/pc_metric/scripts/predicted_mos.py

DATA_PATH=/mnt/ssd/RafaelDiniz/queiroz
CORRELATION_OUTPUT=correlation.csv
SCORE_OUTPUT=scores.csv

# for python pre-processing step
PY_OUTPUT_DATA=data.csv
PY_MOS_INPUT=scores-mean.txt
PY_CORRELATION_OUTPUT=correlation-py.csv

VOXEL_INFO=voxelization.csv


OUTPUT_DIR=/mnt/ssd/RafaelDiniz/queiroz-journal

#switches to enable or disable each metric
M0=1
M1=1
M2=1
M3=1
M4=1
M5=1
M6=1
M7=1
M8=1
M9=1
M10=1

NEIGHBOURHOOD_LIST=6,7,8,9,10,11,12

VOXEL_SIZES="0.7 1.0 1.5 2.0 3.0 4.0 5.0 6.0"

#define LLC_16B 0
#define LLC_12B 1
#define DLLC_16B 2
#define DLLC_12B 3
#define LBP_NN_1ST 4 // LBP NN first
#define LBP_NN_LAST 5 // LBP NN last
#define LBP_UNIFORM 6
#define LBP_ROT_INV 7
#define LBP_NON_ROT_INV 8
#define LBP_ROT_INV_VAR 9
#define DLCP_8B 10 // Local Cielab Distance Pattern

create_directories_journal

generate_voxel_sizes

generate_fv_queiroz_no_voxelize


# generate_fv_queiroz_voxelized

# calculate_histogram_difference_queiroz ?

#correlate_python
