#!/bin/sh

# our library
. ./lbp_functions.sh

# PREFIX=/usr
PREFIX=/home/rafael2k

PC_METRIC_BIN=${PREFIX}/bin/pc_metric
CORRELATE_BIN=${PREFIX}/bin/correlate
COMPARE_HISTOGRAM_BIN=${PREFIX}/bin/compare_histogram
VOXELIZE_BIN=${PREFIX}/bin/optimize_voxel_size
PEDRO_BIN=/home/rafael2k/pc_metric/scripts/predicted_mos.py

DATA_PATH=/mnt/ssd/RafaelDiniz/queiroz
CORRELATION_OUTPUT=correlation.csv
SCORE_OUTPUT=scores.csv

OUTPUT_DIR=/mnt/ssd/RafaelDiniz/queiroz-processed-voxelized

# for python pre-processing step
PY_OUTPUT_DATA=data.csv
PY_MOS_INPUT=scores-mean.txt
PY_CORRELATION_OUTPUT=correlation-py.csv

mkdir -p ${OUTPUT_DIR}

mkdir -p ${OUTPUT_DIR}/a
mkdir -p ${OUTPUT_DIR}/b
mkdir -p ${OUTPUT_DIR}/c
mkdir -p ${OUTPUT_DIR}/d
mkdir -p ${OUTPUT_DIR}/e
mkdir -p ${OUTPUT_DIR}/f

calculate_histogram_queiroz

#last (ganhou!)
#calculate_histogram_voxelize_auto

#calculate_histogram_queiroz_no_voxelize

#calculate_histogram_difference_queiroz

#correlate_python
