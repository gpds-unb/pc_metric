#!/bin/sh

DATA_PATH=/mnt/ssd/RafaelDiniz/queiroz
CORRELATE_BIN=/usr/bin/correlate
OUTPUT_DIR=/mnt/ssd/RafaelDiniz/queiroz-processed
CORRELATION_OUTPUT=correlation.cvs

mkdir -p ${OUTPUT_DIR}

mkdir -p ${OUTPUT_DIR}/a
mkdir -p ${OUTPUT_DIR}/b
mkdir -p ${OUTPUT_DIR}/c
mkdir -p ${OUTPUT_DIR}/d
mkdir -p ${OUTPUT_DIR}/e
mkdir -p ${OUTPUT_DIR}/f

for j in a b c d e f; do

    ${CORRELATE_BIN} -s 60 ${DATA_PATH}/scores.csv ${OUTPUT_DIR}/${j}/scores.csv > ${OUTPUT_DIR}/${j}/correlation.txt

done;

