#!/bin/sh

# X -config /root/xorg.conf :1
# export DISPLAY=:1.0
# xhost +

DATA_PATH=/mnt/ssd/RafaelDiniz/queiroz-processed
VIEWER_BIN=/usr/bin/viewer

cd ${DATA_PATH}

for i in a b c e d f; do
  cd ${DATA_PATH}/${i}

  for j in $(ls -1 *.ply); do
    ${VIEWER_BIN} ${j} $(basename ${j} .ply).png
  done;
done;

