#!/bin/sh

DATA_PATH=/mnt/ssd/RafaelDiniz/queiroz
NORMALS_PATH=/mnt/ssd/RafaelDiniz/queiroz-normals/
COMPARE_HISTOGRAM_BIN=/usr/bin/compare_histogram
OUTPUT_DIR=/mnt/ssd/RafaelDiniz/queiroz-processed
SCORE_OUTPUT=scores.csv

mkdir -p ${OUTPUT_DIR}

mkdir -p ${OUTPUT_DIR}/a
mkdir -p ${OUTPUT_DIR}/b
mkdir -p ${OUTPUT_DIR}/c
mkdir -p ${OUTPUT_DIR}/d
mkdir -p ${OUTPUT_DIR}/e
mkdir -p ${OUTPUT_DIR}/f

for j in a b c d e f; do
    cd ${OUTPUT_DIR}/${j}

    rm -f ${SCORE_OUTPUT}
    
    for i in $(ls -1 *hidden*.bin); do
	
	for k in $(ls -1 $(basename ${i} _hidden-hist.bin)*octree*.bin); do
	    ${COMPARE_HISTOGRAM_BIN} ${OUTPUT_DIR}/${j}/$(basename ${i} _hidden-hist.bin)-hist.bin ${OUTPUT_DIR}/${j}/${k} >> ${SCORE_OUTPUT}
	    echo >> ${SCORE_OUTPUT}
	done;

	${COMPARE_HISTOGRAM_BIN} ${OUTPUT_DIR}/${j}/$(basename ${i} _hidden-hist.bin)-hist.bin ${OUTPUT_DIR}/${j}/${i} >> ${SCORE_OUTPUT}
	echo >> ${SCORE_OUTPUT}

    done;

done;

    
