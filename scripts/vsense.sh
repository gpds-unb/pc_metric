#!/bin/sh

DATA_PATH=/mnt/ssd/RafaelDiniz/vsense/
PC_METRIC_BIN=/usr/bin/pc_metric

OUTPUT_DIR=/mnt/ssd/RafaelDiniz/vsense-processed

mkdir -p ${OUTPUT_DIR}

mkdir -p ${OUTPUT_DIR}/a
mkdir -p ${OUTPUT_DIR}/b
mkdir -p ${OUTPUT_DIR}/c
mkdir -p ${OUTPUT_DIR}/d
mkdir -p ${OUTPUT_DIR}/e
mkdir -p ${OUTPUT_DIR}/f

for name in referencePointClouds/matis_ply_062K  referencePointClouds/matis_ply_127K  referencePointClouds/matis_ply_250K  referencePointClouds/matis_ply_495K  referencePointClouds/rafa_ply_062K  referencePointClouds/rafa_ply_127K  referencePointClouds/rafa_ply_250K  referencePointClouds/rafa_ply_495K; do
  cd ${DATA_PATH}/${name}

  for j in a b c d e f; do
    for i in $(ls -1 *.ply); do
      $PC_METRIC_BIN -i ${i} -m ${OUTPUT_DIR}/${j}/$(basename ${i} .ply)-feature.ply -$j > ${OUTPUT_DIR}/${j}/$(basename $i .ply)-hist.gnuplot
      gnuplot ${OUTPUT_DIR}/${j}/$(basename ${i} .ply)-hist.gnuplot
      mv $(basename ${i} .ply)-histogram.png ${OUTPUT_DIR}/${j}/
    done;
  done;

done;

cd ${DATA_PATH}/decodedPointClouds

for name in $(ls -1); do
  cd ${DATA_PATH}/decodedPointClouds/${name}

  for j in a b c d e f; do
    for i in $(ls -1 *.ply); do
      $PC_METRIC_BIN -i ${i} -m ${OUTPUT_DIR}/${j}/$(basename ${i} .ply)-feature.ply -$j > ${OUTPUT_DIR}/${j}/$(basename $i .ply)-hist.gnuplot
      gnuplot ${OUTPUT_DIR}/${j}/$(basename ${i} .ply)-hist.gnuplot
      mv $(basename ${i} .ply)-histogram.png ${OUTPUT_DIR}/${j}/
    done;
  done;

done;
