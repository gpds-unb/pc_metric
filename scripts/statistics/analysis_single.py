import os
import pandas as pd
import numpy as np
from scipy.stats import pearsonr, spearmanr, kendalltau

from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.linear_model import ARDRegression, BayesianRidge
from sklearn.linear_model import Lars, ElasticNet, ElasticNetCV, Lasso
from sklearn.linear_model import RANSACRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neural_network import MLPRegressor
from xlsxwriter.utility import xl_rowcol_to_cell


class logistic:
  __metaclass__ = type
  pass


models = [
    RandomForestRegressor(),
    ExtraTreesRegressor(),
    GradientBoostingRegressor(),
    BayesianRidge(),
    ARDRegression(
        n_iter=300, tol=0.001, alpha_1=1e-06, alpha_2=1e-06, lambda_1=1e-06,
        lambda_2=1e-06, compute_score=False, threshold_lambda=10000.0,
        fit_intercept=True, normalize=False, copy_X=True, verbose=False
    ),
    Lars(),
    ElasticNet(),
    ElasticNetCV(),
    Lasso(),
    RANSACRegressor(),
    KNeighborsRegressor(
        n_neighbors=5, weights='uniform', algorithm='auto', leaf_size=30,
        p=3, metric='minkowski', metric_params=None, n_jobs=None
    ),
    MLPRegressor(),
    logistic()
]


def PCC(predictions, targets):
    return pearsonr(predictions, targets)[0]


def SROCC(predictions, targets):
    return spearmanr(predictions, targets)[0]


def RMSE(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())


def highlight_max(data, color='red'):
    attr = 'background-color: {}'.format(color)
    data = data.astype(float)
    if data.ndim == 1:  
        is_max = data == data.max()
        return [attr if v else '' for v in is_max]
    else: 
        is_max = data == data.max().max()
        return pd.DataFrame(np.where(is_max, attr, ''),
                            index=data.index, columns=data.columns)


def run(patterns):
    types = [type(m).__name__ for m in models]
    heading = ['pattern', 'correlation'] + types
    out = [heading]
    for pattern in patterns:
        for score in [SROCC, PCC, RMSE]:
            line = [pattern, score.__name__]
            for model in models:
                modelname = type(model).__name__
                filename = "simulations_{m}/results-{p}.csv"
                filename = filename.format(m=modelname, p=pattern)
                df = pd.read_csv(filename)
                s = score(df.MOS, df.MOSp)
                line.append(s)
            out.append(line)
    df_out = pd.DataFrame.from_dict(out)
    df_out.columns = df_out.iloc[0]
    df_out = df_out[1:]
    df_out.style.apply(highlight_max, subset=pd.IndexSlice[:, types])
    df_out.to_excel("table.xlsx", engine='xlsxwriter')




if __name__ == "__main__":
    run(['D1-S1', 'D1-S2', 'D2-S1', 'D2-S2', 'D3-S1', 'D3-S2'])