import pandas as pd
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.stats import pearsonr, spearmanr, kendalltau


def mean_squared_error(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())


def itu_logistic_function(x, b1, b2, b3, b4, wi, ei):
    v = (x-b3) / np.abs(b4)
    v = np.array(v, dtype=np.float128)
    e = np.exp(-v)
    e = np.array(e, dtype=np.float64)
    f = (b1 - b2) / (1.0 + e)
    return ei + wi * (f + b2)


def return_single_simulation(x_train, y_train, x_test, y_test):
    b1 = np.max(y_train)
    b2 = np.min(y_train)
    b3 = np.median(x_train)
    b4 = 1.0
    delta = np.std(y_train)
    wi = 1.0 / delta
    Yiw = wi * y_train
    ei = np.abs(x_test - y_test)
    eiw = wi * ei
    p = [b1, b2, b3, b4, wi, eiw]
    popt, pcov = curve_fit(itu_logistic_function,
                           x_train, Yiw, p0=p, maxfev=14000)
    [b1, b2, b3, b4, wi, eiw] = popt
    y_pred = itu_logistic_function(x_test, b1, b2, b3, b4, wi, eiw)
    residual = y_test - y_pred
    return [x_test, y_test, y_pred, residual, b1, b2, b3, b4, wi, eiw]


def run(ref_pattern, test_pattern):
    ref_filepath = "distances/results-{p}.csv".format(p=ref_pattern)
    test_filepath = "distances/results-{p}.csv".format(p=test_pattern)
    df_ref = pd.read_csv(ref_filepath)
    df_test = pd.read_csv(test_filepath)
    simulations = len(df_test.index)
    output = []

    x_train, y_train = df_ref.distancia_raw.values, df_ref.subjective_MOS.values

    for i in range(simulations):
        row = df_test.iloc[[i]]
        x_test, y_test = row.distancia_raw.values, row.subjective_MOS.values
        [x_test, y_test, y_pred, residual, b1, b2, b3, b4, wi,
            eiw] = return_single_simulation(x_train, y_train, x_test, y_test)

        line = {
            'simulation': i,
            'nome_do_pc': row["nome_do_pc"].tolist()[0],
            'nome_referencia': row["nome_referencia"].tolist()[0],
            'distancia_raw': x_test[0],
            'MOS': y_test[0],
            'MOSp': y_pred[0],
            'residual': residual[0],
            'b1': b1,
            'b2': b2,
            'b3': b3,
            'b4': b4,
            'wi': wi,
            'eiw': eiw
        }
        output.append(line)
    df_out = pd.DataFrame.from_dict(output)
    outpattern = "{r}_{t}".format(r=ref_pattern, t=test_pattern)
    output_filename = "simulation_logistic_cross/results-{p}.csv".format(
        p=outpattern)
    df_out.to_csv(output_filename)

    rmse = mean_squared_error(df_out.MOSp, df_out.MOS)
    SROCC = spearmanr(df_out.MOSp, df_out.MOS)[0]
    PCC = pearsonr(df_out.MOSp, df_out.MOS)[0]
    print(outpattern, "PCC=", PCC, "SROCC=", SROCC, "RMSE=", rmse)


if __name__ == "__main__":
    for ref_db in ['D1', 'D2', 'D3']:
        for test_db in ['D1', 'D2', 'D3']:
            for strategy in ['S1', 'S2']:
                ref = "{d}-{s}".format(d=ref_db, s=strategy)
                test = "{d}-{s}".format(d=test_db, s=strategy)
                if ref != test:
                    run(ref, test)
