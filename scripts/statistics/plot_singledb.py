import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style("white")
markers = ['x','o','v','^','<', '+', '*']

def run(pattern):
    filepath = "simulations_logistic/results-{p}.csv".format(p=pattern)
    df = pd.read_csv(filepath)
    marker = markers.pop()
    ax = sns.regplot(x="MOS", y="MOSp", data=df, label=pattern, marker=marker)
    ax.legend(loc="best")
    out = "plots_logistic/{p}.pdf".format(p=pattern)
    fig = ax.get_figure()
    fig.savefig(out)


if __name__ == "__main__":
    run('D1-S1')
    run('D2-S1')
    run('D3-S1')
    plt.close()
    run('D1-S2')
    run('D2-S2')
    run('D3-S2')
