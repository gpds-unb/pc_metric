#!/bin/sh

# our library
. ./lbp_functions.sh

# PREFIX=/usr
PREFIX=/home/rafael2k

PEDRO_BIN=/home/rafael2k/pc_metric/scripts/predicted_mos.py
MPEG_PCC_BIN=/home/rafael2k/metrics/mpeg-pcc-dmetric/test/pc_error
PC_CONVERT_BIN=${PREFIX}/bin/pc_convert
MATLAB_BIN=/mnt/hd/RafaelDiniz/MATLAB/R2018a/bin/matlab

CORRELATION_OUTPUT=correlation.csv
SCORE_OUTPUT=scores.csv

OUTPUT_DIR=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized

# for python pre-processing step
PY_OUTPUT_DATA=data.csv
PY_MOS_INPUT=results_mean.txt
PY_CORRELATION_OUTPUT=correlation-py.csv


DATA_PATH=/mnt/ssd/RafaelDiniz/QoMEX_2019/contents
NORMALS_DIR=/mnt/ssd/RafaelDiniz/QoMEX_2019/normals
DATA_PATH_XYZ=/mnt/ssd/RafaelDiniz/QoMEX_2019/xyz

MPEG_SCORE_OUTPUT=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-all.txt

PTPOINT_MSE=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-ptpointmse.txt
PTPOINT_MSE_PSNR=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-ptpointmsepsnr.txt

PTPOINT_H=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-ptpointh.txt
PTPOINT_H_PSNR=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-ptpointhpsnr.txt

PTPLANE_MSE=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-ptplanemse.txt
PTPLANE_MSE_PSNR=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-ptplanemsepsnr.txt

PTPLANE_H=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-ptplaneh.txt
PTPLANE_H_PSNR=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-ptplanehpsnr.txt

COLOR_TEMP=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-temp.txt

COLOR_MSE_Y=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-ymse.txt
COLOR_PSNR=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-ypsnr.txt
COLOR_H_Y=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-yh.txt
COLOR_H_PSNR=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-voxelized/mpeg-hpsnr.txt

ANGULAR_SCRIPT_TEMP=angular_script.m
ANGULAR_SCORE_OUTPUT_MSE=angular_scores-mse.csv
ANGULAR_SCORE_OUTPUT_RMS=angular_scores-rms.csv


mkdir -p ${OUTPUT_DIR}

#create_normals
#mpeg_pcc_metrics
#parse_mpeg_metrics


#angular_similarity

correlate_mpeg_python

