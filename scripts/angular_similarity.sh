#!/bin/sh

DATA_PATH=/mnt/ssd/RafaelDiniz/queiroz-xyz
MATLAB_BIN=/mnt/hd/RafaelDiniz/MATLAB/R2018a/bin/matlab
OUTPUT_DIR=/mnt/ssd/RafaelDiniz/queiroz-processed

SCRIPT_TEMP=angular_script.m
SCORE_OUTPUT=angular_scores.txt

mkdir -p ${OUTPUT_DIR}

rm -f ${OUTPUT_DIR}/${SCRIPT_TEMP}
rm -f ${OUTPUT_DIR}/${SCORE_OUTPUT}

cd ${DATA_PATH}

echo "fileID = fopen('${OUTPUT_DIR}/${SCORE_OUTPUT}','a');" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
# echo "cd '${DATA_PATH}';" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}

for i in $(ls -1 *hidden*.xyz); do

  # TODO: iterate in the options of the metric? ('MSE', ...)
  for k in $(ls -1 $(basename ${i} _hidden.xyz)*octree*.xyz); do
    echo "xyzPoints1 = load('${DATA_PATH}/${i}');" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "xyzPoints2 = load('${DATA_PATH}/${k}');" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "normals1 = pcnormals(ptCloud1);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1,'Normal',normals1);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "normals2 = pcnormals(ptCloud2);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2,'Normal',normals2);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'MSE');" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo -n "fprintf(fileID,'MSE,%.10f" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'RMS');" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo -n "fprintf(fileID,'RMS,%.10f" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}

  done;
    echo "xyzPoints1 = load('${DATA_PATH}/${i}');" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "xyzPoints2 = load('${DATA_PATH}/${i}');" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "normals1 = pcnormals(ptCloud1);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "ptCloud1 = pointCloud(xyzPoints1,'Normal',normals1);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "normals2 = pcnormals(ptCloud2);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "ptCloud2 = pointCloud(xyzPoints2,'Normal',normals2);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'MSE');" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo -n "fprintf(fileID,'%.10f" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}

    echo "[asBA, asAB, asSym] = angularSimilarity(ptCloud1, ptCloud2, 'RMS');" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo -n "fprintf(fileID,'%.10f" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo -n '\\n' >> ${OUTPUT_DIR}/${SCRIPT_TEMP}
    echo "', asSym);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}

done;

echo "fclose(fileID);" >> ${OUTPUT_DIR}/${SCRIPT_TEMP}

echo "run: cd ${OUTPUT_DIR}"
echo "run: ${MATLAB_BIN} -nodisplay -r \"run ${SCRIPT_TEMP},quit\""

