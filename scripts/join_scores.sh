#!/bin/bash

INPUT1=/mnt/ssd/RafaelDiniz/queiroz/scores-mean-epfl.txt
INPUT2=/mnt/ssd/RafaelDiniz/queiroz/scores-mean-unb.txt
TMP_FILE=/mnt/ssd/RafaelDiniz/queiroz/scores-joined.txt
OUTPUT_FILE=/mnt/ssd/RafaelDiniz/queiroz/scores-mean.txt

rm -f ${OUTPUT_FILE}

paste -d"\n" ${INPUT1} ${INPUT2} > ${TMP_FILE}

while read a; do 
    read b;
    awk -v a=$a -v b=$b 'BEGIN{print ((a + b)/2)}\' >> ${OUTPUT_FILE}
done < ${TMP_FILE}
