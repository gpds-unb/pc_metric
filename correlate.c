/*
 * Copyright (C) 2019 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <getopt.h>
#include <string.h>
#include <math.h>
#include "linasm/include/Statistics.h"

void fitting(int size, double *x, double *y, double *y_fit)
{
    int n = size;
    int i;
    double a,b;

    double xsum=0,x2sum=0,ysum=0,xysum=0;                //variables for sums/sigma of xi,yi,xi^2,xiyi etc
    for (i=0;i<n;i++)
    {
        xsum=xsum+x[i];                        //calculate sigma(xi)
        ysum=ysum+y[i];                        //calculate sigma(yi)
        x2sum=x2sum+pow(x[i],2);                //calculate sigma(x^2i)
        xysum=xysum+x[i]*y[i];                    //calculate sigma(xi*yi)
    }
    a=(n*xysum-xsum*ysum)/(n*x2sum-xsum*xsum);            //calculate slope
    b=(x2sum*ysum-xsum*xysum)/(x2sum*n-xsum*xsum);            //calculate intercept

    // double y_fit[n];                        //an array to store the new fitted values of y    
    for (i=0;i<n;i++)
        y_fit[i]=a*x[i]+b;                    //to calculate y(fitted) at given x points

    fprintf(stderr, "S.no \t x \t y(observed) \t y(fitted)\n");
    fprintf(stderr, "-----------------------------------------------------------------\n");

    for (i=0;i<n;i++)
        fprintf(stderr, "%d . %.10f %.10f %.10f\n", i+1, x[i], y[i], y_fit[i]);

    fprintf(stderr, "\nThe linear fit line is of the form: %.10fx + %0.10f\n\n", a, b);

}


double calc_rmse(double *array1, double *array2, size_t size)
{
    double sum = 0;

#pragma omp parallel for
    for (size_t i = 0; i < size; i++)
    {
#pragma omp atomic
        sum += pow(array1[i] - array2[i], 2);
    }

    return sqrt(sum / size);
}

int main(int argc, char *argv[])
{
    FILE *h1, *h2;
    size_t size;
    double *array1, *array2, *array2_fitted;
    double pearson = 0;
    double spearman = 0;
    double fechner = 0;
    double rmse = 0;
    size_t *rank1, *rank2;
    double mean1, mean2, mean2_fitted;
    double *tarr;
    size_t *trank;
    char foo[256];
    size_t n_scores = 20;     // queiroz csv default
    
    if (argc < 3){
    usage_info:
        fprintf(stderr, "Usage: %s [OPTIONS]\n", argv[0]);
        fprintf(stderr, "Usage example: %s -s <size> -n <nr_of_subjetive_scores> subjective_scores.csv objective_score.csv\n", argv[0]);
        fprintf(stderr, "Outputs the histogram distance.\n");
        fprintf(stderr, "\nOPTIONS:\n");
        fprintf(stderr, "    -s size           Size of the vector\n");
        fprintf(stderr, "    -n nr_of_subjetive_scores    Number of subjective scores for each object\n");
        
        return EXIT_SUCCESS;
    }

    int opt;
    while ((opt = getopt(argc, argv, "s:n:")) != -1){
        switch (opt){
        case 's':
            size = strtoull(optarg, NULL, 10);
            break;
        case 'n':
            n_scores = strtoull(optarg, NULL, 10);
            break;
        default:
            fprintf(stderr, "Wrong command line.\n");
            goto usage_info;
        }
    }

    h1 = fopen(argv[argc-2], "r");
    h2 = fopen(argv[argc-1], "r");

    if (h1 == NULL || h2 == NULL)
    {
        fprintf(stderr, "Error opening a file.\n");
        return EXIT_FAILURE;
    }

    // pearson and spearman arrays
    array1 = (double *) calloc(size, sizeof(double));
    array2 = (double *) calloc(size, sizeof(double));
    array2_fitted = (double *) calloc(size, sizeof(double));
    
    // sperman-only arrays
    rank1 = (size_t *) calloc(size, sizeof(size_t));
    rank2 = (size_t *) calloc(size, sizeof(size_t));
    tarr = (double *) calloc(size, sizeof(double));
    trank = (size_t *) calloc(size, sizeof(size_t));

    for (size_t i = 0; i < size; i++)
    {
        unsigned int sum = 0;
        unsigned int val;
        for (size_t j = 0; j < n_scores; j++) // n. of people scores
        {
            if (j != (n_scores - 1))
                fscanf(h1, "%u,", &val);
            else
                fscanf(h1, "%u%[^\n]\n", &val, foo);

            sum += val;
        }
        array1[i] = (double) sum / (double) n_scores;
        printf("%.10f\n", array1[i]);
    }

    for (size_t i = 0; i < size; i++)
    {
        double val;
        fscanf(h2, "%lf%[^\n]\n", &val, foo);
        array2[i] = val;
        // fprintf(stderr, "%.10f\n", array2[i]);
    }

    // fitting(size, array1, array2, array2_fitted);


    mean1 = Statistics_Mean_flt64(array1, size);
    mean2 = Statistics_Mean_flt64(array2, size);
    //mean2_fitted = Statistics_Mean_flt64(array2_fitted, size);
    
    pearson = Statistics_PearsonCorrelation_flt64(array1, array2, size, mean1, mean2);
    // pearson = Statistics_PearsonCorrelation_flt64(array1, array2_fitted, size, mean1, mean2_fitted);
    
    //printf("LCC,%.10f\n", pearson);

//    fechner = Statistics_FechnerCorrelation_flt64(array1, array2, size, mean1, mean2);

//    printf("Fechner = %.10f\n", fechner);

    spearman = Statistics_SpearmanCorrelation_flt64(array1, rank1, array2, rank2, tarr, trank, size );
//    spearman = Statistics_SpearmanCorrelation_flt64(array1, rank1, array2_fitted, rank2, tarr, trank, size );

    //printf("SRCC,%.10f\n", spearman);

    // rmse = calc_rmse(array1, array2_fitted, size);
    rmse = calc_rmse(array1, array2, size);

    //printf("RMSE,%.10f\n", rmse);

    free(array1);
    free(array2);
    free(array2_fitted);
    free(rank1);
    free(rank2);
    free(tarr);
    free(trank);

    return 0;
}
