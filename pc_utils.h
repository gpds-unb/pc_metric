/*
 * Copyright (C) 2019 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */

#ifndef __PC_UTILS
#define __PC_UTILS

#include <Open3D.h>

using namespace open3d;
using namespace std;

double rgb_to_y(double r, double g, double b, double max_value);

double rgb_to_balanced_ycbcr(double r, double g, double b, double max_value);

double get_angle( double *a, double *b, double* c);

void equation_plane(double x1, double y1, double z1,
                    double x2, double y2, double z2,
                    double x3, double y3, double z3);

void print_pointcloud(const geometry::PointCloud &pointcloud, bool print_points);
void print_voxelizedpc(const geometry::VoxelGrid &pointcloud, bool print_points);

#endif // __PC_UTILS
