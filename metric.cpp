/*
 * Copyright (C) 2019-2020 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */

#include <unistd.h>
#include <cstdint>
#include <sstream>
#include <iostream>
#include <memory>
#include <thread>

#include <Open3D.h>

#include "metric.h"
#include "pc_utils.h"
#include "textdesc/source/Lib/DefaultLBPUnit.h"
#include "textdesc/source/Lib/UniformLBPUnit.h"
#include "textdesc/source/Lib/RotationInvariantLBPUnit.h"
#include "textdesc/source/Lib/NonRotationInvariantUniformLBPUnit.h"
#include "textdesc/source/Lib/RotationInvariantVarianceLBPUnit.h"

using namespace open3d;
using namespace std;

int main(int argc, char *argv[])
{
    bool normalize = true;
    bool create_feature_pc = false;
    bool create_histogram = false;
    bool voxelize = false;
    bool voxelize_auto = false;
    bool divide_color_by_255 = false;
    double voxel_size = 1.0;
    int metric = DEFAULT_LBP;
    int end_of_scale = 256;
    char input_filename[MAX_FILENAME] = {0};
    char feature_pc_filename[MAX_FILENAME] = {0};
    char histogram_plot_filename[MAX_FILENAME] = {0};
    char histogram_filename[MAX_FILENAME] = {0};

    if (argc < 3){
    usage_info:
        fprintf(stderr, "Usage: %s [OPTIONS]\n", argv[0]);
        fprintf(stderr, "Usage example: %s -i input_pc.ply -m output_feature_pc.ply -h histogram.bin -a\n", argv[0]);
        fprintf(stderr, "\nOPTIONS:\n");
        fprintf(stderr, "    -i input_pc.ply         PC to be evaluated\n");
        fprintf(stderr, "    -h histogram.bin        Save the histogram as a vector of double.\n");
        fprintf(stderr, "    -m output_feature.ply   Output a feature PC map\n");
        fprintf(stderr, "    -y                      Divide color attributes by 255\n");
        fprintf(stderr, "    -v voxel_size           Voxelize and use voxel size specified (or \"auto\" for automatic voxelization)\n");
        fprintf(stderr, "    -a                      DefaultLBP\n");
        fprintf(stderr, "    -b                      UniformLBPUnit\n");
        fprintf(stderr, "    -c                      RotationInvariantLBPUnit\n");
        fprintf(stderr, "    -d                      NonRotationInvariantUniformLBPUnit\n");
        fprintf(stderr, "    -e                      RotationInvariantVarianceLBPUnit\n");
        fprintf(stderr, "    -f                      Alternative DefaultLBP\n");

        return EXIT_SUCCESS;
    }

    int opt;
    while ((opt = getopt(argc, argv, "i:h:m:yabcdefv:")) != -1){
        switch (opt){
        case 'i':
            strncpy (input_filename, optarg, MAX_FILENAME);
            break;
        case 'h':
            create_histogram = true;
            strncpy (histogram_filename, optarg, MAX_FILENAME);
            break;
        case 'm':
            create_feature_pc = true;
            strncpy (feature_pc_filename, optarg, MAX_FILENAME);
            break;
        case 'y':
            divide_color_by_255 = true;
            break;
        case 'a':
            metric = DEFAULT_LBP;
            end_of_scale = 256;
            break;
        case 'b':
            metric = UNIFORM_LBP;
            end_of_scale = 10;
            break;
        case 'c':
            metric = ROTATION_INVARIANT_LBP;
            end_of_scale = 256;
            break;
        case 'd':
            metric = NON_ROTATION_INVARIANT_UNIFORM_LBP;
            end_of_scale = 59;
            break;
        case 'e':
            metric = ROTATION_INVARIANT_VARIANCE;
            end_of_scale = 16384;
            break;
        case 'f':
            metric = DEFAULT_LBP_ALT;
            end_of_scale = 256;
            break;
        case 'v':
            if (!strncmp(optarg, "auto", 4))
            {
                voxelize_auto = true;
            }
            else
            {
                voxelize = true;
                voxel_size = atof(optarg);
            }
            break;
        default:
            fprintf(stderr, "Wrong command line.\n");
            goto usage_info;
        }
    }

    uint64_t *histogram = (uint64_t *) alloca(end_of_scale * sizeof(uint64_t));
    double *histogram_normalized = (double *) alloca(end_of_scale * sizeof(double));

    auto pc = make_shared<geometry::PointCloud>();
    auto feature_pc = make_shared<geometry::PointCloud>();
    auto vis = std::make_shared<visualization::Visualizer>();

    if (io::ReadPointCloud(input_filename, *pc))
    {
        fprintf(stderr, "Successfully read %s\n", input_filename);
    }
    else {
        fprintf(stderr, "Failed to read %s.\n", input_filename);
        return EXIT_FAILURE;
    }

    // workaround 3dtk 2^8 unsigned integer rgb range
    if (strstr (input_filename, ".xyzrgb") || divide_color_by_255)
    {
        for (size_t i = 0; i < pc->points_.size(); i++) {
            pc->colors_[i](0) = pc->colors_[i](0) / 255.0;
            pc->colors_[i](1) = pc->colors_[i](1) / 255.0;
            pc->colors_[i](2) = pc->colors_[i](2) / 255.0;
        }
    }

    // printing PC info (set the second argument true to print all the points)
    // print_pointcloud(*pc, true);

    if (voxelize)
    {
      //        // Voxelization process
      // auto voxel_pc = geometry::VoxelGrid::CreateFromPointCloud(*pc, voxel_size);
      // print_voxelizedpc(*voxel_pc, false);

        pc = pc->VoxelDownSample(voxel_size);

        //print_pointcloud(*pc, false);

    }

    geometry::KDTreeFlann kdtree;
    
    if (voxelize_auto)
    {
      //        double close_nn = 9999999;
      //        double distant_nn = 0;
        double average_dist = 0;

        kdtree.SetGeometry(*pc);

#pragma omp parallel for num_threads(32) reduction(+:average_dist) schedule(dynamic,1000)
        for (size_t i = 0; i < pc->points_.size(); i++)
        {
            std::vector<int> indices_vec(2);
            std::vector<double> dists_vec(2);

            kdtree.SearchKNN(pc->points_[i], 2, indices_vec, dists_vec);

            Eigen::Vector3d point_close = pc->points_[i];
            Eigen::Vector3d point_distant = pc->points_[indices_vec[1]];

            double dist = sqrt ( (pow((point_distant[0] - point_close[0]), 2)) +
                                 (pow((point_distant[1] - point_close[1]), 2)) +
                                 (pow((point_distant[2] - point_close[2]), 2)) );

            average_dist += dist;
#if 0        
            if (dist < close_nn)
            {
                close_nn = dist;
            }
            if (dist > distant_nn)
            {
                distant_nn = dist;
            }
#endif       
        }
        average_dist /= pc->points_.size();
        //fprintf(stderr, "close nn: %.16f distant nn: %.16f average nn: %.16f\n", close_nn, distant_nn, average_dist);
        fprintf(stderr, "%.16f\n", average_dist);

        // voxel_size = average_dist * 1.4;
        voxel_size = average_dist * 2;
        fprintf(stderr, "%0.16f\n", voxel_size);

        //        print_pointcloud(*pc, false);
        pc = pc->VoxelDownSample(voxel_size);
        //        print_pointcloud(*pc, false);
        
    }

    kdtree.SetGeometry(*pc);
      
    // clear the histogram
    memset (histogram, 0, sizeof(uint64_t) * end_of_scale);

    // 8 nearest neighborhood LBP (first is the point itself, so nn = 9)
    int nn = 9;

//    unsigned int max_label = 0;

    // for each point in the PC - parallel execution using OpenMP
#pragma omp parallel for
    for (size_t i = 0; i < pc->points_.size(); i++) {
        unsigned int label = 0;
        // for fast retrieval of nearest neighbor we use kd-tree
        std::vector<int> indices_vec(nn);
        std::vector<double> dists_vec(nn);

        // get the nearest neighbors of point with index "i"
        kdtree.SearchKNN(pc->points_[i], nn, indices_vec, dists_vec);

        const Eigen::Vector3d &point_color = pc->colors_[i];
        double point_y = rgb_to_y(point_color(0), point_color(1), point_color(2), 1.0);
        unsigned int center = point_y * 255;

        vector<unsigned int> neighbours(nn-1);

        for (int j = 1 ; j < nn; j++)
        { // starting from 1!
            const Eigen::Vector3d &color = pc->colors_[indices_vec[j]];
            double y = rgb_to_y(color(0), color(1), color(2), 1.0);

            neighbours[j-1] = y * 255;

            // old code
            if (metric == DEFAULT_LBP_ALT)
            {
                if (y > point_y)
                    label |= 1 << (nn - j - 1); // minus 1 because we summed 1 previously (nn = 9 and not 8)
            }
        }

        switch(metric){
        case DEFAULT_LBP:
            label = texdesc::DefaultLBPUnit<unsigned int>::getLabel(center, neighbours);
            break;
        case UNIFORM_LBP:
            label = texdesc::UniformLBPUnit<unsigned int>::getLabel(center, neighbours);
            break;
        case ROTATION_INVARIANT_LBP:
            label = texdesc::RotationInvariantLBPUnit<unsigned int>::getLabel(center, neighbours);
            break;
        case NON_ROTATION_INVARIANT_UNIFORM_LBP:
            label = texdesc::NonRotationInvariantUniformLBPUnit<unsigned int>::getLabel(center, neighbours);
            break;
        case ROTATION_INVARIANT_VARIANCE:
            label = texdesc::RotationInvariantVarianceLBPUnit<unsigned int>::getLabel(center, neighbours);
            break;
        }

//        if (label > max_label)
//            max_label = label;
        if (create_feature_pc == true)
        {
        #pragma omp critical
            {
                feature_pc->points_.push_back(pc->points_[i]);
                feature_pc->colors_.push_back(Eigen::Vector3d(
                                                  (double) label / (double) (end_of_scale-1),
                                                  (double) label / (double) (end_of_scale-1),
                                                  (double) label / (double) (end_of_scale-1))); // for grayscale, R = G = B = Y
            }
        }
        // printf("%d ", result);
        #pragma omp atomic
        histogram[label]++;
    }

//    fprintf(stderr, "max_label = %u\n", max_label);

    if (normalize == true)
    {
        for (int i = 0; i < end_of_scale; i++)
            histogram_normalized[i] = (double) histogram[i] / (double) pc->points_.size();
    }

    if (create_feature_pc == true)
    {
#if ((OPEN3D_VERSION_MAJOR == 0 ) &&  (OPEN3D_VERSION_MINOR < 10))
        io::WritePointCloudToPLY(feature_pc_filename, *feature_pc, true, false);
#else
        io::WritePointCloudOption pc_params = io::WritePointCloudOption (true, false, false,  NULL);
        io::WritePointCloudToPLY(feature_pc_filename, *feature_pc, pc_params);
#endif
    }

    if (create_histogram == true)
    {
        FILE *hist_fp = fopen(histogram_filename, "w");
        if (hist_fp != NULL)
            fwrite(normalize ? (void *)histogram_normalized : (void *)histogram, 8, end_of_scale, hist_fp); // ps: sizeof(double) == sizeof(uint64_t) == 8
        else
            fprintf(stderr, "Could not write histogram output path: %s.\n", histogram_filename);
        fclose(hist_fp);
    }
        //

    strcpy(histogram_plot_filename, basename(input_filename));
    char *temp = rindex(histogram_plot_filename, '.');
    sprintf(temp, "-histogram.png");

    // print histogram to stdout in a gnuplot format
    printf("set title \"PC Metric\"\n");
    printf("set xlabel \"Value\"\n");
    printf("set ylabel \"Frequency\"\n");
    printf("set terminal png\n");
    printf("set output \"%s\"\n", histogram_plot_filename);
    printf("set style fill solid\n");
    printf("set xrange \[0:%d]\n", end_of_scale);
//    printf("set yrange \[0:0.25]\n");
    printf("$data << EOD\n");

    for (int i = 0; i < end_of_scale; i++)
    {
        if (normalize == false)
            printf("%d %lu\n", i, histogram[i]);
        if (normalize == true)
            printf("%d %.10f\n", i, histogram_normalized[i]);
    }

    printf("EOD\n");
    printf("plot \"$data\" with boxes\n");

    return EXIT_SUCCESS;
}
