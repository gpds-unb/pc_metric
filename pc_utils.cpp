/*
 * Copyright (C) 2019 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include <Eigen/Geometry>

#include "pc_utils.h"

double rgb_to_y(double r, double g, double b, double max_value)
{
    // BT.601 luminance formula
    // double y = (0.299 * r) + (0.587 * g) + (0.114 * b);

    // BT.709 luminance formula
    double y = (0.2126 * r) + (0.7152 * g) + (0.0722 * b);

    if (y > max_value)
    {
        fprintf(stderr, "Clamped Y value!\n");
        y = max_value;
    }

    return y;
}

double rgb_to_balanced_ycbcr(double r, double g, double b, double max_value)
{
    // BT.601 luminance formula
    // double y = (0.299 * r) + (0.587 * g) + (0.114 * b);

    // BT.709 luminance formula
    double y = (0.2126 * r) + (0.7152 * g) + (0.0722 * b);

    double cb  = (b-y) / 1.8556;
    double cr = (r-y) / 1.5748;
    cb = cb + 0.5;  // we sum 0.5 in order not to have negative values.
    cr = cr + 0.5;  // we sum 0.5 in order not to have negative values.

    double value = ((6 * y) + cb + cr) / 8;

    if (value > max_value)
    {
        fprintf(stderr, "Clamped Balanced YCbCr value! %f\n", value);
        value = max_value;
    }

    return value;
}

#if 0
// Driver Code
int main()
{
    float x1 =-1;
    float y1 = 2;
    float z1 = 1;
    float x2 = 0;
    float y2 =-3;
    float z2 = 2;
    float x3 = 1;
    float y3 = 1;
    float z3 =-4;
    equation_plane(x1, y1, z1, x2, y2, z2, x3, y3, z3);
    return 0;
}
#endif
void equation_plane(double x1, double y1, double z1,
                    double x2, double y2, double z2,
                    double x3, double y3, double z3)
{
    double a1 = x2 - x1;
    double b1 = y2 - y1;
    double c1 = z2 - z1;
    double a2 = x3 - x1;
    double b2 = y3 - y1;
    double c2 = z3 - z1;
    double a = b1 * c2 - b2 * c1;
    double b = a2 * c1 - a1 * c2;
    double c = a1 * b2 - b1 * a2;
    double d = (- a * x1 - b * y1 - c * z1);
    fprintf(stderr, "equation of plane is %.2f x + %.2f"
        " y + %.2f z + %.2f = 0.",a,b,c,d);
    return;
}

#if 0
double a[] = {1, 0, 0};

double b[] = {0, 0, 0};

double c[] = {0, 1, 0};

std::cout<< "The angle of abc is " << get_angle(a,b,c)<< "º " << std::endl;
#endif
double get_angle( double* a, double* b, double* c )
//double get_acos(double x1, double y1, double z1,
//                    double x2, double y2, double z2,
//                    double x3, double y3, double z3)
{

    double ab[3] = { b[0] - a[0], b[1] - a[1], b[2] - a[2] };
    double bc[3] = { c[0] - b[0], c[1] - b[1], c[2] - b[2]  };

    double abVec = sqrt(ab[0] * ab[0] + ab[1] * ab[1] + ab[2] * ab[2]);
    double bcVec = sqrt(bc[0] * bc[0] + bc[1] * bc[1] + bc[2] * bc[2]);

    double abNorm[3] = {ab[0] / abVec, ab[1] / abVec, ab[2] / abVec};
    double bcNorm[3] = {bc[0] / bcVec, bc[1] / bcVec, bc[2] / bcVec};

    double res = abNorm[0] * bcNorm[0] + abNorm[1] * bcNorm[1] + abNorm[2] * bcNorm[2];

    return acos(res)*180.0/ 3.141592653589793;
}


void print_pointcloud(const geometry::PointCloud &pointcloud, bool print_points)
{
    bool pointcloud_has_normal = pointcloud.HasNormals();

    fprintf(stderr, "Pointcloud has %d points.\n",
            (int)pointcloud.points_.size());

    Eigen::Vector3d min_bound = pointcloud.GetMinBound();
    Eigen::Vector3d max_bound = pointcloud.GetMaxBound();
    fprintf(stderr, "Bounding box is: (%.4f, %.4f, %.4f) - (%.4f, %.4f, %.4f)\n",
            min_bound(0), min_bound(1), min_bound(2),
            max_bound(0), max_bound(1), max_bound(2));

    if (print_points == true)
    {

        for (size_t i = 0; i < pointcloud.points_.size(); i++) {
            if (pointcloud_has_normal) {
                const Eigen::Vector3d &point = pointcloud.points_[i];
                const Eigen::Vector3d &normal = pointcloud.normals_[i];
                fprintf(stderr, "xyzn %.6f %.6f %.6f %.6f %.6f %.6f\n",
                          point(0), point(1), point(2),
                          normal(0), normal(1), normal(2));
            } else {
                const Eigen::Vector3d &point = pointcloud.points_[i];
                fprintf(stderr, "xyz %.6f %.6f %.6f\n", point(0), point(1), point(2));
            }
            const Eigen::Vector3d &color = pointcloud.colors_[i];
            fprintf(stderr, "rgb %.6f %.6f %.6f\n", color(0), color(1), color(2));
        }
    }

}

void print_voxelizedpc(const geometry::VoxelGrid &pointcloud, bool print_points)
{
    fprintf(stderr, "Voxelized Pointcloud has %d points.\n",
            (int)pointcloud.voxels_.size());

    Eigen::Vector3d min_bound = pointcloud.GetMinBound();
    Eigen::Vector3d max_bound = pointcloud.GetMaxBound();
    fprintf(stderr, "Bounding box is: (%.4f, %.4f, %.4f) - (%.4f, %.4f, %.4f)\n",
            min_bound(0), min_bound(1), min_bound(2),
            max_bound(0), max_bound(1), max_bound(2));

    fprintf(stderr, "Origin: (%.4f, %.4f, %.4f)\n",
                     pointcloud.origin_(0), pointcloud.origin_(1),
                     pointcloud.origin_(2));
    fprintf(stderr,"Voxel size: %.4f\n", pointcloud.voxel_size_);

    if (print_points == true)
    {
#if 0
        if(voxel_pc->HasVoxels())
            fprintf(stderr, "I have voxels\n");
        else
            fprintf(stderr, "I don't have voxels\n");

        for (size_t i = 0; i < pointcloud.points_.size(); i++) {
            if (pointcloud_has_normal) {
                const Eigen::Vector3d &point = pointcloud.points_[i];
                const Eigen::Vector3d &normal = pointcloud.normals_[i];
                fprintf(stderr, "xyzn %.6f %.6f %.6f %.6f %.6f %.6f\n",
                          point(0), point(1), point(2),
                          normal(0), normal(1), normal(2));
            } else {
                const Eigen::Vector3d &point = pointcloud.points_[i];
                fprintf(stderr, "xyz %.6f %.6f %.6f\n", point(0), point(1), point(2));
            }
            const Eigen::Vector3d &color = pointcloud.colors_[i];
            fprintf(stderr, "rgb %.6f %.6f %.6f\n", color(0), color(1), color(2));
        }
#endif
    }

}
