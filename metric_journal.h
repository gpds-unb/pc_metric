/*
 * Copyright (C) 2020 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */


#ifndef __METRIC_JOURNAL
#define __METRIC_JOURNAL

#define MAX_FILENAME 4096

#define MAX_NR_METRICS 16
#define MAX_NN_LIST_SIZE 16

#define LLC_16B 0
#define LLC_12B 1
#define DLLC_16B 2
#define DLLC_12B 3
#define LBP_NN_1ST 4 // LBP NN first
#define LBP_NN_LAST 5 // LBP NN last
#define LBP_UNIFORM 6
#define LBP_ROT_INV 7
#define LBP_NON_ROT_INV 8
#define DLCP_12B 9 // Local Cielab Distance Pattern 12-bit
#define DLCP_8B 10 // Local Cielab Distance Pattern 8-bit
#define DGEO_16B 11
#define DGEO_12B 12
#define DGEO_8B 13
#define COMB_12B 14
#define COMB_16B 15

#endif /* __METRIC_JOURNAL  */
