/*
 * Copyright (C) 2019 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */


#ifndef __METRIC
#define __METRIC

#define MAX_FILENAME 4096
#define MAX_LBP_VALUE 256

// algorithm list
#define DEFAULT_LBP 1
#define UNIFORM_LBP 2
#define ROTATION_INVARIANT_LBP 3
#define NON_ROTATION_INVARIANT_UNIFORM_LBP 4
#define ROTATION_INVARIANT_VARIANCE 5
#define DEFAULT_LBP_ALT 6

// Voxel strategies
#define VOXEL_FIXED 0
#define VOXEL_STRATEGY_3 3
#define VOXEL_STRATEGY_4 4


// leftover of some ideas...
    // Lets convert the PC to a triangle mesh? (need to calculate normals first)
    //     static std::shared_ptr<TriangleMesh> CreateFromPointCloudBallPivoting(
    //            const PointCloud &pcd, const std::vector<double> &radii);
    // if using a triangle mesh, we can use IsWatertight() function...
    // mesh will also help with the complexity of the LBP calculation (I think)
    //    auto mesh = std::make_shared<geometry::TriangleMesh>();
    //    auto voxel = std::make_shared<geometry::VoxelGrid>();


#endif
