#
# Copyright (C) 2019-2020 Rafael Diniz <rafael@riseup.net>
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

PREFIX=/usr

OPEN3D_PREFIX=/usr

# Compilers used
# x86_64 assembly: FASM
# C: gcc
# C++: g++
AS=fasm
CC=gcc
CPP=g++

CFLAGS= -g -std=c11 -Wall

# no openmp
#CXXFLAGS= -g -std=c++17 -fPIC -Wall  -Wno-deprecated-declarations -Wno-unused-result -DUNIX -I$(OPEN3D_PREFIX)/include \
#	-I$(OPEN3D_PREFIX)/include/Open3D \
#	-I$(OPEN3D_PREFIX)/include/Open3D/3rdparty/Eigen \
#	-I$(OPEN3D_PREFIX)/include/Open3D/3rdparty/fmt/include \
#	-I/usr/include/libdrm -I. -I./ColorSpace
#LDFLAGS= -g -std=c++17 -fPIC -Wl,--no-as-needed -rdynamic -lOpen3D -lGLEW -lGLU -lGL -lglfw -lpng16 -lz
#
#CXXFLAGS_FOR_COLORSPACE= -g -std=c++17 -fPIC -Wall  -Wno-deprecated-declarations -Wno-unused-result -I./ColorSpace


CXXFLAGS= -g -std=c++17 -fPIC -fopenmp -Wall  -Wno-deprecated-declarations -Wno-unused-result -DUNIX -I$(OPEN3D_PREFIX)/include \
	-I$(OPEN3D_PREFIX)/include/Open3D -I$(OPEN3D_PREFIX)/include/open3d \
	-I${PREFIX}/include/eigen3/ \
	-I$(OPEN3D_PREFIX)/include/Open3D/3rdparty/Eigen -I$(OPEN3D_PREFIX)/include/open3d/3rdparty/Eigen \
	-I$(OPEN3D_PREFIX)/include/Open3D/3rdparty/fmt/include -I$(OPEN3D_PREFIX)/include/open3d/3rdparty/fmt/include \
	-I${PREFIX}/include/libdrm -I. -I./ColorSpace
LDFLAGS= -g -std=c++17 -fPIC -fopenmp -Wl,--no-as-needed -rdynamic -lOpen3D -lGLEW -lGLU -lGL -lglfw -lpng16 -lz

CXXFLAGS_FOR_COLORSPACE= -g -std=c++17 -fPIC -fopenmp -Wall  -Wno-deprecated-declarations -Wno-unused-result -I./ColorSpace


all: pc_metric_journal pc_metric pc_metric2 pc_metric3 pc_metric4 compare_histogram compare_histogram_journal compare_histogram_journal_split correlate viewer pc_convert create_normals optimize_voxel_size

SRCS=$(wildcard *.cpp)
OBJS=$(SRCS:.cpp=.o)

# main metric binary rules
pc_metric_journal: metric_journal.o pc_utils.o ColorSpace/ColorSpace.o ColorSpace/Conversion.o ColorSpace/Comparison.o
	$(CPP) $(LDFLAGS) -o $@ $^

pc_metric: metric.o pc_utils.o
	$(CPP) $(LDFLAGS) -o $@ $^

pc_metric2: metricv2.o pc_utils.o
	$(CPP) $(LDFLAGS) -o $@ $^

pc_metric3: metricv3.o pc_utils.o
	$(CPP) $(LDFLAGS) -o $@ $^

pc_metric4: metricv4.o pc_utils.o
	$(CPP) $(LDFLAGS) -o $@ $^

metric.o : metric.cpp metric.h
	$(CPP) -c $(CXXFLAGS) $< -o $@

metricv2.o: metricv2.cpp metric.h
	$(CPP) -c $(CXXFLAGS) $< -o $@

metricv3.o: metricv3.cpp metric.h
	$(CPP) -c $(CXXFLAGS) $< -o $@

metricv4.o: metricv4.cpp metric.h
	$(CPP) -c $(CXXFLAGS) $< -o $@

pc_utils.o : pc_utils.cpp pc_utils.h
	$(CPP) -c $(CXXFLAGS) $< -o $@

# correlate binary
correlate: correlate.c linasm/Statistics.o linasm/Array.o
	$(CC) $(CFLAGS) -Ilinasm/include -fopenmp -Wl,--no-as-needed -lm -o $@ $^

# many utils...
viewer: viewer.cpp ColorSpace/ColorSpace.cpp ColorSpace/Conversion.cpp ColorSpace/Comparison.cpp
	$(CPP) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

cylinder: cylinder.cpp
	$(CPP) $(CXXFLAGS) $(LDFLAGS) -o $@ $^


optimize_voxel_size: optimize_voxel_size.cpp pc_utils.o
	$(CPP) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

create_normals: create_normals.cpp pc_utils.o
	$(CPP) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

pc_convert: pc_convert.cpp
	$(CPP) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

compare_histogram: compare_histogram.c
	$(CC) $(CFLAGS) -Wl,--no-as-needed -lm -o $@ $^

compare_histogram_journal: compare_histogram_journal.c csv.c
	$(CC) $(CFLAGS) -Wl,--no-as-needed -lm -o $@ $^

compare_histogram_journal_split: compare_histogram_journal_split.c csv.c
	$(CC) $(CFLAGS) -Wl,--no-as-needed -lm -o $@ $^

# linasm statistic functions
linasm/Statistics.o: linasm/source/Statistics.asm
	$(AS) $< $@

linasm/Array.o: linasm/source/Array.asm
	$(AS) $< $@

# color space stuff
ColorSpace/Comparison.o: ColorSpace/Comparison.cpp
	$(CPP) -c $(CXXFLAGS) $< -o $@

ColorSpace/Conversion.o: ColorSpace/Conversion.cpp
	$(CPP) -c $(CXXFLAGS) $< -o $@

ColorSpace/ColorSpace.o: ColorSpace/ColorSpace.cpp
	$(CPP) -c $(CXXFLAGS) $< -o $@

install: pc_metric
	install -d $(PREFIX)/bin
	install pc_metric $(PREFIX)/bin
	install pc_metric3 $(PREFIX)/bin
	install pc_metric4 $(PREFIX)/bin
	install pc_metric_journal $(PREFIX)/bin
	install compare_histogram $(PREFIX)/bin
	install compare_histogram_journal $(PREFIX)/bin
	install compare_histogram_journal_split $(PREFIX)/bin
	install correlate $(PREFIX)/bin
	install viewer $(PREFIX)/bin
	install pc_convert $(PREFIX)/bin
	install create_normals $(PREFIX)/bin
	install optimize_voxel_size $(PREFIX)/bin
.PHONY: clean
clean:
	rm -f pc_metric pc_metric2 pc_metric3 pc_metric4 pc_metric_journal compare_histogram compare_histogram_journal compare_histogram_journal_split correlate viewer pc_convert create_normals optimize_voxel_size *.o linasm/*.o
