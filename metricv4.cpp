/*
 * Copyright (C) 2019-2020 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */

#include <unistd.h>
#include <cstdint>
#include <sstream>
#include <iostream>
#include <memory>
#include <thread>

#include <Open3D.h>

#include "metric.h"
#include "pc_utils.h"
#include "textdesc/source/Lib/DefaultLBPUnit.h"
#include "textdesc/source/Lib/UniformLBPUnit.h"
#include "textdesc/source/Lib/RotationInvariantLBPUnit.h"
#include "textdesc/source/Lib/NonRotationInvariantUniformLBPUnit.h"
#include "textdesc/source/Lib/RotationInvariantVarianceLBPUnit.h"

using namespace open3d;
using namespace std;

int main(int argc, char *argv[])
{
    bool normalize = true;
    bool create_feature_pc = false;
    bool create_histogram = false;
    bool create_voxel_file = false;
    bool voxelize = false;
    int voxel_strategy = VOXEL_FIXED;
    bool divide_color_by_255 = false;
    bool use_balanced_y = false;
    double voxel_size = 0;
    int metric = DEFAULT_LBP;
    int end_of_scale = 256;
    char input_filename[MAX_FILENAME] = {0};
    char feature_pc_filename[MAX_FILENAME] = {0};
    char histogram_plot_filename[MAX_FILENAME] = {0};
    char histogram_filename[MAX_FILENAME] = {0};
    char voxel_size_filename[MAX_FILENAME] = {0};

    
    if (argc < 3){
    usage_info:
        fprintf(stderr, "Usage: %s [OPTIONS]\n", argv[0]);
        fprintf(stderr, "Usage example: %s -i input_pc.ply -m output_feature_pc.ply -h histogram.bin -a\n", argv[0]);
        fprintf(stderr, "\nOPTIONS:\n");
        fprintf(stderr, "    -i input_pc.ply         PC to be evaluated\n");
        fprintf(stderr, "    -h histogram.bin        Save the histogram as a vector of double.\n");
        fprintf(stderr, "    -m output_feature.ply   Output a feature PC map\n");
        fprintf(stderr, "    -y                      Divide color attributes by 255\n");
        fprintf(stderr, "    -v voxel_size/voxel_bias     Voxelize and use voxel size/bias as specified\n");
        fprintf(stderr, "    -s voxel_strategy       Voxelize using strategy 3 (percentage) or 4 (average_nn threshold)\n");
        fprintf(stderr, "    -o voxel_size_file      Appends the voxel size to file\n");
        fprintf(stderr, "    -a                      Y hist no target 16 bit\n");
        fprintf(stderr, "    -b                      Y hist no target 16 bit optimized\n");
        fprintf(stderr, "    -c                      Y hist 12 bit\n");
        fprintf(stderr, "    -d                      Y diff 16 bit\n");
        fprintf(stderr, "    -e                      Y diff 12 bit\n");
        fprintf(stderr, "    -f                      Y diff 8 bit\n");
        fprintf(stderr, "    -z                      Use balanced Y instead of plain Y\n");
        return EXIT_SUCCESS;
    }

    int opt;
    while ((opt = getopt(argc, argv, "i:h:m:yabcdefv:s:o:z")) != -1){
        switch (opt){
        case 'i':
            strncpy (input_filename, optarg, MAX_FILENAME);
            break;
        case 'h':
            create_histogram = true;
            strncpy (histogram_filename, optarg, MAX_FILENAME);
            break;
        case 'm':
            create_feature_pc = true;
            strncpy (feature_pc_filename, optarg, MAX_FILENAME);
            break;
        case 'y':
            divide_color_by_255 = true;
            break;
        case 'a':
            metric = DEFAULT_LBP;
            end_of_scale = 65536;
            break;
        case 'b':
            metric = UNIFORM_LBP;
            end_of_scale = 65536;
            break;
        case 'c':
            metric = ROTATION_INVARIANT_LBP;
            end_of_scale = 4096;
            break;
        case 'd':
            metric = NON_ROTATION_INVARIANT_UNIFORM_LBP;
            end_of_scale = 65536;
            break;
        case 'e':
            metric = ROTATION_INVARIANT_VARIANCE;
            end_of_scale = 65536;
            break;
        case 'f':
            metric = DEFAULT_LBP_ALT;
            end_of_scale = 4096;
            break;
        case 'v':
            voxelize = true;
            voxel_size = atof(optarg);
            break;
        case 's':
            voxel_strategy = atoi(optarg);
            break;
        case 'o':
            create_voxel_file = true;
            strncpy (voxel_size_filename, optarg, MAX_FILENAME);
            break;
        case 'z':
            use_balanced_y = true;
            break;
        default:
            fprintf(stderr, "Wrong command line.\n");
            goto usage_info;
        }
    }

    uint64_t *histogram = (uint64_t *) alloca(end_of_scale * sizeof(uint64_t));
    double *histogram_normalized = (double *) alloca(end_of_scale * sizeof(double));

    auto pc = make_shared<geometry::PointCloud>();
    auto feature_pc = make_shared<geometry::PointCloud>();
    auto vis = std::make_shared<visualization::Visualizer>();

    if (io::ReadPointCloud(input_filename, *pc))
    {
        fprintf(stderr, "Successfully read %s\n", input_filename);
    }
    else {
        fprintf(stderr, "Failed to read %s.\n", input_filename);
        return EXIT_FAILURE;
    }

    // workaround 3dtk 2^8 unsigned integer rgb range
    if (strstr (input_filename, ".xyzrgb") || divide_color_by_255)
    {
        for (size_t i = 0; i < pc->points_.size(); i++) {
            pc->colors_[i](0) = pc->colors_[i](0) / 255.0;
            pc->colors_[i](1) = pc->colors_[i](1) / 255.0;
            pc->colors_[i](2) = pc->colors_[i](2) / 255.0;
        }
    }

    // printing PC info (set the second argument true to print all the points)
    print_pointcloud(*pc, false);

    geometry::KDTreeFlann kdtree;
    
    if (voxel_strategy == VOXEL_STRATEGY_3 || voxel_strategy == VOXEL_STRATEGY_4)
    {
        // double close_nn = 9999999;
        // double distant_nn = 0;
        double average_dist = 0;

        kdtree.SetGeometry(*pc);
        
#pragma omp parallel for num_threads(32) reduction(+:average_dist) schedule(dynamic,1000)
        for (size_t i = 0; i < pc->points_.size(); i++)
        {
            std::vector<int> indices_vec(2);
            std::vector<double> dists_vec(2);

            kdtree.SearchKNN(pc->points_[i], 2, indices_vec, dists_vec);

            Eigen::Vector3d point_close = pc->points_[i];
            Eigen::Vector3d point_distant = pc->points_[indices_vec[1]];

            double dist = sqrt ( (pow((point_distant[0] - point_close[0]), 2)) +
                                 (pow((point_distant[1] - point_close[1]), 2)) +
                                 (pow((point_distant[2] - point_close[2]), 2)) );

            // #pragma omp atomic
            average_dist += dist;

            //            fprintf(stderr, "i = %lu\n", i);
#if 0
            //#pragma omp critical
            {
                if (dist < close_nn)
                {
                    close_nn = dist;
                }
                if (dist > distant_nn)
                {
                    distant_nn = dist;
                }
            }
#endif       
        }
        average_dist /= pc->points_.size();
        //  fprintf(stderr, "close nn: %.16f distant nn: %.16f average nn: %.16f\n", close_nn, distant_nn, average_dist);
        fprintf(stderr, "average nn: %.16f\n", average_dist);

        if (voxel_strategy == VOXEL_STRATEGY_3)
        {

            double step = average_dist / 4;

            double local_voxel_size = average_dist;
            auto pc_copy = pc;

            while (pc_copy->points_.size() > (pc->points_.size() * voxel_size))
            {
                local_voxel_size += step;
                fprintf(stderr, "local_voxel_size = %0.10f\n", local_voxel_size);
                pc_copy = pc->VoxelDownSample(local_voxel_size);
                // fprintf(stderr, "pc %d: %ld\n", min_pc_index, pc[min_pc_index]->points_.size());
            }

            voxel_size = local_voxel_size;

            fprintf(stderr, "voxel_size = %0.10f\n", voxel_size);

        }

        if (voxel_strategy == VOXEL_STRATEGY_4)
        {
            voxel_size = average_dist * voxel_size;
        }

    }
    
    if (create_voxel_file)
    {
        FILE *v_out = fopen(voxel_size_filename, "a");
        if (v_out != NULL)
        {
           fprintf(v_out, "%s,%.10f\n", input_filename, voxel_size);
           fclose(v_out);
        }

    }
    
    if (voxelize || voxel_strategy == VOXEL_STRATEGY_3 || voxel_strategy == VOXEL_STRATEGY_4)
    {
      //        // Voxelization process
      // auto voxel_pc = geometry::VoxelGrid::CreateFromPointCloud(*pc, voxel_size);
      // print_voxelizedpc(*voxel_pc, false);

        pc = pc->VoxelDownSample(voxel_size);

        //print_pointcloud(*pc, false);

    }

    
    kdtree.SetGeometry(*pc);
      
    // clear the histogram
    memset (histogram, 0, sizeof(uint64_t) * end_of_scale);

    // 8 nearest neighborhood LBP (first is the point itself, so nn = 9)
    int nn = 9;

//    unsigned int max_label = 0;

    // for each point in the PC - parallel execution using OpenMP
#pragma omp parallel for num_threads(32) schedule(dynamic,1000)
    for (size_t i = 0; i < pc->points_.size(); i++) {
        unsigned int label = 0;
        // for fast retrieval of nearest neighbor we use kd-tree
        std::vector<int> indices_vec(nn);
        std::vector<double> dists_vec(nn);

        // get the nearest neighbors of point with index "i"
        kdtree.SearchKNN(pc->points_[i], nn, indices_vec, dists_vec);

        const Eigen::Vector3d &point_color = pc->colors_[i];
        double point_y;
        if (use_balanced_y)
            point_y = rgb_to_balanced_ycbcr(point_color(0), point_color(1), point_color(2), 1.0);
        else
            point_y = rgb_to_y(point_color(0), point_color(1), point_color(2), 1.0);

        int center = point_y * 255;

        vector<int> neighbours(nn-1);

        for (int j = 1 ; j < nn; j++)
        { // starting from 1!
            const Eigen::Vector3d &color = pc->colors_[indices_vec[j]];
            double y;
            if (use_balanced_y)
                y = rgb_to_balanced_ycbcr(color(0), color(1), color(2), 1.0);
            else
                y = rgb_to_y(color(0), color(1), color(2), 1.0);
            
            neighbours[j-1] = y * 255;

            if (metric == DEFAULT_LBP)
            {
              // 16 bits
                if (neighbours[j-1] >= 0 && neighbours[j-1] < 16)
                    label |= 1 << 0;
                if (neighbours[j-1] >= 16 && neighbours[j-1] < 32)
                    label |= 1 << 1;
                if (neighbours[j-1] >= 32 && neighbours[j-1] < 48)
                    label |= 1 << 2;
                if (neighbours[j-1] >= 48 && neighbours[j-1] < 64)
                    label |= 1 << 3;
                if (neighbours[j-1] >= 64 && neighbours[j-1] < 80)
                    label |= 1 << 4;
                if (neighbours[j-1] >= 80 && neighbours[j-1] < 96)
                    label |= 1 << 5;
                if (neighbours[j-1] >= 96 && neighbours[j-1] < 112)
                    label |= 1 << 6;
                if (neighbours[j-1] >= 112 && neighbours[j-1] < 128)
                    label |= 1 << 7;
                if (neighbours[j-1] >= 128 && neighbours[j-1] < 144)
                    label |= 1 << 8;
                if (neighbours[j-1] >= 144 && neighbours[j-1] < 160)
                    label |= 1 << 9;
                if (neighbours[j-1] >= 160 && neighbours[j-1] < 176)
                    label |= 1 << 10;
                if (neighbours[j-1] >= 176 && neighbours[j-1] < 192)
                    label |= 1 << 11;
                if (neighbours[j-1] >= 192 && neighbours[j-1] < 208)
                    label |= 1 << 12;
                if (neighbours[j-1] >= 208 && neighbours[j-1] < 224)
                    label |= 1 << 13;
                if (neighbours[j-1] >= 224 && neighbours[j-1] < 240)
                    label |= 1 << 14;
                if (neighbours[j-1] >= 240 && neighbours[j-1] < 256)
                    label |= 1 << 15;

            }

            if (metric == UNIFORM_LBP)
            {
              // 16 bits adapted
                if (neighbours[j-1] >= 0 && neighbours[j-1] < 32)
                    label |= 1 << 0;
                if (neighbours[j-1] >= 32 && neighbours[j-1] < 45)
                    label |= 1 << 1;
                if (neighbours[j-1] >= 45 && neighbours[j-1] < 58)
                    label |= 1 << 2;
                if (neighbours[j-1] >= 58 && neighbours[j-1] < 71)
                    label |= 1 << 3;
                if (neighbours[j-1] >= 71 && neighbours[j-1] < 84)
                    label |= 1 << 4;
                if (neighbours[j-1] >= 84 && neighbours[j-1] < 97)
                    label |= 1 << 5;
                if (neighbours[j-1] >= 97 && neighbours[j-1] < 110)
                    label |= 1 << 6;
                if (neighbours[j-1] >= 110 && neighbours[j-1] < 123)
                    label |= 1 << 7;
                //                if (neighbours[j-1] >= 136 && neighbours[j-1] < 149) // mtf
                if (neighbours[j-1] >= 123 && neighbours[j-1] < 136)
                    label |= 1 << 8;
                if (neighbours[j-1] >= 136 && neighbours[j-1] < 149)
                    label |= 1 << 9;
                if (neighbours[j-1] >= 149 && neighbours[j-1] < 162)
                    label |= 1 << 10;
                if (neighbours[j-1] >= 162 && neighbours[j-1] < 175)
                    label |= 1 << 11;
                if (neighbours[j-1] >= 175 && neighbours[j-1] < 188)
                    label |= 1 << 12;
                if (neighbours[j-1] >= 188 && neighbours[j-1] < 201)
                    label |= 1 << 13;
                if (neighbours[j-1] >= 201 && neighbours[j-1] < 230)
                    label |= 1 << 14;
                if (neighbours[j-1] >= 230 && neighbours[j-1] < 256)
                    label |= 1 << 15;
            }
            if (metric == ROTATION_INVARIANT_LBP)
            {
                     // 12 bits

                if (neighbours[j-1] >= 0 && neighbours[j-1] < 21)
                    label |= 1 << 0;
                if (neighbours[j-1] >= 21 && neighbours[j-1] < 42)
                    label |= 1 << 1;
                if (neighbours[j-1] >= 42 && neighbours[j-1] < 63)
                    label |= 1 << 2;
                if (neighbours[j-1] >= 63 && neighbours[j-1] < 84)
                    label |= 1 << 3;
                if (neighbours[j-1] >= 84 && neighbours[j-1] < 106) // +1
                    label |= 1 << 4;
                if (neighbours[j-1] >= 106 && neighbours[j-1] < 128) // +1
                    label |= 1 << 5;
                if (neighbours[j-1] >= 128 && neighbours[j-1] < 150) // +1
                    label |= 1 << 6;
                if (neighbours[j-1] >= 150 && neighbours[j-1] < 172) // +1
                    label |= 1 << 7;
                if (neighbours[j-1] >= 172 && neighbours[j-1] < 193)
                    label |= 1 << 8;
                if (neighbours[j-1] >= 193 && neighbours[j-1] < 214)
                    label |= 1 << 9;
                if (neighbours[j-1] >= 214 && neighbours[j-1] < 235)
                    label |= 1 << 10;
                if (neighbours[j-1] >= 235 && neighbours[j-1] < 256)
                    label |= 1 << 11;

            }
            
            if (metric == NON_ROTATION_INVARIANT_UNIFORM_LBP)
            {
                int diff = abs(center - neighbours[j-1]);
              // 16 bits
                if (diff >= 0 && diff < 16)
                    label |= 1 << 0;
                if (diff >= 16 && diff < 32)
                    label |= 1 << 1;
                if (diff >= 32 && diff < 48)
                    label |= 1 << 2;
                if (diff >= 48 && diff < 64)
                    label |= 1 << 3;
                if (diff >= 64 && diff < 80)
                    label |= 1 << 4;
                if (diff >= 80 && diff < 96)
                    label |= 1 << 5;
                if (diff >= 96 && diff < 112)
                    label |= 1 << 6;
                if (diff >= 112 && diff < 128)
                    label |= 1 << 7;
                if (diff >= 128 && diff < 144)
                    label |= 1 << 8;
                if (diff >= 144 && diff < 160)
                    label |= 1 << 9;
                if (diff >= 160 && diff < 176)
                    label |= 1 << 10;
                if (diff >= 176 && diff < 192)
                    label |= 1 << 11;
                if (diff >= 192 && diff < 208)
                    label |= 1 << 12;
                if (diff >= 208 && diff < 224)
                    label |= 1 << 13;
                if (diff >= 224 && diff < 240)
                    label |= 1 << 14;
                if (diff >= 240 && diff < 256)
                    label |= 1 << 15;
            }

            if (metric == ROTATION_INVARIANT_VARIANCE)
            {

                int diff = abs(center - neighbours[j-1]);
              // 16 bits adapted
                if (diff >= 0 && diff < 10)
                    label |= 1 << 0;
                if (diff >= 10 && diff < 20)
                    label |= 1 << 1;
                if (diff >= 20 && diff < 30)
                    label |= 1 << 2;
                if (diff >= 30 && diff < 40)
                    label |= 1 << 3;
                if (diff >= 40 && diff < 50)
                    label |= 1 << 4;
                if (diff >= 50 && diff < 65)
                    label |= 1 << 5;
                if (diff >= 65 && diff < 80)
                    label |= 1 << 6;
                if (diff >= 80 && diff < 95)
                    label |= 1 << 7;
                if (diff >= 95 && diff < 115)
                    label |= 1 << 8;
                if (diff >= 115 && diff < 135)
                    label |= 1 << 9;
                if (diff >= 135 && diff < 155)
                    label |= 1 << 10;
                if (diff >= 155 && diff < 175)
                    label |= 1 << 11;
                if (diff >= 175 && diff < 195)
                    label |= 1 << 12;
                if (diff >= 195 && diff < 215)
                    label |= 1 << 13;
                if (diff >= 215 && diff < 235)
                    label |= 1 << 14;
                if (diff >= 235 && diff < 256)
                    label |= 1 << 15;

            }

            if (metric == DEFAULT_LBP_ALT)
            {
                int diff = abs(center - neighbours[j-1]);
              // 12 bits
                if (diff >= 0 && diff < 10)
                    label |= 1 << 0;
                if (diff >= 10 && diff < 20)
                    label |= 1 << 1;
                if (diff >= 20 && diff < 35)
                    label |= 1 << 2;
                if (diff >= 35 && diff < 50)
                    label |= 1 << 3;
                if (diff >= 50 && diff < 65)
                    label |= 1 << 4;
                if (diff >= 65 && diff < 80)
                    label |= 1 << 5;
                if (diff >= 80 && diff < 100)
                    label |= 1 << 6;
                if (diff >= 100 && diff < 130)
                    label |= 1 << 7;
                if (diff >= 130 && diff < 160)
                    label |= 1 << 8;
                if (diff >= 160 && diff < 190)
                    label |= 1 << 9;
                if (diff >= 190 && diff < 210)
                    label |= 1 << 10;
                if (diff >= 210 && diff < 256)
                    label |= 1 << 11;

            }            
        }

#if 0
        switch(metric){

        case DEFAULT_LBP:
            label = texdesc::DefaultLBPUnit<unsigned int>::getLabel(center, neighbours);
            break;
        case UNIFORM_LBP:
            label = texdesc::UniformLBPUnit<unsigned int>::getLabel(center, neighbours);
            break;
        case ROTATION_INVARIANT_LBP:
            label = texdesc::RotationInvariantLBPUnit<unsigned int>::getLabel(center, neighbours);
            break;
        case NON_ROTATION_INVARIANT_UNIFORM_LBP:
            label = texdesc::NonRotationInvariantUniformLBPUnit<unsigned int>::getLabel(center, neighbours);
            break;
        case ROTATION_INVARIANT_VARIANCE:
            label = texdesc::RotationInvariantVarianceLBPUnit<unsigned int>::getLabel(center, neighbours);
            break;
        }
#endif
//        if (label > max_label)
//            max_label = label;
        if (create_feature_pc == true)
        {
        #pragma omp critical
            {
                feature_pc->points_.push_back(pc->points_[i]);
                feature_pc->colors_.push_back(Eigen::Vector3d(
                                                  (double) label / (double) (end_of_scale-1),
                                                  (double) label / (double) (end_of_scale-1),
                                                  (double) label / (double) (end_of_scale-1))); // for grayscale, R = G = B = Y
            }
        }
        // printf("%d ", result);
#pragma omp atomic
        histogram[label]++;
    }

//    fprintf(stderr, "max_label = %u\n", max_label);

    if (normalize == true)
    {
        for (int i = 0; i < end_of_scale; i++)
            histogram_normalized[i] = (double) histogram[i] / (double) pc->points_.size();
    }

    if (create_feature_pc == true)
    {
#if ((OPEN3D_VERSION_MAJOR == 0 ) &&  (OPEN3D_VERSION_MINOR < 10))
        io::WritePointCloudToPLY(feature_pc_filename, *feature_pc, true, false);
#else
        io::WritePointCloudOption pc_params = io::WritePointCloudOption (true, false, false,  NULL);
        io::WritePointCloudToPLY(feature_pc_filename, *feature_pc, pc_params);
#endif
    }

    if (create_histogram == true)
    {
        FILE *hist_fp = fopen(histogram_filename, "w");
        if (hist_fp != NULL)
            fwrite(normalize ? (void *)histogram_normalized : (void *)histogram, 8, end_of_scale, hist_fp); // ps: sizeof(double) == sizeof(uint64_t) == 8
        else
            fprintf(stderr, "Could not write histogram output path: %s.\n", histogram_filename);
        fclose(hist_fp);
    }
        //

    strcpy(histogram_plot_filename, basename(input_filename));
    char *temp = rindex(histogram_plot_filename, '.');
    sprintf(temp, "-histogram.png");

    // print histogram to stdout in a gnuplot format
    printf("set title \"PC Metric\"\n");
    printf("set xlabel \"Value\"\n");
    printf("set ylabel \"Frequency\"\n");
    printf("set terminal png\n");
    printf("set output \"%s\"\n", histogram_plot_filename);
    printf("set style fill solid\n");
    printf("set xrange \[0:%d]\n", end_of_scale);
//    printf("set yrange \[0:0.25]\n");
    printf("$data << EOD\n");

    for (int i = 0; i < end_of_scale; i++)
    {
        if (normalize == false)
            printf("%d %lu\n", i, histogram[i]);
        if (normalize == true)
            printf("%d %.10f\n", i, histogram_normalized[i]);
    }

    printf("EOD\n");
    printf("plot \"$data\" with boxes\n");

    return EXIT_SUCCESS;
}
