/*
 * Copyright (C) 2020 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <getopt.h>
#include <stdbool.h>

#include "csv.h"
#include "metric_journal.h"

#define MAX_LINE_SIZE 4096

int main(int argc, char *argv[])
{
    int neiborhood_list_size = 0;
    int max_neiborhood_size = 0;
    int neiborhood_size[MAX_NN_LIST_SIZE];

    uint8_t metric_enabled[MAX_NR_METRICS];

    char metrics_enabled_list[MAX_FILENAME] = {0};
    char neiborhood_sizes_list[MAX_FILENAME] = {0};
    char input_filename[MAX_FILENAME] = {0};
    char fv_filename[MAX_FILENAME] = {0};
    char output_filename[MAX_FILENAME] = {0};

    if (argc < 3){
    usage_info:
        fprintf(stderr, "Usage: %s [OPTIONS]\n", argv[0]);
        fprintf(stderr, "Usage example: %s -i input_pc.ply -n 8 -h fv_file_prot.csv\n", argv[0]);
        fprintf(stderr, "\nOPTIONS:\n");
        fprintf(stderr, "    -i list.csv      Input file containing the subjective experiment data.\n");
        fprintf(stderr, "    -f fv.csv        Input file basename containing the feature vectors (or base file name in case of split files.\n");
        fprintf(stderr, "    -o output.csv    Input file basename containing the squared root fv distances.\n");
        fprintf(stderr, "    -n neiborhood_sizes_list Comma separated neiborhood size to test\n");
        fprintf(stderr, "    -m metrics_enabled_list  Comma separated enabled metrics\n");
        return EXIT_SUCCESS;
    }

    int opt;
    while ((opt = getopt(argc, argv, "i:f:o:n:m:")) != -1){
        switch (opt){
        case 'i':
            strncpy (input_filename, optarg, MAX_FILENAME);
            break;
        case 'f':
            strncpy (fv_filename, optarg, MAX_FILENAME);
            break;
        case 'o':
            strncpy (output_filename, optarg, MAX_FILENAME);
            break;
        case 'n':
            strncpy (neiborhood_sizes_list, optarg, MAX_FILENAME);
            break;
        case 'm':
            strncpy (metrics_enabled_list, optarg, MAX_FILENAME);
            break;
        default:
            fprintf(stderr, "Wrong command line.\n");
            goto usage_info;
        }
    }

    if (neiborhood_sizes_list[0] == 0)
    {
        fprintf(stderr, "Specify at least one neighbour size.\n");
        goto usage_info;
    }

    // parse input neighborhood list
    char *tok = neiborhood_sizes_list;
    strtok(neiborhood_sizes_list, ",");
    do
    {
        sscanf(tok, "%d", &neiborhood_size[neiborhood_list_size]);
        if (neiborhood_size[neiborhood_list_size] > max_neiborhood_size)
            max_neiborhood_size = neiborhood_size[neiborhood_list_size];
        neiborhood_list_size++;
    } while ((tok = strtok(NULL, ",")) && neiborhood_list_size < MAX_NN_LIST_SIZE);

#if 1
    fprintf(stderr, "total neighbours: %d max %d\n", neiborhood_list_size, max_neiborhood_size);
    for (int i = 0; i < neiborhood_list_size; i++)
        fprintf(stderr, "neighbours: %d\n", neiborhood_size[i]);
#endif

    // parse metrics selection list
    int metrics_counter = 0;
    tok = metrics_enabled_list;
    strtok(metrics_enabled_list, ",");
    do
    {
        sscanf(tok, "%hhu", &metric_enabled[metrics_counter]);
        // fprintf(stderr, "%d\n", metric_enabled[metrics_counter]);
        metrics_counter++;
    } while ((tok = strtok(NULL, ",")) && metrics_counter < MAX_NR_METRICS);

#if 1
    fprintf(stderr, "total available metrics: %d\n", metrics_counter);
    for (int i = 0; i < metrics_counter; i++)
        fprintf(stderr, "metrics[%d]: %s\n", i, metric_enabled[i]? "enabled":"disabled");
#endif

    // 2D vector with the histograms
    double *histogram[MAX_NR_METRICS][neiborhood_list_size];

    int end_of_scale[MAX_NR_METRICS][neiborhood_list_size];

    for (int j = 0; j < neiborhood_list_size; j++)
    {
        end_of_scale[LLC_16B][j] = end_of_scale[DLLC_16B][j] = 65536;
        end_of_scale[LLC_12B][j] = end_of_scale[DLLC_12B][j] = 4096;
        end_of_scale[LBP_NN_1ST][j] = end_of_scale[LBP_NN_LAST][j] = end_of_scale[LBP_ROT_INV][j] = pow(2, neiborhood_size[j]);

        end_of_scale[DLCP_8B][j] = 256;
        end_of_scale[DLCP_12B][j] = 4096;

        end_of_scale[LBP_UNIFORM][j] = neiborhood_size[j] + 2; // Uniform and rotation invariant // 10 with 8bit
        end_of_scale[LBP_NON_ROT_INV][j] = neiborhood_size[j] * (neiborhood_size[j] - 1) + 3; // Uniform non rotation invariant 59 with 8bit
    }

    for (int i = 0; i < MAX_NR_METRICS; i++)
    {
        if (metric_enabled[i] != 0)
        {
            for (int j = 0; j < neiborhood_list_size; j++)
            {
                // printf("i = %d, j = %d\n", i, j);
                histogram[i][j] = (double *) calloc(end_of_scale[i][j], sizeof(double));
                memset (histogram[i][j], 0, sizeof(double) * end_of_scale[i][j]);
            }
        }
    }


    FILE *fv_split;
    FILE *out_split;

    // write headers first?
    // for ... for ...
    // fprintf(out_split, "SIGNAL, REF, SCORE\n");

    struct csv list;
    char** fields;
    int ret;

    // fmt: SIGNAL, REF, SCORE, SIGNAL_LOCATION, REF_LOCATION, ATTACK, CLASS
    csv_open(&list, input_filename, ',', 7);

    csv_read_record(&list, &fields); /* header skip */
    while ((ret = csv_read_record(&list, &fields)) == CSV_OK)
    {
        printf("filename: %s\n", fields[3]);
        printf("ref: %s\n", fields[4]);

        for (int j = 0; j < neiborhood_list_size; j++)
        {
            for (int i = 0; i < MAX_NR_METRICS; i++)
            {
                if (metric_enabled[i] == 0)
                    continue;

                char filename[4096];

                // this is our output file format for split files!
                sprintf(filename, "%s_M%02d_N%02d.csv", fv_filename, i, neiborhood_size[j]);
                fv_split = fopen(filename, "r");

                sprintf(filename, "%s_M%02d_N%02d.csv", output_filename, i, neiborhood_size[j]);
                out_split = fopen(filename, "a");

                double histogram_ref[end_of_scale[i][j]];
                double histogram_tgt[end_of_scale[i][j]];

                bool ref_found = false;
                while (ref_found == false)
                {
                    char *line = NULL; size_t len = 0;
                    ssize_t reads = getline(&line, &len, fv_split);

                    char field1[MAX_LINE_SIZE];
                    sscanf(line, "%[^,],", field1);


                    if ( !strncmp(field1, fields[4], MAX_LINE_SIZE))
                    {
                        ref_found = true;
                        char *needle = line;
                        for (int comma = 0; comma < 1; comma++)
                        {
                            while (needle[0] != ',')
                                needle++;
                            needle++;
                        }

                        for (int jj = 0; jj < end_of_scale[i][j]; jj++)
                        {
                            histogram_ref[jj] = atof(needle);
                            // fprintf(stderr, "value[%d] = %0.16f\n", jj, histogram_ref[jj]);
                            while ((needle < line + reads) && needle[0] != ',')
                                needle++;
                            needle++;
                        }
                    }

                    free(line);
                }


                if (ref_found == false)
                    abort();

                fseek(fv_split, 0, SEEK_SET);
                bool tgt_found = false;
                while (tgt_found == false)
                {
                    char *line = NULL; size_t len = 0;
                    ssize_t reads  = getline(&line, &len, fv_split);

                    char field1[MAX_LINE_SIZE];
                    sscanf(line, "%[^,],", field1);

                    // TODO: WE NEED TO GET RID OF SPACES!!
                    //fprintf(stderr, "field1 ,%s, fields[3] ,%s,\n", field1, fields[3]);


                    if ( !strncmp(field1, fields[3], MAX_LINE_SIZE))
                    {
                        tgt_found = true;
                        char *needle = line;
                        for (int comma = 0; comma < 1; comma++)
                        {
                            while (needle[0] != ',')
                                needle++;
                            needle++;
                        }

                        for (int jj = 0; jj < end_of_scale[i][j]; jj++)
                        {
                            histogram_tgt[jj] = atof(needle);
                            // fprintf(stderr, "value[%d] = %0.16f\n", jj, histogram_tgt[jj]);

                            //printf("needle: %llu\n line = %llu\n reads = %llu\n line+reads = %llu\n", needle, line, reads, line+reads);

                            while ( (needle < line + reads) && needle[0] != ',')
                                needle++;
                            needle++;
                        }
                    }

                    free(line);

                }

                if (tgt_found == false)
                    abort();

                double sum = 0;
                for (int jj = 0; jj < end_of_scale[i][j]; jj++)
                {
                    sum += pow(histogram_tgt[jj] - histogram_ref[jj], 2);
                }

                double result = sqrt(sum);
                fprintf(out_split, "%s,%s,%.16f\n", fields[3], fields[4],  result);

                fclose(fv_split);
                fclose(out_split);
            }
        }

    } // cvs input while


    csv_close(&list);

#if 0
    while(list_end == false)
    {
        fscanf(list, "", );
        double hist1[histogram_size];
        double hist2[histogram_size];

        double sum = 0;

        fread(hist1, sizeof(double), histogram_size, h1);
        fread(hist2, sizeof(double), histogram_size, h2);

        for (int i = 0; i < histogram_size; i++)
        {
            sum += pow(hist1[i] - hist2[i], 2);
        }

        double result = sqrt(sum);
        // double result = sqrt(sum / histogram_size);
        printf("%.10f", result);

    }

    fclose(list);
    fclose(fm);
    fclose(output);
#endif

    return EXIT_SUCCESS;
}
